#include <iostream>
#include<math.h>
#include<fstream>
#include<cmath>
using namespace std;

double fkcExp(double x) { //zwracanie funkcji exp

	return (1.0 - exp(-x)) / x;

}

double Szereg(double x) {

	double wyjscie = 1, nastepny_wyraz = 1;


	for (double i = 1; i < 180; i=i+1) {

		nastepny_wyraz *= -(x / (i + 1));
		wyjscie += nastepny_wyraz;

	}

	return wyjscie;

}

int main()
{
	double dane, Wynik;

	fstream fileIn;
	fstream fileOut;

	fileIn.open("dane.txt");
    fileOut.open("wynikowy.txt");
    fileOut.clear();

	cout<<"\tDane wejsciowe \t\t\t\t"<<"Wynik z Szeregu \t\t"<<"Wynik z EXP \t\t\t"<<"Prawidlowy Wynik"<<"\t\t\t"<<"Blad"<<endl;
	fileOut<<"\tDane wejsciowe \t\t\t\t"<<"Wynik z Szeregu \t\t"<<"Wynik z EXP \t\t\t"<<"Prawidlowy Wynik"<<"\t\t\t"<<"Blad"<<endl;

	while (!fileIn.eof()) {

		fileIn >> dane;
		fileIn >> dane;
		fileIn >> Wynik;

		cout.setf(ios::scientific);
		cout.precision(15);
		cout.width(25);
		fileOut.setf(ios::scientific);
		fileOut.precision(15);
		fileOut.width(25);


		double blad=fkcExp(dane) - Wynik;
        if (abs (blad) > 1.0e-015) {

            cout<< dane << "\t\t" << Szereg(dane) << fkcExp(dane)<<"\t\t" << Wynik <<"\t\t" <<blad<< endl;
            fileOut << dane << "\t\t" << Szereg(dane) << "\t\t" <<fkcExp(dane)<<"\t\t"<< Wynik <<"\t\t" << blad<< endl;

		}
		else{
			cout << dane << "\t\t" <<"                   \t\t"<<fkcExp(dane) << "\t\t" << Wynik << "\t\t" << blad<<endl;
            fileOut << dane << "\t\t" <<"                   \t\t"<< fkcExp(dane) << "\t\t" << Wynik<<"\t\t" << blad<<endl;

		}
	}
	fileIn.close();
	fileOut.close();


    return 0;
}
