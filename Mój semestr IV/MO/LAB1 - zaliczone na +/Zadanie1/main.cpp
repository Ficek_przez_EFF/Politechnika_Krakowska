#include <iostream>
#include <math.h>
#include <stdio.h>
#include <float.h>


using namespace std;

int main()
{

int i=0;
    float e=1.0f,y=2.0f,epsilon_flt,epsilon_dbl;

    do
    {
    epsilon_flt=e;
    e=e/2.0f;
    y=e+1.0f;
    i++;
    }
    while(y!=1.0f);

    cout<<endl<<"************************ F L O A T ***************************"<<endl<<endl;
    cout<<"Wyznaczona liczba bitow mantysy dla float: "<<--i<<endl;
    cout<<"Poprawna liczba bitow mantysy dla float: 23"<<endl<<endl;
    cout<<"Wyznaczony epsilon maszynowy dla float "<<epsilon_flt<<endl;
    cout<<"Poprawny epsilon maszynowy dla float "<<FLT_EPSILON<<endl<<endl;

//***********************************************************************************************************

    i=0;
    double ed=1.0,ye=2.0;
    while(ye!=1.0)
    {
    epsilon_dbl=ed;
    ed=ed/2.0;
    ye=ed+1.0;
    i++;
    }

    cout<<"*********************** D O U B L E ****************************"<<endl<<endl;
    cout<<"Wyznaczona liczba bitow mantysy dla double: "<<--i<<endl;
    cout<<"Poprawna liczba bitow mantysy dla double: 52"<<endl<<endl;
    cout<<"Wyznaczony epsilon maszynowy dla double "<<epsilon_dbl<<endl;
    cout<<"Poprawny epsilon maszynowy dla double "<<DBL_EPSILON<<endl;



    cout<<endl<<endl<<endl<<endl<<endl;
    return 0;
}
