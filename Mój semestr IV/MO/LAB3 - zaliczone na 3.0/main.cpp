#include <iostream>
#include<cmath>
#include <iomanip>


using namespace std;

const int N_MAX = 100; //max ilosc petlii
const double TOLX = 1e-7; //zadana tolerancja bledu
const double TOLF = 1e-7; //zadana tolerancja reziduum


double fkcA(double x){

    return sin(x/4)*sin(x/4) - x;

};

double fkcB(double x) {

    return tan(2*x) - x - 1;
}

double fiPicardA(double x){

    return sin(x/4)*sin(x/4);

}

double fiPicardB(double x){

    return tan(2*x) - 1;

}

double fiPicardAPochodna(double x){

    return 1/4 * sin(x/2);

}


double fiPicardBPochodna(double x) {

    return 2 / (cos(2*x) * cos(2*x));
}

//zwykla pochodna funkcji do metody newtona
double fNewtonAPochodna(double x) {

    return 1/4 * sin(x/2) - 1;

}

// b) (tan(2x) - x - 1) = 2/(cos^2(2x)) - 1
double fNewtonBPochodna(double x){

    return 2 / (cos(2*x) * cos(2*x)) - 1;

}
void picardA(double x0)
{
    if (fabs(fiPicardAPochodna(x0)) >= 1)
        cout << "Pochodna z fi wieksza od jeden niestety metoda dla picarda nie jest zbiezna" << endl;
    else {

        cout << "\nPicarda:" << endl;
        int n = 0;
        double xn, est;

        cout << "n" << setw(13) << "x0" << setw(14) << "estymator" << setw(15) << "reziduum" << endl;

        do {
            xn = fiPicardA(x0);
            est = xn - x0;
            x0 = xn;

            cout << n << setw(13) << x0 << setw(14) << est << setw(15) << fkcA(x0) << endl;
            n++;
        } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcA(x0)) > TOLF);
    }
}

void picardB(double x0)
{
    if (fabs(fiPicardBPochodna(x0)) >= 1.0)
        cout << "Pochodna z fi wieksza od jeden niestety metoda nie jest zbiezna dla picarda" << endl;
    else
    {
        cout << "\nPicarda:" << endl;
        int n = 0;
        double xn, est;

       // cout << "n\t\tx0\t\t\testymator\t\treziduum" << endl;
        cout << "n" << setw(13) << "x0" << setw(14) << "estymator" << setw(15) << "reziduum" << endl;

        do
        {
            xn = fiPicardB(x0);
            est = xn - x0;
            x0 = xn;

            cout << n << " " << x0 << " " << est << " " << fkcB(x0) << endl;
            n++;
        } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcB(x0)) > TOLF);
    }
}

//przedzial [a,b] dzielimy na pol i wybieramy ten z dwoma roznymi znakami

void bisekcjaA(double a, double b)
{
    if ((fkcA(a)>0 && fkcA(b) > 0) || (fkcA(a) < 0 && fkcA(b) < 0))
        cout << "f(a) i f(b) musza byc roznego znaku" << endl;
    else
    {
        cout << "\nBisekcji:" << endl;
        int n = 0;
        double xn, est;
        xn = (a + b) / 2;
        est = (b - a) / 2;
        cout <<setw(3) << "n" << setw(15) << "x0" << setw(15) << "estymator" << setw(15) << "reziduum" << endl;

        do
        {
            if (fkcA(a) * fkcA(xn) < 0)
                b = xn;
            else
                a = xn;

            xn = (a + b) / 2;
            est = (b - a) / 2;

            cout <<setw(3) << n << setw(15) << xn << setw(15) << est << setw(15) << fabs(fkcA(xn)) << endl;
            n++;
        } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcA(xn)) > TOLF);
    }
}
void bisekcjaB(double a, double b)
{
    if ((fkcB(a)>0 && fkcB(b) > 0) || (fkcB(a) < 0 && fkcB(b) < 0))
        cout << "f(a) i f(b) musza byc roznego znaku" << endl;
    else
    {
        cout << "\nBisekcji:" << endl;

        int n = 0;
        double xn, est;
        xn = (a + b) / 2;
        est = (b - a) / 2;
        cout <<setw(3) << "n" << setw(15) << "x0" << setw(15) << "estymator" << setw(15) << "reziduum" << endl;
        do
        {
            if (fkcB(a) * fkcB(xn) < 0)
                b = xn;
            else
                a = xn;

            xn = (a + b) / 2;
            est = (b - a) / 2;

            cout <<setw(3) << n << setw(15) << xn << setw(15) << est << setw(15) << fabs(fkcB(xn)) << endl;
            n++;
        } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcB(xn)) > TOLF);
    }
}
//x(n + 1) = f(xn) / f'(xn)
void newtonA(double xn)
{
    cout << "\nNewtona:" << endl;

    int n = 0;
    double fxn, xn1, est;
    cout << "n" << " " << "xn" << setw(15) << "estymator" << setw(15) << "reziduum" << endl;

    do
    {
        xn1 = xn;
        xn = xn1 - fkcA(xn1)/ fNewtonAPochodna(xn1);
        est = xn - xn1;

        cout << n << " " << xn << setw(18) << est << setw(18) << fkcA(xn) << endl;
        n++;
    } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcA(xn)) > TOLF);
}

void newtonB(double xn)
{
    cout << "\nNewtona:" << endl;
    int n = 0;
    double fxn, xn1, est;
    //cout << "n\t\txn\t\t\testymator\t\treziduum" << endl;
    cout << "n" << " " << "x0" << setw(18) << "estymator" << setw(18) << "reziduum" << endl;

    do
    {
        xn1 = xn;
        xn = xn1 - fkcB(xn1)/ fNewtonBPochodna(xn1);
        est = xn - xn1;

        cout << n << " " << xn << setw(18) << est << setw(18) << fkcB(xn) << endl;
        n++;
    } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcB(xn)) > TOLF);
}

//b = a - (f(b)-f(a)/(f(b)-f(a))) wzor na kolejny wyraz
void sieczneA(double xn0, double xn1)
{
    cout << "\nSiecznych:" << endl;
    int n = 0;
    double xn2, est, fxn2;
    cout << "n" << setw(10) << "x0" << setw(15) << "estymator" << setw(15) << "reziduum" << endl;

    //cout << "n\t\txn\t\t\testymator\t\treziduum" << endl;

    do
    {
        xn2 = xn1 - fkcA(xn1) / ((fkcA(xn1)-fkcA(xn0))/(xn1-xn0));
        est = xn2 - xn1;
        xn0 = xn1;
        xn1 = xn2;

        cout << n << setw(13) << xn0 << setw(15) <<  est << setw(15)  << fkcA(xn0) << endl;
        n++;
    } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcA(xn0)) > TOLF);
}
void sieczneB(double xn0, double xn1)
{
    cout << "\nSiecznych:" << endl;
    int n = 0;
    double xn2, est, fxn2;
    cout << "n" << setw(10) << "x0" << setw(15) << "estymator" << setw(15) << "reziduum" << endl;

    //cout << "n\t\txn\t\t\testymator\t\treziduum" << endl;

    do {

        xn2 = xn1 - fkcB(xn1) / ((fkcB(xn1)-fkcB(xn0))/(xn1-xn0));
        est = xn2 - xn1;
        xn0 = xn1;
        xn1 = xn2;

        cout << n << setw(10) << xn0 << setw(15) <<  est << setw(15)  << fkcB(xn0) << endl;
        n++;
    } while (n < N_MAX && fabs(est) > TOLX && fabs(fkcB(xn0)) > TOLF);
}
int main() {

    //cout.setf(ios::scientific);
    cout << "Dla sin^2(x/4) - x = 0" <<  endl;
    picardA(0.5);
    bisekcjaA(-0.5, 0.9);
    newtonA(-0.5);
    sieczneA(-0.5, 0.6);

    cout << endl << "Dla tan(2x) - x - 1 = 0" << endl;
    picardB(0.5);
    bisekcjaB(0.4, 0.5);
    newtonB(0.6);
    sieczneB(0.4, 0.5);

    return 0;

}
