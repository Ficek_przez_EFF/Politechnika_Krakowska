#define _USE_MATH_DEFINES
#include <iostream>
#include <Windows.h>
#include <cmath>
#include <fstream>
#include <string>
#include <typeinfo>

using namespace std;

const int il_krokow = 20;
const int metoda = 7;

// tu nie patrzy� na kod tylko na wykresy pyta�:
//1. gdzie mamy ma�y krok a gdzie dzuy,
//2. co mozemy wywnioskowa� z b�ed�w obciecia-rzad, podac wz�r i zroibc obliczenia tg(alfa), pokazac gdzie ten k�t alfa sie znajduje,
//3. dlaczego rzedy sie r�znia od teoretycznych- bo gdy wsp�czynnik A=0 przy Ah^p to elementem dominuj�cym staje sie element kolejny gdzie Bh^p+1 dlatego p-rz�d jest inny
//4.gdzie na wykresie mozemy obserwowac b�ad obciecia a gdzie maszynowy
//5. jesli h=-5 to jaka bedzie wartosc? = 10^ -5


template <typename T> T fun(T x)
{
        return cos(x / 2);
}

template <typename T> T roznica_progresywna(T x, T h)
{
        return (fun(x + h) - fun(x)) / h;
}

template <typename T> T roznica_wsteczna(T x, T h)
{
        return (fun(x) - fun(x - h)) / h;
}

template <typename T> T roznica_centralna(T x, T h)
{
        return(fun(x + h) - fun(x - h)) / (2.0 * h);
}

template <typename T> T roznica_progresywna_trzypunktowa(T x, T h)
{
        return (-3.0 * fun(x)+ 4.0 * fun(x + h)  - fun(x + 2.0 * h)) / (2.0 * h);
}

template <typename T> T roznica_wsteczna_trzypunktowa(T x, T h)
{
        return (fun(x - 2.0 * h) - 4.0 * fun(x - h) + 3.0 * fun(x)) / (2.0 * h);
}

template <typename T> T pochodna(T x) {
        return -0.5*sin(x / 2.0);
}


template <typename T> void oblicz_roznice(T *kroki, T **wyniki)
{
        T poczatek = 0.0;
        T srodek = M_PI_2;
        T koniec = M_PI;
        T krok = 0.1;


        for (int i = 0; i < il_krokow; i++)
        {
                kroki[i] = krok;
                // punkt ko�cowy siatki po lewej, mozemy jedynie zastosowac przyblizenie na roznice progresywna
                wyniki[0][i] = fabs(pochodna(poczatek) - roznica_progresywna(poczatek, krok));
                wyniki[1][i] = fabs(pochodna(poczatek) - roznica_progresywna_trzypunktowa(poczatek, krok));
                // w �rodkowym w�le siatki
                wyniki[2][i] = fabs(pochodna(srodek) - roznica_progresywna(srodek, krok));
                wyniki[3][i] = fabs(pochodna(srodek) - roznica_centralna(srodek, krok));
                wyniki[4][i] = fabs(pochodna(srodek) - roznica_wsteczna(srodek, krok));
                // punkt ko�cowy siatki po prawej, mozemy jedynie zastosowac przyblizenie na roznice wsteczna
                wyniki[5][i] = fabs(pochodna(koniec) - roznica_wsteczna(koniec, krok));
                wyniki[6][i] = fabs(pochodna(koniec) - roznica_wsteczna_trzypunktowa(koniec, krok));

                krok *= 0.1;

        }
}


template <typename T> void zapis_danych(T *kroki, T **wyniki)
{
        fstream plik;
        string nazwa = "Dane_";

        nazwa += typeid(T).name();
        nazwa += ".txt";
        cout << endl << endl;

        plik.open(nazwa.c_str(), fstream::out);
        cout << "      |       punkt poczatkowy        |               punkt centralny                 |         punkt koncowy          |" << endl;

        cout << " Krok |  progr. 2pkt  |  progr. 3pkt  |  progr. 2pkt   | centr. 2pkt  | wsteczna 2pkt | wsteczna 2pkt  | wsteczna 3pkt |" << endl;
        cout << "-----------------------------------------------------------------------------------------------------------------------" << endl;
        plik << endl;

        for (int i = 0; i < il_krokow; i++)
        {
                cout.width(6);
                cout << log10(kroki[i]) << "|";
                plik << log10(kroki[i]) << " ";

                for (int j = 0; j < metoda; j++)
                {
                        plik << log10(wyniki[j][i]) << " ";
                        cout.width(15);
                        cout << log10(wyniki[j][i]) << "|";
                }

                plik << endl;
                cout << endl;
        }
        cout << "-----------------------------------------------------------------------------------------------------------------------" << endl;
        plik.close();
}

int main()
{

        float *krokiFloat, **wynikiFloat;
        double *krokiDouble, **wynikiDouble;

        krokiFloat = new float[il_krokow];
        krokiDouble = new double[il_krokow];

        wynikiFloat = new float *[metoda];
        wynikiDouble = new double *[metoda];

        for (int i = 0; i < metoda; i++)
        {
                wynikiFloat[i] = new float[il_krokow];
                wynikiDouble[i] = new double[il_krokow];
        }

        oblicz_roznice(krokiFloat, wynikiFloat);
        oblicz_roznice(krokiDouble, wynikiDouble);

        zapis_danych(krokiFloat, wynikiFloat);
        zapis_danych(krokiDouble, wynikiDouble);

        getchar();
        return 0;
}
