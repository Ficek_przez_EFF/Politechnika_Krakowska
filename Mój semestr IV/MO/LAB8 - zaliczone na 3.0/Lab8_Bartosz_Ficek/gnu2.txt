cd 'D:\Studia\M�j semestr IV\MO\LAB8\Lab8_Bartosz_Ficek'
set xrange [-21:0]
set yrange [-14:4]
set title 'Wykres double'
set ylabel 'log10(|bledu|)'
set xlabel 'log10(kroku)
set grid 
plot \
 "Dane_d.txt" using 1:2 with lines title "Poczatek(0),progresywna dwupunktowa"  ,\
 "Dane_d.txt" using 1:3 with lines title "Poczatek, progresywna trzypunktowa"  ,\
 "Dane_d.txt" using 1:4 with lines title "Srodek(pi/2),progresywna dwupunktowa"  ,\
 "Dane_d.txt" using 1:5 with lines title "Srodek(pi/2),centralna dwupunktowa"  ,\
 "Dane_d.txt" using 1:6 with lines title "Srodek(pi/2),wsteczna dwupunktowa"  ,\
 "Dane_d.txt" using 1:7 with lines title "Koniec(pi),wsteczna dwupunktowa"  ,\
 "Dane_d.txt" using 1:8 with lines title "Koniec(pi),wsteczna trzypunktowa" 

