#include <iostream>
#include <iomanip>
#define N 6
using namespace std;


void wypiszWektor(double *wektor)
{
	for (int i = 0; i < N; i++)
	{
		cout.width(8);
		cout << setprecision(6) << wektor[i];
		cout << "\t";
		cout.width(8);
	}
}

void etai(double *l, double *d, double *u, double *eta)
{
	eta[0] = d[0];
	for (int i = 1; i < N; i++)
	{
		eta[i]= d[i] - l[i] / eta[i - 1] * u[i - 1];
	}
}

void ri(double *l, double *eta, double *r, double *b)
{
	r[0] = b[0];
	for (int i = 1; i < N; i++)
	{
		r[i] = b[i] - l[i] / eta[i - 1] * r[i - 1];
	}
}

//mowil zeby pokazac gdzie jest procedura(funkcja) do liczenia tego x
void rozwiazanie( double *u, double *x, double *eta, double *r)
{
	x[N - 1] = (1.0 / eta[N - 1])*r[N - 1];
	for (int i = N - 2; i >= 0; i--)
	{
		x[i] = ((r[i] - (u[i] * x[i + 1])) / eta[i]);
	}
}


int main()
{
	double *u, *d, *l, *b, *x, *eta, *r;

	u = new double[N];
	d = new double[N];
	l = new double[N];
	b = new double[N];
	x = new double[N];
	eta = new double[N];
	r = new double[N];


	u[0] = 2.0 / 3.0;
	u[1] = 5.0 / 6.0;
	u[2] = 9.0 / 10.0;
	u[3] = 13.0 / 14.0;
	u[4] = 17.0 / 18.0;
	u[5] = 0.0;

	d[0] = 30.0;
	d[1] = 20.0;
	d[2] = 10.0;
	d[3] = 10.0;
	d[4] = 20.0;
	d[5] = 30.0;

	l[0] = 0.0;
	l[1] = 3.0 / 4.0;
	l[2] = 7.0 / 8.0;
	l[3] = 11.0 / 12.0;
	l[4] = 15.0 / 16.0;
	l[5] = 19.0 / 20.0;

	b[0] = 94.0 / 3.0;
	b[1] = 173.0 / 4.0;
	b[2] = 581.0 / 20.0;
	b[3] = -815.0 / 28.0;
	b[4] = -6301.0 / 144.0;
	b[5] = -319.0 / 10.0;

	cout << endl << "Wektor U:  ";
	wypiszWektor(u);

	cout << endl << "Wektor D ";
	wypiszWektor(d);

	cout << endl << "Wektor L:    ";
	wypiszWektor(l);

	cout << endl << "Wektor b:  ";
	wypiszWektor(b);
	cout << endl;

	cout << endl << endl<<"wektor eta:" <<endl<< endl;
	etai(l, d, u, eta);
	wypiszWektor(eta);

	cout << endl << endl<<"wektor r:" <<endl<< endl;
	ri(l, eta, r, b);
	wypiszWektor(r);


	rozwiazanie(u, x, eta, r);
	cout << endl << endl << "wektor X:" <<endl<< endl;
	wypiszWektor(x);


    return 0;
}
