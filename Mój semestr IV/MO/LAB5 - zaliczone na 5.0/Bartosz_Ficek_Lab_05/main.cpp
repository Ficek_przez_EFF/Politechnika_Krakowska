
#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>

#define N 4

using namespace std;

//Funkcja do wyświetlania macierzy
void wyswietl_macierz(double **macierz) {
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j)
			cout << setw(10) << macierz[i][j];
		cout << endl;
	}
}

//Funkcja do wyswietlania wektora
void wyswietl_wektor(double *wektor, int *index) {
	for (int i = 0; i < N; ++i)
		cout << setw(10) << wektor[index[i]] << endl;
}


//Alokowanie pamieci dla macierzy
double **tworz_nowa_macierz(int rozmiar)
{
	double **macierz = new double *[rozmiar];
	for (int i = 0; i<rozmiar; i++)
	{
		macierz[i] = new double[rozmiar];
	}
	return macierz;
}


//Obliczanie sumy w wierszu
double suma_w_wierszu(int i, double** macierz, double b[], int* index) {
	double suma = 0.0;
	for (int j = i + 1; j < N; ++j)
		suma += macierz[index[i]][j] * b[index[j]];

	return suma;
}


//Funkcja która jest odpowiedzialna za wybor elementu podstawowego
int wybor_elementu(double **macierz, int n, int j, int *index) {
	int zamiana;
	for (int i = n + 1; i < j; ++i) {
		if (fabs(macierz[index[i]][n])<fabs(macierz[index[i + 1]][n]))
			zamiana = index[i + 1];
		else
			zamiana = index[i];
	}
	return zamiana;
}


//Funkcja ktora wykonuje dekompozycje LU
void dekompozycja_LU(double **macierz, int *index) {
	int tmp;
	double s;
	for (int k = 0; k < N-1; ++k) {


		if (macierz[index[k]][index[k]] == 0.0)
		{
			tmp = wybor_elementu(macierz, index[k], 3, index);
			index[tmp] = index[k];
			index[k] = tmp;
		}

		for (int i = k + 1; i < N; ++i) {
			s = macierz[index[i]][k] / macierz[index[k]][k];    //obliczamy współczynnik

			for (int j = k + 1; j < N; ++j) {
				macierz[index[i]][j] = macierz[index[i]][j] - macierz[index[k]][j] * s; //eliminacja Gaussa
			}
			macierz[index[i]][k] = s;
		}
	}
}


//Funkcja wyliczająca wynikowy wektor x i wektor y
void wynik(double **macierz, int *index, double *b) {
	for (int k = 1; k < N; ++k)
		for (int i = k; i < N; ++i)
			b[index[i]] -= b[index[k - 1]] * macierz[index[i]][k - 1];

	cout << "Wektor y:" << endl;
	wyswietl_wektor(b, index);


	b[index[N - 1]] = b[index[N - 1]] / macierz[index[N - 1]][N- 1];
	for (int i = N - 2; i >= 0; --i)
		b[index[i]] = (b[index[i]] - suma_w_wierszu(i, macierz, b, index)) / macierz[index[i]][i];

	cout <<endl<< "Wektor x:" << endl;
	wyswietl_wektor(b, index);
}


int main()
{

	double **A = tworz_nowa_macierz(N);
	A[0][0] = 1.0;	A[0][1] = 20.0;   A[0][2] = -30.0;  A[0][3] = -4.0;
	A[1][0] = 4.0;	A[1][1] = 20.0;   A[1][2] = -6.0;	A[1][3] = 50.0;
	A[2][0] = 9.0;	A[2][1] = -18.0;  A[2][2] = 12.0;	A[2][3] = -11.0;
	A[3][0] = 16.0;	A[3][1] = -15.0;  A[3][2] = 14.0;	A[3][3] = 130.0;

	double *b;
	b = new double[N];

	b[0] = 0;
	b[1] = 114;
	b[2] = -5;
	b[3] = 177;

	int *index;
	index = new int[N];
	index[0] = 0;
	index[1] = 1;
	index[2] = 2;
	index[3] = 3;


	cout << "MACIERZ A "<<endl;
	wyswietl_macierz(A);
	cout << endl;

	cout << "WEKTOR B "<<endl;
	wyswietl_wektor(b, index);
	cout << endl;


	cout << endl << "dekompozycja LU:" << endl;
	dekompozycja_LU(A, index);
	wyswietl_macierz(A);
	cout << endl;

	wynik(A, index, b);


    return 0;
}
