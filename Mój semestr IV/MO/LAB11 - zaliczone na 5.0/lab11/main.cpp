
#include <cstdlib>
#include <iostream>
#include <cmath>
#include "calerf.h"
#include <cstdio>

using namespace std;

typedef double* ( * MetodaRozwiazywaniaMacierzy )( double**, double* b, int m);


//wskazniki do plikow z danymi
    FILE *ogolne_LU, *ogolne_TH,*wykr20 , *wykr21 , *wykr22 , *wykr23 , *wykr24 , *wykr25, *wykr26,*maksymalne_LU,*maksymalne_Thomas;
//deklaracja stalych i zmiennych projektowych
    double h , dt, lambda, D=1;
    int x, t, a=1, b=11, tmax=2;

//tablica wynikowa dla laasonen+thomas
    double** wynik;
//tablica wynikowa dla laasonen+LU
    double** wynik2;
//tablica testowa
    double** testowe;


/** DEKOMPOZYCJA LU **/

double* deLU(double **A, double *b, int M)
{
    double *wyn = new double[M];
	double x;
	for (int k = 0; k<M - 1; k++)
	{
		for (int i = k + 1; i<M; i++)
		{
			x = A[i][k] / A[k][k];
			A[i][k] = x;
			for (int j = k + 1; j<M; j++)
			{
				A[i][j] = A[i][j] - (x*A[k][j]);
			}
		}
	}

	double suma;
	double *z = new double[M];

	for (int i = 0; i<M; i++)
	{
		suma = 0;
		for (int j = 0; j <= i - 1; j++)
		{
			suma += A[i][j] * z[j];
		}

		z[i] = b[i] - suma;
	}

	for (int i = M - 1; i >= 0; i--)
	{
		suma = 0;
		for (int j = i + 1; j<M; j++)
		{
			suma += A[i][j] * wyn[j];
		}

		wyn[i] = (z[i] - suma) / A[i][i];
	}
	return wyn;
}

/** Algorytm Thomasa **/
double *metodaThomasa(double **A, double *b,int m)
{
            double n,r,suma=0;
            // wektor wartosci wynikowych
            double* x = new double [m];
            // przeksztalcenie macierzy do postaci trojkatnej gornej
            for(int i=1;i<m;i++)
            {
                    n=A[i-1][i-1];
                    r=b[i-1];
                    A[i][i]=A[i][i]-A[i][i-1]*pow(n,-1)*A[i-1][i];
                    b[i]=b[i]-A[i][i-1]*pow(n,-1)*r;
                    A[i][i-1]=0;
            }
            // algorytm rozwiazywania ukladu row. liniowych z macierza trojkatna gorna
            x[m-1]=b[m-1]/A[m-1][m-1];
            for(int i=m-2;i>=0;i--)
            {
                    for(int j=i+1;j<m;j++)
                    {
                            suma=suma+A[i][j]*x[j];
                    }
                    x[i]=(b[i]-suma)/A[i][i];
                    suma=0;
            }
            return x;
}

/** Metoda realizująca dyskretyzacje lasonen */
void laasonen(MetodaRozwiazywaniaMacierzy metoda, double** macierz_wynikowa)
{
    // macierz trojdiagonalna wspolczynnikow przy niewiadomych (stala)
    double** A = new double *[x];
        for (int i=0;i<x;i++) A[i] = new double[x];
    // uzupelnienie macierzy zerami
    for(int i=0;i<x;i++) for(int j=0;j<x;j++) A[i][j]=0;

    // wektor zawierajacy znane wartosci rownania z poprzedniego poziomu czasowego
    double* b2 = new double [x];
    for(int i=0;i<x;i++) b2[i]=0;

    // wektor wynikowy ktory zostanie uzupeniony obliczonymi wartosciami rownania
    double* w = new double [x];
    for(int i=0;i<x;i++) w[i]=0;

    double dh;
    // petla uzupelniajaca macierz wspolczynnikow (zalezna od postaci rownania)
    for(int k=1;k<t;k++){

            A[0][0]=1;
            A[x-1][x-1]=1;
            dh=h+1;
            for(int i=1;i<x-1;i++){
            // dla roznicy centralnej pierwszej pochodnej przestrzennej w rownaniu z problemu D
                    A[i][i-1]=lambda*(1.0 - (h/dh));
                    A[i][i]=-(1.0 + (2*lambda));
                    A[i][i+1]=lambda*(1.0 + (h/dh));
                    b2[i] = -macierz_wynikowa[k-1][i];
                    dh+=h;
            }
            b2[0] = macierz_wynikowa[k][0];
            b2[x-1] = macierz_wynikowa[k][x-1];

            // obliczenie wyniku dla aktualnego poziomu czasowego za pomoca metody Thomasa
            w=metoda(A,b2,x);

            for( int i = 1; i < x-1; i++ ) macierz_wynikowa[k][i] = w[i];
    }

    // zwolnienie pamieci
    for(int i=x-1;i>=0;i--) delete []A[i];
    delete []A;
    delete []b2;
    delete []w;
}

/** Metoda uzupelniajaca tablice rozwiazaniami analitycznymi */
void rozwAnalit()
{
    double tt=dt;
    double dh;
    for(int i=1;i<t;i++){
            dh=1;
            for(int j=0;j<x;j++){
               testowe[i][j]=1.0-(((double)a/(double)dh)*erfc((double)(dh-a)/(2.0*sqrt(D*tt))));
               dh+=h;
            }
            tt+=dt;
    }
}


int main(int argc, char *argv[])
{
    // krok przestrzenny
    h=0.1;
    dt=h*h;

// inicjalizacja zmiennych (krok dt obliczany tak aby lambda = 1 przy D = 1
    x=(int)((b-a)/h)+1;
    t=(int)(tmax/dt)+1;
    lambda = D*(dt/(h*h));

// tablica wynikowa laasonen + thomas
    wynik = new double *[t];
        for (int i=0;i<t;i++) wynik[i] = new double[x];
    for(int i=0;i<t;i++) for(int j=0;j<x;j++) wynik[i][j]=0;

    // tablica wyynikowa lassonem + LU
    wynik2 = new double *[t];
        for (int i=0;i<t;i++) wynik2[i] = new double[x];
    for(int i=0;i<t;i++) for(int j=0;j<x;j++) wynik2[i][j]=0;

// tablica testowa wartosci analitycznych
    testowe = new double *[t];
        for (int i=0;i<t;i++) testowe[i] = new double[x];
    for(int i=0;i<t;i++) for(int j=0;j<x;j++) testowe[i][j]=0;



/**uzupelnianie tablic o warunki brzegowe i poczatkowy**/
//warunek poczatkowy

    for(int i=0;i<x;i++) wynik[0][i]=1;
    for(int i=0;i<x;i++) wynik2[0][i]=1;

//1 warunek brzegowy
    for(int i=1;i<t;i++) wynik[i][0]=0;
    for(int i=1;i<t;i++) wynik2[i][0]=0;
    double tt=dt;

//2 warunek brzegowy
    for(int i=1;i<t;i++){
             wynik[i][x-1]=1.0-(((double)a/(double)b)*erfc(10.0/(2.0*sqrt(D*tt))));
             tt+=dt;
    }
    tt=dt;
    for(int i=1;i<t;i++){
             wynik2[i][x-1]=1.0-(((double)a/(double)b)*erfc(10.0/(2.0*sqrt(D*tt))));
             tt+=dt;
    }


/** uzupelnienie macierzy rozwiazan analitycznych**/
    rozwAnalit();


/** metody obliczenia danego rownania rozniczkowego czastkowego **/

    laasonen(metodaThomasa, wynik);
    laasonen(deLU, wynik2);


/** porownanie wynikow z wart. analitycznymi **/

    cout<<"\nlaasonen:\tDekompozycja LU:\tanalityczne:"<<endl;
    for(int i=0;i<x;i++) printf("%.6lf\t%.6lf\t%.6lf\n",wynik[2][i],wynik2[2][i],testowe[2][i]);



/** wyliczenie bladow i zapis do plikow**/

//Zapis do pliku wartości ogólnych dla LU
   ogolne_LU = fopen("ogolne_lasonen+LU.txt","w");

   fprintf(ogolne_LU,"Wartosci i bledy dla metody lasonen + LU przy kroku przestrzennym %lf i czasowym %lf\n\n",h,dt);
   double dtt=dt, dxx=1;
   for(int i=1;i<t;i++){
           dxx=1;
           fprintf(ogolne_LU,"\nczas: %.4lf\n",dtt);
           for(int j=0;j<x;j++){
                   fprintf(ogolne_LU,"x = %.4lf\t| obliczone: %.15lf\t| analityczne: %.15lf\t  | blad bezwzgl. = %.15lf\n",
                   dxx,wynik2[i][j],testowe[i][j],fabs(testowe[i][j]-wynik2[i][j]));
                   dxx+=h;
           }
           dtt+=dt;
   }
   fclose(ogolne_LU);


//Zapis do pliku wartości ogólnych dla LU
   ogolne_TH = fopen("ogolne_lasonen+TH.txt","w");
   fprintf(ogolne_TH,"Wartosci i bledy dla metody lasonen + THOMAS przy kroku przestrzennym %lf i czasowym %lf\n\n",h,dt);
    dtt=dt;
     dxx=1;
     double maxblad=0;
   for(int i=1;i<t;i++){
           dxx=1;
           fprintf(ogolne_TH,"\nczas: %.4lf\n",dtt);
           for(int j=0;j<x;j++){
                   fprintf(ogolne_TH,"x = %.4lf\t| obliczone: %.15lf\t| analityczne: %.15lf\t  | blad bezwzgl. = %.15lf\n",
                   dxx,wynik[i][j],testowe[i][j],fabs(testowe[i][j]-wynik[i][j]));
                   if ((fabs(testowe[i][j]-wynik[i][j]))>maxblad) maxblad=fabs(testowe[i][j]-wynik[i][j]);
                   dxx+=h;
           }
           dtt+=dt;
   }
fclose(ogolne_TH);


//Maksymalna wartosc bezwzgledna bledu funkcji w czasie t dla LU
    maksymalne_LU = fopen("maksymalne_LU.txt","w");
    dtt=dt;

     double maxbladLU=0;
   for(int i=1;i<t;i++){

           for(int j=0;j<x;j++){
                   if ((fabs(testowe[i][j]-wynik2[i][j]))> maxbladLU) maxbladLU=fabs(testowe[i][j]-wynik2[i][j]);
           }
           fprintf(maksymalne_LU,"\n%.12lf\t %.12lf",dtt,maxbladLU);
           maxbladLU=0;
           dtt+=dt;
   }
   fclose(maksymalne_LU);

//Maksymalna wartosc bezwzgledna bledu funkcji w czasie t dla Thomasa

    maksymalne_Thomas = fopen("maksymalne_Thomas.txt","w");
    dtt=dt;
     double maxbladTH=0;
   for(int i=1;i<t;i++){

           for(int j=0;j<x;j++){
                   if ((fabs(testowe[i][j]-wynik[i][j]))> maxbladTH) maxbladTH=fabs(testowe[i][j]-wynik[i][j]);
           }
           fprintf(maksymalne_Thomas,"\n%.12lf\t %.12lf ",dtt,maxbladTH);
           maxbladTH=0;
           dtt+=dt;
   }
fclose(maksymalne_Thomas);

//Wartosci analityczne i wyliczone dla kilku wartosci t dla LU

wykr20 = fopen("Wartosci dla t=0.2.txt","w");
   dxx=0;
   for(int j=0;j<x;j+=1){
       fprintf(wykr20,"%.4lf \t %.15lf \t %.15lf \t %.15lf \n",
       dxx,wynik2[20][j], wynik[20][j], testowe[20][j]);
       dxx+= h;
   }
fclose(wykr20);

//**********************************************

wykr21 = fopen("Wartosci dla t=0.4.txt","w");
   dxx=0;
   for(int j=0;j<x;j+=1){
       fprintf(wykr20,"%.4lf \t %.15lf \t %.15lf \t %.15lf \n",
       dxx,wynik2[40][j], wynik[40][j], testowe[40][j]);
       dxx+= h;
   }
fclose(wykr21);

//***********************************************
wykr22 = fopen("Wartosci dla t=0.6.txt","w");
    dxx=0;
   for(int j=0;j<x;j+=1){
       fprintf(wykr20,"%.4lf \t %.15lf \t %.15lf \t %.15lf \n",
       dxx,wynik2[60][j], wynik[60][j], testowe[60][j]);
       dxx+= h;
   }
fclose(wykr22);

//********************************************
wykr23 = fopen("Wartosci dla t=0.8.txt","w");
   dxx=0;
   for(int j=0;j<x;j+=1){
       fprintf(wykr20,"%.4lf \t %.15lf \t %.15lf \t %.15lf \n",
       dxx,wynik2[80][j], wynik[80][j], testowe[80][j]);
       dxx+= h;
   }
fclose(wykr23);

//********************************************
wykr24 = fopen("Wartosci dla t=1.1.txt","w");
   dxx=0;
   for(int j=0;j<x;j+=1){
       fprintf(wykr20,"%.4lf \t %.15lf \t %.15lf \t %.15lf \n",
       dxx,wynik2[110][j], wynik[110][j], testowe[110][j]);
       dxx+= h;
   }
fclose(wykr24);

//********************************************
wykr25 = fopen("Wartosci dla t=1.4.txt","w");
   dxx=0;
   for(int j=0;j<x;j+=1){
       fprintf(wykr20,"%.4lf \t %.15lf \t %.15lf \t %.15lf \n",
       dxx,wynik2[140][j], wynik[140][j], testowe[140][j]);
       dxx+= h;
   }
fclose(wykr25);

//********************************************

wykr26 = fopen("Wartosci dla t=1.9.txt","w");
   dxx=0;
   for(int j=0;j<x;j+=1){
       fprintf(wykr20,"%.4lf \t %.15lf \t %.15lf \t %.15lf \n",
       dxx,wynik2[190][j], wynik[190][j], testowe[190][j]);
       dxx+= h;
   }
fclose(wykr26);


/** czyszczenie pamieci**/

    for(int i=t-1;i>=0;i--) delete []wynik[i];
    delete []wynik;
    for(int i=t-1;i>=0;i--) delete []wynik2[i];
    delete []wynik2;
    for(int i=t-1;i>=0;i--) delete []testowe[i];
    delete []testowe;

    system("PAUSE");
    return EXIT_SUCCESS;
}
