cd 'D:\Studia\M�j semestr IV\MO\LAB11\lab11'

set xrange [0.0 : 10.5]
set yrange [0.0 : 1.2]
set title 'Wykres warto�ci dla t=0.8'
set ylabel 'Warto�ci funkcji'
set xlabel 'x'

plot \
"Wartosci dla t=0.8.txt" using 1:2 with points pt 2 ps 2 lc 7 title "Dekompozycja LU",\
"Wartosci dla t=0.8.txt" using 1:3 with points pt 1 ps 2 lc 0 title "Algorytm Thomasa",\
"Wartosci dla t=0.8.txt" using 1:4 with lines title "Warto�ci analityczne"