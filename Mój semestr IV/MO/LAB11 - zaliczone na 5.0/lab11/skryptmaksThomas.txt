cd 'D:\Studia\M�j semestr IV\MO\LAB11\lab11'

set xrange [-1.8 : -0.6]
set yrange [-5.0 :-3.0]
set title 'Wartosci bledu maksymalnego w t-max dla roznych krokow dla Thomasa'
set ylabel 'log10(maksblad)'
set xlabel 'log10(krok)'

plot \
"blad maks w tmax dla Thomasa.txt" using (log10($2)):(log10($1)) with lines title "Wartosci",\