#include <iostream>
#include <Windows.h>
#include <math.h>
#define TOL  1.0e-6
#define N 4

const int N_MAX = 100;          //ilosc max iteracji

using namespace std;
void zapis(double **, double *, double *);
void Gauss_Seidel(double **, double *, double *);
void SOR(double **, double *, double *);
void Jacobiego(double **, double *, double *);
void piszWektor(double *);
void piszMacierz(double **);


int main()
{
        double **A = new double*[N];
        for (int i = 0; i < N; i++)
                A[i] = new double[N];

        double *b = new double[N];
        double *x = new double[N];

        zapis(A, b, x);
        piszMacierz(A);
        cout << endl << endl;
        cout << " Wektor b:" << endl << endl;
        piszWektor(b);
        cout << endl;
        cout << " Wektor x:" << endl << endl;
        piszWektor(x);

        Jacobiego(A, b, x);
        zapis(A, b, x);
        Gauss_Seidel(A, b, x);
        zapis(A, b, x);
        SOR(A, b, x);


        return 0;
}

//wczytanie wartosci
void zapis(double **A, double *b, double *x)
{
        A[0][0] = 100.0; A[0][1] = 1.0; A[0][2] = -2.0;  A[0][3] = 3.0;
        A[1][0] = 4.0;   A[1][1] = 300.0; A[1][2] = -5.0; A[1][3] = 6.0;
        A[2][0] = 7.0;  A[2][1] = -8.0;  A[2][2] = 400.0; A[2][3] = 9.0;
        A[3][0] = -10.0;   A[3][1] = 11.0; A[3][2] = -12.0;  A[3][3] = 200.0;

        b[0] = 395.0;
        b[1] = 603.0;
        b[2] = -415.0;
        b[3] = -606;

        x[0] = 1.0;
        x[1] = 1.0;
        x[2] = 1.0;
        x[3] = 1.0;
}

//wypisuje macierz
void piszMacierz(double **A)
{
        cout << "Macierz:" << endl;
        for (int i = 0; i < N; i++)
        {
                for (int j = 0; j < N; j++)
                {
                        cout.width(4);
                        cout << A[i][j];
                }
                cout << endl;
        }
}

//wypisuje wektor
void piszWektor(double *c)
{
        for (int i = 0; i<N; i++)
        {
                cout.width(4);
                cout << c[i] << endl;
        }
}



double estymator(double *x, double *x_nowe)   //norma maksimum z roznicy kolejnych wektor�w przyblizen
{
        x[0] = fabs(x[0] - x_nowe[0]);
        x[1] = fabs(x[1] - x_nowe[1]);
        x[2] = fabs(x[2] - x_nowe[2]);
        x[3] = fabs(x[3] - x_nowe[3]);

        double maximum = x[0];

        for (int z = 0; z<N; z++)
        {
                if (x[z]>maximum)
                        maximum = x[z];
        }
        return maximum;
}


double residuum(double **A, double *b, double *x_nowe) //norma maksimum z wektor�w residualnych
{

        double Ax[N];
        Ax[0] = fabs((A[0][0] * x_nowe[0] + A[0][1] * x_nowe[1] + A[0][2] * x_nowe[2] + A[0][3] * x_nowe[3]) - b[0]);
        Ax[1] = fabs((A[1][0] * x_nowe[0] + A[1][1] * x_nowe[1] + A[1][2] * x_nowe[2] + A[1][3] * x_nowe[3]) - b[1]);
        Ax[2] = fabs((A[2][0] * x_nowe[0] + A[2][1] * x_nowe[1] + A[2][2] * x_nowe[2] + A[2][3] * x_nowe[3]) - b[2]);
        Ax[3] = fabs((A[3][0] * x_nowe[0] + A[3][1] * x_nowe[1] + A[3][2] * x_nowe[2] + A[3][3] * x_nowe[3]) - b[3]);

        double maximum = Ax[0];

        for (int z = 0; z<N; z++)
        {
                if (Ax[z]>maximum)
                        maximum = Ax[z];
        }
        return maximum;
}

void Jacobiego(double **A, double *b, double *x)
{
        double _estymator = 0.0, _residuum = 0.0;
        double *x_ = new double[N];
        double suma = 0.0;


        cout << endl << endl << "metoda Jacobiego" << endl;
        cout << "  n |           x0                                                  |      EST      |     RES       |" << endl << endl;

        for (int iter = 0; iter < N_MAX; iter++)
        {
                for (int i = 0; i < N; i++)
                {
                        suma = 0.0;
                        for (int j = 0; j < N; j++)
                                if (j != i)
                                        suma += A[i][j] * x[j];         //(L+U)*X
                        x_[i] = (1.0 / A[i][i]) * (b[i] - suma);        //x_=(1/D)*b-(1/D)((L+U)*X)
                }

                _estymator = estymator(x, x_);
                _residuum = residuum(A, b, x_);

                for (int i = 0; i < N; i++)
                        x[i] = x_[i];

                cout.width(4);
                cout << iter << "|";
                cout.width(15);
                cout << x_[0] << " ";
                cout.width(15);
                cout << x_[1] << " ";
                cout.width(15);
                cout << x_[2] << " ";
                cout.width(15);
                cout << x_[3] << "|";
                cout.width(15);
                cout << _estymator << "|";
                cout.width(15);
                cout << _residuum << "|" << endl;

                if (_estymator < TOL && _residuum < TOL)
                        break;


        }
}

void Gauss_Seidel(double **A, double *b, double *x)
{
        double *x_poprz = new double[N];
        double suma = 0.0;
        double _estymator = 0.0, _residuum = 0.0;

        cout << endl << endl << "Gauss_Seidel" << endl;
        cout << "  n |           x0                                                  |      EST      |     RES       |" << endl << endl;


        for (int iter = 0; iter < N_MAX; iter++)
        {
                for (int i = 0; i < N; i++)
                {
                        suma = 0.0;
                        for (int j = 0; j < N; j++)
                                if (j != i)
                                        suma += A[i][j] * x[j];
                        // L || U
                        x_poprz[i] = x[i];
                        x[i] = (1.0 / A[i][i]) * (b[i] - suma);
                }


                _estymator = estymator(x_poprz, x);
                _residuum = residuum(A, b, x);


                cout.width(4);
                cout << iter << "|";
                cout.width(15);
                cout << x[0] << " ";
                cout.width(15);
                cout << x[1] << " ";
                cout.width(15);
                cout << x[2] << " ";
                cout.width(15);
                cout << x[3] << "|";
                cout.width(15);
                cout << _estymator << "|";
                cout.width(15);
                cout << _residuum << "|" << endl;


                if (_estymator < TOL && _residuum < TOL)
                        break;
        }
}

void SOR(double **A, double *b, double *x)
{
        double *x_ = new double[N];
        double *x_poprz = new double[N];
        double suma = 0.0, omega = 0.5;
        double EST = 0.0, RESIDUUM = 0.0;

        cout << endl << endl << "SOR" << endl;

        cout << "  n |           x0                                                  |      EST      |     RES       |" << endl << endl;


        for (int iter = 0; iter < N_MAX; iter++)
        {
                for (int i = 0; i < N; i++)
                {
                        suma = 0.0;
                        for (int j = 0; j < N; j++)
                                if (j != i)
                                        suma += A[i][j] * x[j];         //(L+U)*X

                        x_poprz[i] = x[i];
                        x_[i] = (1.0 - omega) * x[i] + (omega / A[i][i]) * (b[i] - suma);
                        x[i] = x_[i];
                }

                EST = estymator(x_poprz, x_);
                RESIDUUM = residuum(A, b, x_);


                cout.width(4);
                cout << iter << "|";
                cout.width(15);
                cout << x[0] << " ";
                cout.width(15);
                cout << x[1] << " ";
                cout.width(15);
                cout << x[2] << " ";
                cout.width(15);
                cout << x[3] << "|";
                cout.width(15);
                cout << EST << "|";
                cout.width(15);
                cout << RESIDUUM << "|" << endl;

                if (EST < TOL && RESIDUUM < TOL)
                        break;
        }
}
