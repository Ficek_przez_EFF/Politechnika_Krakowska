#Rozwiazanie
set xrange [-1 : 1]
set yrange [-0.4 : 0.4]
set terminal png size 1366,768
set output "Wykres interpolacji.png"
set title 'Wykres'
set ylabel 'y'
set xlabel 'x'
set grid 
plot \
 "wyniki.txt" using 1:2 with lines title "Funkcja",\
 "wyniki.txt" using 1:3 with points pt 3 lc 3 title "Czebyszew",\
 "wyniki.txt" using 1:4 with points pt 12 lc 2 title "Rownoodlegle"

set xrange [-1 : 1]
set yrange [-0.4 : 0.4]
set terminal png size 1366,768
set output "Wykres bledow.png"
set title 'Wykres'
set ylabel 'y'
set xlabel 'x'
set grid
plot \
 "blad.txt" using 1:2 with lines title "Czybyszew",\
 "blad.txt" using 1:3 with points pt 3 lc 3 title "Rownoodlegle"