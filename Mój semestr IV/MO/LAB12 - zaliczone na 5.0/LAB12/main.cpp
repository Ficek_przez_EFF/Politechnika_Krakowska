#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

const int nodes = 15;
const double x_start = -1.0, x_end = 1.0;
double const node_step = (x_end - x_start) / (nodes - 1);

double x_ro_cooefficients[nodes];
double y_ro_cooefficients[nodes];

double x_cz_cooefficients[nodes];
double y_cz_cooefficients[nodes];

double ideal_function(double x) {
    return x / (1 + (20 * x * x * x * x));
}


//wyznaczam wspolczynniki wielomianu interpolacyjnego
void baza_newtona_rownolegle_wspolczynniki() {
    int i = 0, j, k = 1;
    int n = nodes - 1;

    for (double z = x_start; z <= x_end; z += node_step) {
        x_ro_cooefficients[i] = z;
        y_ro_cooefficients[i] = ideal_function(z);
        i++;
    }

    for (j = n; j > 0; j--) {
        for (i = n; i >= k; i--) {
            y_ro_cooefficients[i] = (y_ro_cooefficients[i] - y_ro_cooefficients[i - 1]) / (x_ro_cooefficients[i] - x_ro_cooefficients[i - k]);
        }
        k++;
    }

}


//funkcja zeby zminimalizowa� b��dy. bez zastosowania w�z��w nier�wnoodleg�ych (w�z��w Czybyszewa) funkcja na ko�cach si� jeszcze bardziej rozbiega
void baza_newtowna_czebyszew_wspolczynniki() {
    int i, j, k = 1;
    int n = nodes;

    for (i = 0; i < nodes; i++) {
        x_cz_cooefficients[i] = cos(((2.0 * i + 1.0) * M_PI) / (2.0 * nodes));
        y_cz_cooefficients[i] = ideal_function(x_cz_cooefficients[i]);
    }

    for (j = n; j > 0; j--) {
        for (i = n; i >= k; i--) {
            y_cz_cooefficients[i] = (y_cz_cooefficients[i] - y_cz_cooefficients[i - 1]) / (x_cz_cooefficients[i] - x_cz_cooefficients[i - k]);
        }
        k++;
    }
}

double wynik(double x, double *x_cofs, double* y_cofs){
    double wynik;
    int n = nodes - 1, i;
    wynik = y_cofs[n];

    for (i = n; i >= 0; i--) {
        wynik *= (x - x_cofs[i]);
        wynik += y_cofs[i];
    }
    return wynik;
}



int main() {
    fstream data;

    const int N = 200;

    double x = x_start;
    double krok = (x_end - x_start) / N;

    baza_newtona_rownolegle_wspolczynniki();
    baza_newtowna_czebyszew_wspolczynniki();

    data.open("wyniki.txt", fstream::out);

    for (; x < x_end; x += krok) {
        double calculated_value = ideal_function(x);
        double newt_czeb = wynik(x, x_cz_cooefficients, y_cz_cooefficients);
        double newt_rown = wynik(x, x_ro_cooefficients, y_ro_cooefficients);
        data << x << " " << calculated_value << " " << newt_czeb << " " << newt_rown << endl;
    }
    data.close();



    return 0;
}
