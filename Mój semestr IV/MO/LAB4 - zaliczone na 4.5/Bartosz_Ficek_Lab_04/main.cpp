
#include <iostream>
#include <math.h>
#include <algorithm>

using namespace std;

#define N_MAX 10
const double TOLX = 1.0e-7;
const double TOLF = 1.0e-7;

double F[3][1];

double J_F[3][1];
double x[3][1];

double f0(double x, double y, double z) { return x*x + y*y + z*z - 2.0; }
double f1(double x, double y, double z) { return x*x + y*y - 1.0; }
double f2(double x, double y, double z) { return x*x - y; }

double j0_0(double x, double y, double z) { return 2.0*x; }
double j0_1(double x, double y, double z) { return 2.0*y; }
double j0_2(double x, double y, double z) { return 2.0*z; }
double j1_0(double x, double y, double z) { return 2.0*x; }
double j1_1(double x, double y, double z) { return 2.0*y; }
double j1_2(double x, double y, double z) { return 0.0; }
double j2_0(double x, double y, double z) { return 2.0*x; }
double j2_1(double x, double y, double z) { return -1.0; }
double j2_2(double x, double y, double z) { return 0.0; }


//***************************************
double wyznacznik(double(&J)[3][3])
{
	return (J[0][0] * J[1][1] * J[2][2] + J[1][0] * J[2][1] * J[0][2] + J[2][0] * J[0][1] * J[1][2]) - (J[0][2] * J[1][1] * J[2][0] + J[1][2] * J[2][1] * J[0][0] + J[2][2] * J[0][1] * J[1][0]);
}

double max(double x, double y, double z)
{
	double n;

	n = fabs(x);
	if (fabs(y)>x)
	{
		n = fabs(y);
	}
	if (fabs(z)>x)
	{
		n = fabs(z);
	}
	return n;
}

int main()
{
	double x, y, z, dx, dy, dz, det, fun1, fun2, fun3, EST, RESIDUUM;
	double J[3][3];
	double Jpomoc[3][3];
	x = y = z = 2.0;
	dx = dy = dz = 0.0;

	cout << " n |      x           |          y       |        z         |           EST    |          RES     |\n";

	for (int i = 0; i < N_MAX; i++)
	{
		fun1 = f0(x, y, z);
		fun2 = f1(x, y, z);
		fun3 = f2(x, y, z);
		EST = max(dx, dy, dz);
		RESIDUUM = max(fun1, fun2, fun3);

		cout.width(3);
		cout << i << "|";
		cout.width(18);
		cout << x << "|";
		cout.width(18);
		cout << y << "|";
		cout.width(18);
		cout << z << "|";
		cout.width(18);
		cout << EST << "|";
		cout.width(18);
		cout << RESIDUUM << "|" << endl;


		//wypełnaimy macierz jakobiego
		J[0][0] = j0_0(x, y, z);
		J[0][1] = j0_1(x, y, z);
		J[0][2] = j0_2(x, y, z);
		J[1][0] = j1_0(x, y, z);
		J[1][1] = j1_1(x, y, z);
		J[1][2] = j1_2(x, y, z);
		J[2][0] = j2_0(x, y, z);
		J[2][1] = j2_1(x, y, z);
		J[2][2] = j2_2(x, y, z);

		//obliczenie wyznacznika macierzy Jakobiego
		det = wyznacznik(J);

		//wyznacznik x
		Jpomoc[0][0] = fun1;
		Jpomoc[1][0] = fun2;
		Jpomoc[2][0] = fun3;

		for (int j = 0; j < 3; ++j)
			for (int k = 1; k < 3; k++)
				Jpomoc[j][k] = J[j][k];

		dx = wyznacznik(Jpomoc);

		//wyznaczniik y
		Jpomoc[0][1] = fun1;
		Jpomoc[1][1] = fun2;
		Jpomoc[2][1] = fun3;

		for (int j = 0; j < 3; ++j)
			for (int k = 0; k < 3; k++)
				if (k != 1)
					Jpomoc[j][k] = J[j][k];

		dy = wyznacznik(Jpomoc);

		//wyznacznik z
		Jpomoc[0][2] = fun1;
		Jpomoc[1][2] = fun2;
		Jpomoc[2][2] = fun3;

		for (int j = 0; j < 3; ++j)
			for (int k = 0; k < 2; k++)
				Jpomoc[j][k] = J[j][k];

		dz = wyznacznik(Jpomoc);


		//obliczamy poprawke
		dx = dx / det;
		dy = dy / det;
		dz = dz / det;

		//odejmujemy poprawke i otrzymujemy kolejne przyblizenia
		x = x - dx;
		y = y - dy;
		z = z - dz;

//*********************************
		if ((max(dx, dy, dz)) <= TOLX)
		{
			cout << "Kryterium estymatora bledu " << TOLX << endl;
			break;

		}
		if (max(fun1, fun2, fun3) <= TOLF)
		{
			cout << "Kryterium residuum " << TOLF << endl;
			break;
		}
	}
	system("pause");
}
