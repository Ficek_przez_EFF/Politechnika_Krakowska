cd 'D:\Studia\M�j semestr IV\MO\LAB10\lab10'

set xrange [0 : 5]
set yrange [0 : 1.20]
set terminal png size 1400,900
set output 'Wykres_MPE.png'
set title 'Metoda Posrednia Eulera '
set ylabel 'y'
set xlabel 't'
set grid
set key outside center bottom horizontal
plot \
 'dane.txt' using 1:2 with lines title "Rozwiazanie Analityczne",\
'dane.txt'  using 1:3 with points pt 1 lc 8 title "Metoda Posrednia Eulera",\


set xrange [0 : 5]
set yrange [0 : 1.20]
set terminal png size 1400,900
set output 'Wykres_MT.png'
set title 'Metoda Trapezow'
set ylabel 'y'
set xlabel 't'
set grid
set key outside center bottom horizontal
plot \
'dane.txt' using 1:2 with lines title "Rozwiazanie Analityczne",\
'dane.txt' using 1:4 with points pt 1 lc 8 title "Metoda Trapezow"


set xrange [0 : 5]
set yrange [0 : 1.20]
set terminal png size 1400,900
set output 'Wykres_MBE_stabilna.png'
set title 'Metoda Bezposrednia Eulera - stabilna (krok 0.005)'
set ylabel 'y'
set xlabel 't'
set grid
set key outside center bottom horizontal
plot \
'dane.txt' using 1:2 with lines title "Rozwiazanie Analityczne",\
'dane.txt' using 1:5 with points pt 1 lc 8 title "Metoda Bezposrednia Eulera - stabilna",


set xrange [0 : 5]
set yrange [-600 : 600]
set terminal png size 1400,900
set output 'Wykres_MBE_niestabilna.png'
set title 'Metoda Bezposrednia Eulera - niestabilna (krok 0.2)'
set ylabel 'y'
set xlabel 't'
set grid
set key outside center bottom horizontal
plot \
'dane.txt' using 1:2 with lines title "Rozwiazanie Analityczne",\
'danee.txt' using 1:2 with points pt 1 lc 8 title "Metoda Bezposrednia Eulera - niestabilna",


set xrange [-20 : 0]
set yrange [-30 : 2]
set terminal png size 1400,900
set output 'Wykres_bledow.png'
set title 'Porownanie bledow'
set ylabel 'log10(bledu)'
set xlabel 'log10(dt)'
set grid
set key outside center bottom horizontal
plot \
 'bledy.txt' using 1:2 with lines title "Metoda Bezposrednia Eulera",\
 'bledy.txt' using 1:3 with lines title "Metoda Posrednia Eulera",\
 'bledy.txt' using 1:4 with lines title "Metoda Trapezow"