
set title 'Metoda Bezposrednia Eulera - niestabilna (krok 0.12)'
set ylabel 'y'
set xlabel 't'
set grid
set key outside center bottom horizontal
plot \
'dane.txt' using 1:2 with lines title "Rozwiazanie Analityczne",\
'daneNS.txt' using 1:2 with points pt 1 lc 8 title "Metoda Bezposrednia Eulera - niestabilna",

