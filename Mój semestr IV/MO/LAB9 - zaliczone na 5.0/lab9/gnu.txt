cd 'D:\Studia\M�j semestr IV\MO\LAB9\lab9'
 
set xrange [-0.1 : 1]
set yrange [-0.1 : 0.6]
set terminal png size 1366,768
set output "Wykres_funkcji.png"
set title 'Wykres funkcji'
set ylabel 'y'
set xlabel 'x'
set key inside bottom right
set grid 
plot \
 "wyniki_konwencjonalna.txt" using 1:2 with points pt 1 ps 2 lc 2 title "Dyskretyzacja Konwencjonalna Trojpunktowa",\
 "wyniki_numerowa.txt" using 1:2 with points pt 2 ps 2 lc 7 title "Dyskretyzacja Numerowa",\
 "wyniki_konwencjonalna.txt" using 1:3 with lines lc 8 title "Rozwiazanie analityczne"