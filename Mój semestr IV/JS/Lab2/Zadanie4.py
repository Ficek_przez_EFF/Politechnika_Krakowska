import random
def k4():
    
    return random.randint(1, 4)
def k8():
    
    return random.randint(1, 8)
def k10():
    
    return random.randint(1, 10)
def k20():
    
    return random.randint(1, 20)

def getDice(tekst):
    if tekst=='k4':
        return k4()
    elif tekst=='k8':
        return k8()
    elif tekst=='k10':
        return k10()
    elif tekst=='k20':
        return k20()

def rollDice(tekst):
    liczby=tekst.split("k")  
    suma=0
    ilosc=int(liczby[0])
   
    for x in range (0, ilosc):
        if liczby[1]=="4":
            funkcja=k4()
            suma=suma+funkcja
          
        elif liczby[1]=="8":
            funkcja=k8()
            suma=suma+funkcja
            
        elif liczby[1]=="10":
            funkcja=k10()
            suma=suma+funkcja
            
        elif liczby[1]=="20":
            funkcja=k20()
            suma=suma+funkcja
            
    return suma     
            
      
    
print(rollDice("3k4"))
