
def plansza(wielkosc):
    tablica = [[x_czy_y(x, y, wielkosc) for y in range(1, wielkosc + 1)] for x in range(1, wielkosc + 1)]
    return tablica

def x_czy_y(x, y, wielkosc):
    if wielkosc % 2 == 0:
        if y % 2 == 0:
            return 1
        else:
            return 0
    else:
        if x % 2 == 0:
            if y % 2 == 0:
                return 1
            else:
                return 0
        else:
            if y % 2 == 0:
                return 0
            else:
                return 1


print(plansza(3))
print(plansza(5))
