from View import View
import tkinter as tk
from functools import partial

class ViewItems(View):
    __buy_button = None
    __imageCacheItems = {}
    __imageCacheCoins = {}
    __choice = 0
    __choice_variable = None
    __balance_text_variable = None
    __item_desc_text_variables = {}

    def __init__(self, frame=None):
        super().__init__(frame) # wywołanie konstruktora z klasy bazowej frame

    def show(self):
        self.display_items()
        self.display_choice_buttons()
        self.display_choice()
        self.display_pay_buttons()
        self.display_menu_buttons()
        self.show_me_baby2()
        pass

    def show_me_baby2(self):
        self.__am_variable = tk.StringVar()
        am_label = tk.Label(master=self._frame.master, textvariable=self.__am_variable, width=30, height=2,bg='#c9fcf4', activebackground='#a1cec7')
        self.__am_variable.set(str(self.controller._coin_holder_deposit.get_coins()))
        am_label.place(x=730, y=100) #x=515 y=560

    def hide(self):
        for image in self.__imageCacheItems:
            image.destroy()
        for image in self.__imageCacheCoins:
            image.destroy()

    def display_items(self):
        row = 1
        items_in_curr_row = 0
        for item_id in range(30, 51):
            item = self.controller.get_item(item_id)
            if item is None:
                continue

            self.__imageCacheItems[item_id] = tk.PhotoImage(master=self._frame.master, file=item.get_image_path())
            label_image = tk.Label(master=self._frame.master, text="", image=self.__imageCacheItems[item_id],
                                   borderwidth=0)
            item_x = (items_in_curr_row + 1) * 90 - 50
            item_y = row * 150 - 110
            label_image.place(x=item_x+10, y=item_y)
            label_id = tk.Label(master=self._frame.master, text=item.get_id(),bg='#A9A9A9')
            label_id.place(x=item_x + 34, y=item_y + 70)

            self.__item_desc_text_variables[item_id] = tk.StringVar()
            self.update_item_desc(item)
            label_price_stock = tk.Label(master=self._frame.master, textvariable=self.__item_desc_text_variables[item_id],bg='#D3D3D3')
            label_price_stock.place(x=item_x, y=item_y + 95)

            items_in_curr_row += 1
            if items_in_curr_row >= 7:
                row += 1
                items_in_curr_row = 0

            self.__juh_variable = tk.StringVar()
            juh_label = tk.Label(master=self._frame.master, textvariable=self.__juh_variable, width=22, height=1) # mój juh ma 10
            self.__juh_variable.set("Stan monet w automacie:")
            juh_label.place(x=762, y=40)

            self.__juhc_variable = tk.StringVar()
            juhc_label = tk.Label(master=self._frame.master, textvariable=self.__juhc_variable, width=30, height=1)  # mój juh ma 10
            self.__juhc_variable.set("1gr,2gr,5gr,10gr,20gr,50gr,1zl,2zl,5zl")
            juhc_label.place(x=730, y=71)


    def display_choice_buttons(self):
        button1 = tk.Button(master=self._frame.master, text="1", width=3, bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_1)
        button2 = tk.Button(master=self._frame.master, text="2", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_2)
        button3 = tk.Button(master=self._frame.master, text="3", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_3)
        button4 = tk.Button(master=self._frame.master, text="4", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_4)
        button5 = tk.Button(master=self._frame.master, text="5", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_5)
        button6 = tk.Button(master=self._frame.master, text="6", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_6)
        button7 = tk.Button(master=self._frame.master, text="7", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_7)
        button8 = tk.Button(master=self._frame.master, text="8", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_8)
        button9 = tk.Button(master=self._frame.master, text="9", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_9)
        button0 = tk.Button(master=self._frame.master, text="0", width=3,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_0)
        buttonC = tk.Button(master=self._frame.master, text="CLR", width=6,bg='#c9fcf4', activebackground='#a1cec7',
                            command=self.on_click_button_items_cancel)
        buttonCh = tk.Button(master=self._frame.master, text="CHECK", width=10,bg='#c9fcf4', activebackground='#a1cec7',
                             command=self.controller.on_click_button_check)
        button1.place(x=130, y=560)
        button2.place(x=160, y=560)
        button3.place(x=190, y=560)
        button4.place(x=220, y=560)
        button5.place(x=250, y=560)
        button6.place(x=280, y=560)
        button7.place(x=310, y=560)
        button8.place(x=340, y=560)
        button9.place(x=370, y=560)
        button0.place(x=400, y=560)
        buttonC.place(x=430, y=560)
        buttonCh.place(x=480, y=560)

    def display_pay_buttons(self):
        values = [1, 2, 5, 10, 20, 50, 100, 200, 500]
        row = 1
        items_in_curr_row = 0
        for i in range(len(values)):
            file_name = "coins/" + str(values[i]) + ".png"
            self.__imageCacheCoins[i] = tk.PhotoImage(master=self._frame.master, file=file_name)
            event = partial(self.controller.on_click_button_coin, values[i])
            button = tk.Button(master=self._frame.master, text="", image=self.__imageCacheCoins[i], borderwidth=0,
                               command=event)
            button.place(x=797 + items_in_curr_row * 40, y=180 + row * 40)
            items_in_curr_row += 1
            if items_in_curr_row >= 3:
                row += 1
                items_in_curr_row = 0

        self.__balance_text_variable = tk.StringVar()
        balance_text_label = tk.Label(master=self._frame.master, textvariable=self.__balance_text_variable)
        self.__balance_text_variable.set("0 zł")
        balance_text_label.place(x=797, y=180, width=114, height=30)

    def display_choice(self):
        self.__choice_variable = tk.StringVar()
        choice_label = tk.Label(master=self._frame.master, textvariable=self.__choice_variable, width=8, height=2, bg='#c9fcf4')
        self.__choice_variable.set("0")
        choice_label.place(x=320, y=500)

    def display_menu_buttons(self):
        button_cancel = tk.Button(master=self._frame.master, text="Anuluj", width=15, bg='#ff8989', activebackground='#fc4646',
                                  command=self.controller.on_click_button_cancel)
        button_cancel.place(x=788, y=350)
        button_buy = tk.Button(master=self._frame.master, text="Kup", width=15, bg='#87ff7c', activebackground='#49ff53',
                               command=self.controller.on_click_button_buy)
        button_buy.place(x=788, y=380)

    def update_item_desc(self, item):
        self.__item_desc_text_variables[item.get_id()].set(
            "Cena: " + str(item.get_price() / 100) + "0 zł\nStan: " + str(item.get_quantity()))

    def on_click_button_items_1(self):
        self.append_choice_symbol(1)

    def on_click_button_items_2(self):
        self.append_choice_symbol(2)

    def on_click_button_items_3(self):
        self.append_choice_symbol(3)

    def on_click_button_items_4(self):
        self.append_choice_symbol(4)

    def on_click_button_items_5(self):
        self.append_choice_symbol(5)

    def on_click_button_items_6(self):
        self.append_choice_symbol(6)

    def on_click_button_items_7(self):
        self.append_choice_symbol(7)

    def on_click_button_items_8(self):
        self.append_choice_symbol(8)

    def on_click_button_items_9(self):
        self.append_choice_symbol(9)

    def on_click_button_items_0(self):
        self.append_choice_symbol(0)

    def on_click_button_items_cancel(self):
        self.__choice = 0
        self.__choice_variable.set(str(self.__choice))

    def append_choice_symbol(self, symbol):
        if self.__choice == 0:
            self.__choice = int(symbol)
        elif self.__choice < 10:
            self.__choice = (self.__choice * 10) + int(symbol)
        self.__choice_variable.set(str(self.__choice))

    def set_balance_text(self, text):
        self.__balance_text_variable.set(text)

    def set_choice(self, choice):
        self.__choice = choice
        self.__choice_variable.set(str(self.__choice))

    def get_choice(self):
        return self.__choice
