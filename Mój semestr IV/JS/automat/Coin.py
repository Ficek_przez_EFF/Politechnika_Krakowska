class Coin:
    def __init__(self, value): # konstruktor klasy coin
        allowed_values = [1, 2, 5, 10, 20, 50, 100, 200, 500] # dostępne waluty
        if value not in allowed_values:
            if value not in allowed_values:
                raise Exception("Invalid coin value specified")# wyrzuca wyjątek jak Coin nie jest z allowed_values
        self._value = value

    def get_value(self) -> int:
        return self._value # zwraca wartość monety


