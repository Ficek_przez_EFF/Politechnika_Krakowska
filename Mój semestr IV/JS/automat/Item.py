
class Item:
    def __init__(self, id, name, image_path, price, quantity=5): # id, nazwa, ścieżka, cena, ilość - domyślnie 5
        self._id = id
        self._name = name
        self._image_path = image_path
        self._price = price
        self._quantity = quantity

    def get_id(self):
        return self._id
    def get_name(self):
        return self._name

    def get_image_path(self):
        return self._image_path

    def get_price(self):
        return self._price

    def get_quantity(self):
        return self._quantity

    def set_quantity(self, quantity):
        self._quantity = quantity

