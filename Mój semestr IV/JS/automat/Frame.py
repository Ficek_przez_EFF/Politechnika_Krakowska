import tkinter as tk


class Frame(tk.Frame):
    def __init__(self, master=None):
        self.master = tk.Tk()
        super().__init__(master)

        self.initialize()

    def initialize(self):
        self.master.title("Człowiek nie wielbłąd - pić musi")
        self.master.configure(background='silver')
        self.set_size(1000, 650)
        self.center()

    def set_size(self, width, height):
        self.master.minsize(width=width, height=height)
        self.master.maxsize(width=width, height=height)

    def center(self):
        self.master.update_idletasks()
        width = self.master.winfo_width()
        height = self.master.winfo_height()
        x = (self.master.winfo_screenwidth() // 2) - (width // 2)
        y = (self.master.winfo_screenheight() // 2) - (height // 2)
        self.master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
