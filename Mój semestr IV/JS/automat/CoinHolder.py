from copy import deepcopy

from Coin import Coin


class CoinHolder:
    _coins = []

    def __init__(self):
        self._coins = []


    def add_coin(self, coin): # metoda dodająca monetę, jeśli nie podasz monety, to wyrzuca wyjątek
        if type(coin) is Coin:
            self._coins.append(coin)
        else:
            raise Exception("You can add only coins")


    def get_sum(self): # metoda sumująca wartość monet
        value = 0
        for coin in self._coins:
            value += coin.get_value()
        return value

    def clear(self): # czyści monety
        self._coins.clear()

    def get_coins(self): # tworzy listę, pierwsze miejsce to liczba monet jednogroszowych, drugie dwugroszowych itp.
        values = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        for coin in self._coins:
            if coin.get_value() == 1:
                values[0] += 1
            if coin.get_value() == 2:
                values[1] += 1
            if coin.get_value() == 5:
                values[2] += 1
            if coin.get_value() == 10:
                values[3] += 1
            if coin.get_value() == 20:
                values[4] += 1
            if coin.get_value() == 50:
                values[5] += 1
            if coin.get_value() == 100:
                values[6] += 1
            if coin.get_value() == 200:
                values[7] += 1
            if coin.get_value() == 500:
                values[8] += 1
        return values

    def add_coins(self, coins): # dodaje monety do listy coins
        if coins[0] > 0:
            for i in range(coins[0]):
                self.add_coin(Coin(1))
        if coins[1] > 0:
            for i in range(coins[1]):
                self.add_coin(Coin(2))
        if coins[2] > 0:
            for i in range(coins[2]):
                self.add_coin(Coin(5))
        if coins[3] > 0:
            for i in range(coins[3]):
                self.add_coin(Coin(10))
        if coins[4] > 0:
            for i in range(coins[4]):
                self.add_coin(Coin(20))
        if coins[5] > 0:
            for i in range(coins[5]):
                self.add_coin(Coin(50))
        if coins[6] > 0:
            for i in range(coins[6]):
                self.add_coin(Coin(100))
        if coins[7] > 0:
            for i in range(coins[7]):
                self.add_coin(Coin(200))
        if coins[8] > 0:
            for i in range(coins[8]):
                self.add_coin(Coin(500))

    def remove_coins(self, coins): # usuwa monety
        coins_to_remove = deepcopy(coins)
        new_coins = []
        for coin in self._coins:
            if coin.get_value() == 1:
                if coins_to_remove[0] > 0:
                    coins_to_remove[0] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 2:
                if coins_to_remove[1] > 0:
                    coins_to_remove[1] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 5:
                if coins_to_remove[2] > 0:
                    coins_to_remove[2] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 10:
                if coins_to_remove[3] > 0:
                    coins_to_remove[3] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 20:
                if coins_to_remove[4] > 0:
                    coins_to_remove[4] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 50:
                if coins_to_remove[5] > 0:
                    coins_to_remove[5] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 100:
                if coins_to_remove[6] > 0:
                    coins_to_remove[6] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 200:
                if coins_to_remove[7] > 0:
                    coins_to_remove[7] -= 1
                else:
                    new_coins.append(coin)
            elif coin.get_value() == 500:
                if coins_to_remove[8] > 0:
                    coins_to_remove[8] -= 1
                else:
                    new_coins.append(coin)
        self._coins = new_coins

