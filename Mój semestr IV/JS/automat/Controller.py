from tkinter import messagebox

from Coin import Coin
from CoinHolder import CoinHolder
from Item import Item
from views.ViewItems import ViewItems

class Controller():
    __application = None
    __views = {}
    __current_view = None
    _coin_holder_deposit = None
    _coin_holder_transaction = None
    _items = []
    _selected_item = 0

    def __init__(self, application):
        self.__application = application
        self._coin_holder_deposit = CoinHolder()
        self._coin_holder_transaction = CoinHolder()

        self.init_views()
        self.init_items()

        self.show_view('items')
        self.get_view('items').show_me_baby2()


    def show_me_baby(self):
        print(self._coin_holder_deposit.get_coins())

    def init_views(self):
        view_items = ViewItems(self.__application)
        view_items.set_controller(self)
        self.add_view('items', view_items)

    def add_view(self, name, view):
        self.__views[name] = view

    def get_view(self, name):
        return self.__views[name]

    def show_view(self, name):
        if self.__current_view is not None:
            self.get_view(self.__current_view).hide()

        self.__current_view = name
        self.get_view(self.__current_view).show()

    def init_items(self):
        self._items.append(Item(30, "7up 0.33l", "items/7up.png", 200))
        self._items.append(Item(31, "Coca Cola 0.33l", "items/cocacola.png", 200))
        self._items.append(Item(32, "Fanta 0.33l", "items/fanta.png", 200))
        self._items.append(Item(33, "Goralek", "items/goralek.png", 200))
        self._items.append(Item(34, "Sprite 0.33l", "items/sprite.png", 200))
        self._items.append(Item(35, "Icea Tea 0.33l", "items/icetea.png", 350))
        self._items.append(Item(36, "Mirinda 0.33l", "items/mirinda.png", 350))
        self._items.append(Item(37, "Schwepps 0.33l", "items/schwepps.png", 370))
        self._items.append(Item(38, "Pepsi 0.33l", "items/pepsi.png", 350))
        self._items.append(Item(39, "Mountain Dew 0.33l", "items/mount.png", 230))
        self._items.append(Item(40, "7up 0.5l", "items/7up.png", 200))
        self._items.append(Item(41, "Coca Cola 0.5l", "items/cocacola.png", 260))
        self._items.append(Item(42, "Fanta 0.5l", "items/fanta.png", 160))
        self._items.append(Item(43, "Sprite 0.5l", "items/sprite.png", 180))
        self._items.append(Item(44, "Snickers", "items/snickers.png", 180))
        self._items.append(Item(45, "Icea Tea 0.5l", "items/icetea.png", 180))
        self._items.append(Item(46, "Mirinda 0.5l", "items/mirinda.png", 180))
        self._items.append(Item(47, "Schwepps 0.5l", "items/schwepps.png", 180))
        self._items.append(Item(48, "Mountain Dew", "items/mount.png", 180))
        self._items.append(Item(49, "Mars", "items/mars.png", 180))
        self._items.append(Item(50, "Snickers x3", "items/snickers.png", 180))

    def get_item(self, id):
        for item in self._items:
            if item.get_id() == id:
                return item
        return None

    def on_click_launch(self):
        self.show_view('items')

    def on_click_button_coin(self, value):
        self._coin_holder_transaction.add_coin(Coin(value))
        self.get_view('items').set_balance_text(str(self._coin_holder_transaction.get_sum() / 100) + " zł")
        pass

    def on_click_button_cancel(self):
        self.get_view('items').set_choice(0)
        returned_coins_list = self.to_string(self._coin_holder_transaction.get_coins())
        if len(returned_coins_list) == 0:
            messagebox.showinfo("Anulowano transakcję", "Anulowano transakcję.")
        else:
            messagebox.showinfo("Anulowano transakcję",
                                "Anulowano transakcję. \nOtrzymałeś z powrotem monety:\n" + returned_coins_list)

        self._coin_holder_transaction.clear()
        self.get_view('items').set_balance_text("0 zł")
        pass

    def on_click_button_buy(self):
        item_id = self.get_view('items').get_choice()
        item = self.get_item(item_id)
        reset_choice = False
        if item is None:
            messagebox.showinfo("Błąd", "Wybrano nieprawidłowy przedmiot.")
            reset_choice = True
        elif item.get_quantity() < 1:
            messagebox.showinfo("Błąd", "Ten przedmiot został już wyprzedany.")
            reset_choice = True
        elif item.get_price() > self._coin_holder_transaction.get_sum():
            messagebox.showinfo("Błąd", "Zapłacono za mało.\nCena " + item.get_name() + " to " + str(
                item.get_price() / 100) + "0 zł.")
        elif self.get_required_coins_to_give_change((self._coin_holder_transaction.get_sum() - item.get_price()))==-1:
            messagebox.showinfo("Błąd", "Tylko odliczona kwota.")
        else:
            change = self._coin_holder_transaction.get_sum() - item.get_price()
            item.set_quantity(item.get_quantity() - 1)
            self._coin_holder_deposit.add_coins(self._coin_holder_transaction.get_coins())
            self._coin_holder_transaction.clear()
            self.get_view('items').set_balance_text("0 zł")
            self.get_view('items').update_item_desc(item)
            if change > 0:
                change_coins = self.get_required_coins_to_give_change(change)
                self._coin_holder_deposit.remove_coins(change_coins)
                change_text = self.to_string(change_coins)
                messagebox.showinfo("Kupiono przedmiot", "Kupiłeś " + item.get_name() + " za " + str(
                    item.get_price() / 100) + "0 zł.\nReszta:" + change_text)
            else:
                messagebox.showinfo("Kupiono przedmiot", "Kupiłeś " + item.get_name() + " za " + str(
                    item.get_price() / 100) + "0 zł.")
            reset_choice = True
        if reset_choice:
            self.get_view('items').set_choice(0)
        pass
        self.show_me_baby()
        self.get_view('items').show_me_baby2()

    def on_click_button_check(self):
        item_id = self.get_view('items').get_choice()
        item = self.get_item(item_id)
        if item is None:
            messagebox.showinfo("Błąd", "Wybrano nieprawidłowy przedmiot.")
        else:
            messagebox.showinfo("Sprawdzenie ceny", "Cena przedmiotu " + item.get_name() + " wynosi " + str(
                item.get_price() / 100) + "0 zł.\n")



    def to_string(self, coins): # wypisuje resztę jako string
        text = ''
        if coins[0] > 0:
            text += str(coins[0]) + ' x 1 gr\n'
        if coins[1] > 0:
            text += str(coins[1]) + ' x 2 gr\n'
        if coins[2] > 0:
            text += str(coins[2]) + ' x 5 gr\n'
        if coins[3] > 0:
            text += str(coins[3]) + ' x 10 gr\n'
        if coins[4] > 0:
            text += str(coins[4]) + ' x 20 gr\n'
        if coins[5] > 0:
            text += str(coins[5]) + ' x 50 gr\n'
        if coins[6] > 0:
            text += str(coins[6]) + ' x 1 zł\n'
        if coins[7] > 0:
            text += str(coins[7]) + ' x 2 zł\n'
        if coins[8] > 0:
            text += str(coins[8]) + ' x 5 zł\n'
        return text

    def get_change(self, value): # wydaje resztę
        required_coins = self.get_required_coins_to_give_change(value)
        return required_coins

    def get_required_coins_to_give_change(self, value): # wylicza resztę
        coins_values = [500, 200, 100, 50, 20, 10, 5, 2, 1]
        required_coins = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        i = 0
        j = 0
        tmp = self._coin_holder_deposit.get_coins()
        tmp.reverse()
        print('----')
        # print(tmp)
        while value > 0:
            if(i == 9):
                return -1
            if value >= coins_values[i]:
                amount = value // coins_values[i]

                if(amount>tmp[i]):
                    for j in range(amount):
                        if(amount<=tmp[i]):
                            break
                        else:
                            if(amount>0):
                                amount-=1
                value -= coins_values[i] * amount
                required_coins[i] += amount

            i += 1
        return required_coins[::-1]


