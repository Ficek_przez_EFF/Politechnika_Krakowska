from abc import ABC, abstractmethod


class View(ABC):
    _frame = None
    controller = None

    def __init__(self, frame):
        super(View, self).__init__() # wywołanie konstruktora klasy bazowej
        self._frame = frame

    def set_controller(self, controller):
        self.controller = controller

    @abstractmethod
    def show(self): pass

    @abstractmethod
    def hide(self): pass
