import cmath

slownik={'bartek':34,'zenon':50,'xenon':-5,'zenobiusz':386,'serena':3}

wynik={key: 'inf' if value>200 else 8/(cmath.log10(value)) for key, value in slownik.items() if value>0}

print(wynik)
