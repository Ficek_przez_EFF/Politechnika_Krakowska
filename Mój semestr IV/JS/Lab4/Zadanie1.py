import math

print("Funkcje sin i cos przyjmują wartości w radianach")

def funkcja(x):
    krotka=(math.sin(x),math.cos(x))
    return krotka
#print(funkcja(1))

def wypisz():
    for i in range(0,91,9):
        radian=(math.pi/180)*i
        print("kąt: ",i," sin: ",funkcja(radian)[0]," cos: ",funkcja(radian)[1])
    return 0

wypisz()
    
