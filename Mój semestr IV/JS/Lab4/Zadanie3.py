from random import seed, randint

def blur(lista,wagi):
    lista1=lista[:]
    for x in range(1,len(lista)-1,1):
        lista1[x]=((lista[x-1]*wagi[0])+(lista[x]*wagi[1])+(lista[x+1]*wagi[2]))/3
        
    return lista1, lista, wagi
#print(blur([3,1,2,0,4], [1,1,1]))
#print(blur([3,1,2,0,4], [2,-1,2]))


def losowe(ile):
    liczby=[ randint(1,10) for x in range(ile)]
    return liczby

print(blur(losowe(5),losowe(3)))
