from random import seed, randint

kto_tab=[]
coz_tab=[]
jaki_tab=[]
co_tab=[]

with open("01_kto.txt",'r') as kto_open:
    for line in kto_open:
        kto_tab.append(line.strip())

with open("02_co_zrobil.txt",'r') as coz_open:
    for line in coz_open:
        coz_tab.append(line.strip())

with open("03_jaki.txt",'r') as jaki_open:
    for line in jaki_open:
        jaki_tab.append(line.strip())

with open("04_co.txt",'r') as co_open:
    for line in co_open:
        co_tab.append(line.strip())

dl_kto = len(kto_tab)
dl_coz = len(coz_tab)
dl_jaki = len(jaki_tab)
dl_co = len(co_tab)

def generate():
    wynik=open("wynik.txt",'w')
    for x in range (100):
        wynik.write("{xa} ".format(xa=x+1))
        wynik.write("{kto} {co_zrobil} {jaki} {co}".format(
            kto=kto_tab[randint(0,dl_kto-1)],
            co_zrobil = coz_tab[randint(0,dl_coz-1)],
            jaki=jaki_tab[randint(0,dl_jaki-1)],
            co=co_tab[randint(0,dl_co-1)],
            ))
        wynik.write("\n")        

    wynik.close()
generate()

