wybor=input('Wybierz język [pl,eng] : ')
if wybor=='pl':
    from pl import *
else:
    from eng import *

def funkcja():
    print(lang['hello'])
    try:
        liczba=int(input(lang['info']))
        if(liczba>0):
            wynik=liczba**(1/2)
            print(liczba,"**(1/2) = ",wynik)
            print(lang['bye'])
        else:
            print(lang['error'])
            print(lang['bye'])
    except ValueError:
        print(lang['error'])
        print(lang['bye'])
        
funkcja()
