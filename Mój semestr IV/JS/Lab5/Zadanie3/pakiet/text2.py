

def lewak(tekst,maks):
    print(tekst.ljust(maks))
    return 0
def prawak(tekst,maks):
    print(tekst.rjust(maks))
    return 0
def centerk(tekst,maks):
    print(tekst.center(maks))
    return 0

if __name__ == "__main__":
    print("Moduł 'text' definiuje funkcje center, left, right i służy do:")
    print("Wypisania tekstu wyrównanego do lewej,".ljust(80))
    print("do prawej,".rjust(80))
    print("Lub wycentrowanego.".center(80))
    print("")
    print("Dodatkowo ten moduł zawiera: ", dir())
    
