def fib_iter(ktory):
    poprzedni=1
    nastepny=1
    pomoc=1
    for x in range(ktory-2):
        pomoc=poprzedni+nastepny
        poprzedni=nastepny
        nastepny=pomoc
    return pomoc

def fib_rek(ktory):
    if(ktory<3):
        return 1
    else:
        return fib_rek(ktory-2)+fib_rek(ktory-1)



if __name__ == "__main__":
    print("Moduł 'fibonacci' definiuje funkcje które służą do:")
    print("1. Obliczania n-tego wyrazu ciągu fibonacciego iteracyjnie")
    print("2. Obliczania n-tego wyrazu ciągu fibonacciego rekurencyjnie")
    print("Dodatkowo ten moduł zawiera: ", dir())
