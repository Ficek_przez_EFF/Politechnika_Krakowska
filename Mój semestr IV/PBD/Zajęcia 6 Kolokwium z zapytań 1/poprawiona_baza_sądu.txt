CREATE TABLE SALA (
     ID_SALA     NUMBER(4)   CONSTRAINT PK_ID_SALA PRIMARY KEY,
     NAZWA      VARCHAR2(12),
     POZIOM_ZAB VARCHAR2(20)
);


CREATE TABLE SPECJALNOSC ( 
     ID_SPEC      NUMBER(4)   CONSTRAINT PK_ID_SPEC PRIMARY KEY,
     NAZWA    VARCHAR2(25)
          );


CREATE TABLE  TYP_SPRAWY( 
     ID_TYP     NUMBER(4)   CONSTRAINT PK_ID_TYP PRIMARY KEY,
     NAZWA    VARCHAR2(15)
); 



CREATE TABLE POZWANY ( 
     ID_POZ      NUMBER(4)   CONSTRAINT PK_ID_POZ PRIMARY KEY,
     NAZWISKO    VARCHAR2(15),
     IMIE        VARCHAR2(15),
     DATA_UR DATE,
     PESEL NUMBER (11),
     MIASTO VARCHAR2 (20)
);

CREATE TABLE OBSLUGA ( 
     ID_OBSLUGA      NUMBER(4)   CONSTRAINT PK_ID_OBSLUGA PRIMARY KEY,
     NAZWISKO    VARCHAR2(15),
     IMIE        VARCHAR2(15),
     ID_SPEC     NUMBER(4) CONSTRAINT FK_ID_SPEC REFERENCES SPECJALNOSC(ID_SPEC)
);


CREATE TABLE ROZPRAWA (
     ID_ROZPRAWA   NUMBER(4) CONSTRAINT PK_ID_ROZPRAWA PRIMARY KEY,
     ID_POZ  NUMBER(4) CONSTRAINT FK_ID_POZ REFERENCES POZWANY(ID_POZ),
     ID_TYP   NUMBER(4) CONSTRAINT FK_ID_TYP REFERENCES TYP_SPRAWY(ID_TYP),
     ID_OBSLUGA  NUMBER(4) CONSTRAINT FK_ID_OBSLUGA REFERENCES OBSLUGA(ID_OBSLUGA),   
     ID_SALA NUMBER(4) CONSTRAINT FK_ID_SALA REFERENCES SALA(ID_SALA),
     DATA DATE,
     KOSZT    NUMBER(6,2)
     );

-- WSTAWIANIE DANYCH


INSERT INTO POZWANY VALUES (1,'KOWALSKI','PIOTR',TO_DATE('2000-01-16','YYYY-MM-DD'),'00011234567','WARSZAWA');
INSERT INTO POZWANY VALUES (2,'NOWAK','JAN',TO_DATE('1999-01-23','YYYY-MM-DD'),'99012343561','WROCLAW');
INSERT INTO POZWANY VALUES (3,'ADAMSKI','PAWEL',TO_DATE('1965-11-16','YYYY-MM-DD'),'65119876452','POZNAN');
INSERT INTO POZWANY VALUES (4,'BRACKI','BOGDAN',TO_DATE('1987-09-16','YYYY-MM-DD'),'87094378690','GDANSK');
INSERT INTO POZWANY VALUES (5,'WOLIN','JAKUB',TO_DATE('1998-03-16','YYYY-MM-DD'),'98036712098','KATOWICE');
INSERT INTO POZWANY VALUES (6,'LIKA','DAWID',TO_DATE('1999-02-28','YYYY-MM-DD'),'99026712098','KIELCE');
INSERT INTO POZWANY VALUES (7,'ADAMCZYK','PAWEL',TO_DATE('1977-11-16','YYYY-MM-DD'),'77118967400','POZNAN');
INSERT INTO POZWANY VALUES (8,'BRANICKI','JAN',TO_DATE('1995-04-16','YYYY-MM-DD'),'95043478690','GDANSK');
INSERT INTO POZWANY VALUES (9,'LIS','JAN',TO_DATE('1998-12-16','YYYY-MM-DD'),'98127744098','KATOWICE');
INSERT INTO POZWANY VALUES (10,'KOWAL','STEFAN',TO_DATE('1994-01-16','YYYY-MM-DD'),'94016712098','KARNIOWICE');



INSERT INTO SPECJALNOSC VALUES (1,'ADWOKAT');
INSERT INTO SPECJALNOSC VALUES (2,'SEDZIA');
INSERT INTO SPECJALNOSC VALUES (3,'PROKURATOR');
INSERT INTO SPECJALNOSC VALUES (4,'OBRONCA');



INSERT INTO TYP_SPRAWY VALUES (1,'KARNA');
INSERT INTO TYP_SPRAWY VALUES (2,'GOSPODARCZA');
INSERT INTO TYP_SPRAWY VALUES (3,'RODZINNA');
INSERT INTO TYP_SPRAWY VALUES (4,'ZADLUZENIE');


INSERT INTO SALA VALUES (1,'F01','NISKI');
INSERT INTO SALA VALUES (2,'F02', 'WYSOKI');
INSERT INTO SALA VALUES (3,'F03', 'SREDNI');
INSERT INTO SALA VALUES (4,'F04','BARDZO WYSOKI');


INSERT INTO OBSLUGA VALUES (1,'BRANDYS','BOGDAN',1);
INSERT INTO OBSLUGA VALUES (2,'LISOWSKI','JAN',4);
INSERT INTO OBSLUGA VALUES (3,'KOWNACKI','STEFAN',3);
INSERT INTO OBSLUGA VALUES (4,'BRAMKA','BOGDAN',3);
INSERT INTO OBSLUGA VALUES (5,'LISEK','JAN',2);
INSERT INTO OBSLUGA VALUES (6,'PAWLICKI','STEFAN',4);
INSERT INTO OBSLUGA VALUES (7,'DRECZ','BOGDAN',2);
INSERT INTO OBSLUGA VALUES (8,'LIGARA','JAN',1);
INSERT INTO OBSLUGA VALUES (9,'ABRA','JAN',3);
INSERT INTO OBSLUGA VALUES (10,'LIN','KAROL',2);
INSERT INTO OBSLUGA VALUES (11,'AZALIN','STEFAN',1);
INSERT INTO OBSLUGA VALUES (12,'KOCA','PIOTR',1);



INSERT INTO ROZPRAWA VALUES (1,10,4,7,3,TO_DATE('2018-03-16', 'YYYY-MM-DD'),765);
INSERT INTO ROZPRAWA VALUES (2,2,3,6,1, TO_DATE('2018-01-16', 'YYYY-MM-DD'),897);
INSERT INTO ROZPRAWA VALUES (3,4,3,4,1,TO_DATE('2016-11-16', 'YYYY-MM-DD'),5431);
INSERT INTO ROZPRAWA VALUES (4,3,1,9,2, TO_DATE('2016-09-26', 'YYYY-MM-DD'), 1000);
INSERT INTO ROZPRAWA VALUES (5,8,4,4,4, TO_DATE('2017-04-06', 'YYYY-MM-DD'),875);
INSERT INTO ROZPRAWA VALUES (6,1,3,2, 3, TO_DATE('2017-02-16', 'YYYY-MM-DD'),908);
INSERT INTO ROZPRAWA VALUES (7,6,4,6,3, TO_DATE('2018-04-16', 'YYYY-MM-DD'),1234);
INSERT INTO ROZPRAWA VALUES (8,9,3,3, 2, TO_DATE('2017-02-01', 'YYYY-MM-DD'),8762);
INSERT INTO ROZPRAWA VALUES (9,5,2,1,4, TO_DATE('2017-03-29', 'YYYY-MM-DD'),2345);
INSERT INTO ROZPRAWA VALUES (10,1,3,12, 1, TO_DATE('2016-09-16', 'YYYY-MM-DD'),932);
INSERT INTO ROZPRAWA VALUES (13,9,2,5, 3, TO_DATE('2018-05-04', 'YYYY-MM-DD'),961);
INSERT INTO ROZPRAWA VALUES (14,6,1,8, 1, TO_DATE('2017-09-16', 'YYYY-MM-DD'),432);
INSERT INTO ROZPRAWA VALUES (15,3,3,11, 2, TO_DATE('2016-07-10', 'YYYY-MM-DD'),2908);
INSERT INTO ROZPRAWA VALUES (16,5,2,9, 3, TO_DATE('2017-03-16', 'YYYY-MM-DD'),4897);
INSERT INTO ROZPRAWA VALUES (17,7,1,4, 4, TO_DATE('2018-04-09', 'YYYY-MM-DD'),3210);
INSERT INTO ROZPRAWA VALUES (18,3,2,8, 3, TO_DATE('2017-04-26', 'YYYY-MM-DD'),765);
INSERT INTO ROZPRAWA VALUES (19,10,2,10, 2, TO_DATE('2017-12-16', 'YYYY-MM-DD'),3214);
INSERT INTO ROZPRAWA VALUES (20,1,3,1, 1, TO_DATE('2017-11-26', 'YYYY-MM-DD'),800);


