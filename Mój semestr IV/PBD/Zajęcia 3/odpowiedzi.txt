/****************************  Z A J E C I A     N U M E R    T R Z Y  **********************************************/
 
/*Zadanie 1*/
SELECT DISTINCT C.NAZWA, B.NAZWA
FROM CHARAKTER C, ZAJECIA Z, BUDYNEK B, SALA S
WHERE B.ID_BUDYNEK = S.ID_BUDYNEK AND S.ID_SALA = Z.ID_SALA AND Z.ID_CHARAKTER = C.ID_CHARAKTER AND B.NAZWA = 'Wydzia� Fizyki Matematyki i Iinformatyki';
 
/*Zadanie 2*/
SELECT DISTINCT LPAD(W.IMIE,20,'*') ||'  '|| LPAD(W.NAZWISKO,20,'*') ||'  '|| LPAD(T.NAZWA,10,'*') ||'  '|| LPAD(K.NAZWA,40,'*') AS PROFESOR
FROM WYKLADOWCA W, TYTULNAUKOWY T, KIERUNEK K, ZAJECIA Z, GRUPA G
WHERE W.ID_TYTUL = T.ID_TYTUL AND Z.ID_WYKLADOWCA = W.ID_WYKLADOWCA AND Z.ID_GRUPA = G.ID_GRUPA AND G.ID_KIERUNEK = K.ID_KIERUNEK;
 
/*ZADANIE 3*/
SELECT S.KODSALI ||'  '|| B.NAZWA AS GDZIE_WYKLADY
FROM SALA S, BUDYNEK B, CHARAKTER C, ZAJECIA Z
WHERE B.ID_BUDYNEK = S.ID_BUDYNEK AND S.ID_SALA = Z.ID_SALA AND Z.ID_CHARAKTER = C.ID_CHARAKTER AND C.NAZWA = 'Wyk�ady';
 
/*ZADANIE 4*/
SELECT S.NAZWISKO ||'  '|| S.IMIE ||'  '|| S.NRALBUMU ||'  '|| COUNT(O.ID_OCENA) AS LICZBA_OCEN_STUDENTA
FROM STUDENT S, OCENA O
WHERE S.ID_STUDENT = O.ID_STUDENT GROUP BY S.IMIE, S.NAZWISKO, S.NRALBUMU ;
 
/*ZADANIE 5*/
SELECT DZIENTYG, COUNT(ID_ZAJECIA)
FROM ZAJECIA
GROUP BY DZIENTYG
HAVING COUNT(ID_ZAJECIA) IN (SELECT MIN(COUNT(ID_ZAJECIA)) FROM ZAJECIA GROUP BY DZIENTYG);
 
/*ZADANIE 6*/
SELECT DISTINCT S.IMIE, S.NAZWISKO, A.ULICA, A.NRBUDYNKU, A.NRLOKALU, A.KODPOCZTOWY, A.MIASTO, COUNT(O.OCENA)
FROM STUDENT S, OCENA O, ADRES A
WHERE S.ID_STUDENT = O.ID_STUDENT AND S.ID_ADRES = A.ID_ADRES
GROUP BY S.IMIE, S.NAZWISKO, A.ULICA, A.NRBUDYNKU, A.NRLOKALU, A.KODPOCZTOWY, A.MIASTO
HAVING COUNT(O.OCENA) IN (SELECT MAX(COUNT(O.OCENA)) FROM STUDENT S, OCENA O, ADRES A
WHERE S.ID_STUDENT = O.ID_STUDENT AND S.ID_ADRES = A.ID_ADRES
GROUP BY S.IMIE, S.NAZWISKO, A.ULICA, A.NRBUDYNKU, A.NRLOKALU, A.KODPOCZTOWY, A.MIASTO);
 
/*ZADANIE 7*/
SELECT DISTINCT P.NAZWA, COUNT(O.OCENA)
FROM PRZEDMIOT P, OCENA O, ZAJECIA Z
WHERE P.ID_PRZEDMIOT = Z.ID_PRZEDMIOT AND Z.ID_ZAJECIA = O.ID_ZAJECIA
GROUP BY P.NAZWA
HAVING COUNT(O.OCENA) IN (SELECT COUNT(O.OCENA) FROM PRZEDMIOT P, OCENA O, ZAJECIA Z
WHERE P.ID_PRZEDMIOT = Z.ID_PRZEDMIOT AND Z.ID_ZAJECIA = O.ID_ZAJECIA
GROUP BY P.NAZWA );
 
/*ZADANIE 8*/
SELECT DISTINCT  P.NAZWA, COUNT(W.ID_WYKLADOWCA) AS ILOSC_WYKLADOWCOW
FROM PRZEDMIOT P, ZAJECIA Z, WYKLADOWCA W
WHERE Z.ID_PRZEDMIOT = P.ID_PRZEDMIOT AND Z.ID_WYKLADOWCA = W.ID_WYKLADOWCA
GROUP BY P.NAZWA
ORDER BY COUNT(W.ID_WYKLADOWCA) DESC;
/*HAVING COUNT(W.ID_WYKLADOWCA) IN (SELECT COUNT(W.ID_WYKLADOWCA) FROM PRZEDMIOT P, ZAJECIA Z, WYKLADOWCA W
WHERE Z.ID_PRZEDMIOT = P.ID_PRZEDMIOT AND Z.ID_WYKLADOWCA = W.ID_WYKLADOWCA
GROUP BY P.NAZWA)
ORDER BY COUNT(W.ID_WYKLADOWCA) DESC;*/
 
/*ZADANIE 9*/
SELECT DISTINCT G.NAZWA, COUNT(S.ID_STUDENT) AS ILOSC_STUDENTOW
FROM GRUPA G, STUDENT S, KIERUNEK K
WHERE S.ID_GRUPA = G.ID_GRUPA AND K.ID_KIERUNEK = G.ID_KIERUNEK AND K.NAZWA='Informatyka'
GROUP BY G.NAZWA
ORDER BY COUNT(S.ID_STUDENT) DESC;
 
/*ZADANIE 10*/
SELECT DISTINCT A.MIASTO, COUNT(S.ID_STUDENT)
FROM ADRES A, STUDENT S, GRUPA G, KIERUNEK K
WHERE S.ID_ADRES=A.ID_ADRES AND S.ID_GRUPA = G.ID_GRUPA AND G.ID_KIERUNEK = K.ID_KIERUNEK AND K.NAZWA = 'Matematyka'
GROUP BY A.MIASTO;