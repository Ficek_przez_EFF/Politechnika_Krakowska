/*ZADANIE 1*/
SELECT DISTINCT S.*, G.NAZWA, K.NAZWA, O.OCENA
FROM GRUPA G,KIERUNEK K, STUDENT S, OCENA O
WHERE G.ID_GRUPA = S.ID_GRUPA AND G.ID_KIERUNEK = K.ID_KIERUNEK AND O.ID_STUDENT = S.ID_STUDENT AND O.OCENA IN
(SELECT DISTINCT MAX(O.OCENA) FROM GRUPA G, KIERUNEK K, STUDENT S, OCENA O
WHERE G.ID_GRUPA = S.ID_GRUPA AND G.ID_KIERUNEK = K.ID_KIERUNEK AND O.ID_STUDENT = S.ID_STUDENT AND K.NAZWA = 'Informatyka');
 
 
/*ZADANIE 2*/
SELECT DISTINCT W.*, P.*
FROM WYKLADOWCA W, PRZEDMIOT P, ZAJECIA Z, OCENA O
WHERE W.ID_WYKLADOWCA = Z.ID_WYKLADOWCA AND Z.ID_PRZEDMIOT = P.ID_PRZEDMIOT AND P.ECTS>4 AND Z.ID_ZAJECIA = O.ID_ZAJECIA;
 
/*ZADANIE 3*/
select distinct st.imie, st.nazwisko, st.nralbumu, gr.nazwa as grupa, kr.nazwa as kierunek from student st,
grupa gr, kierunek kr where
st.id_grupa = gr.id_grupa and gr.id_kierunek = kr.id_kierunek
and gr.nazwa in(select gr.nazwa from student st, grupa gr, kierunek kr
where gr.id_kierunek = kr.id_kierunek and st.id_grupa = gr.id_grupa 
group by gr.nazwa
having count(st.id_student) > (select max(st.id_student)/max(gr.id_grupa) from student st, grupa gr));
 
 
/*ZADANIE 4*/
SELECT B.*, S.*
FROM BUDYNEK B, SALA S, WYKLADOWCA W, ZAJECIA Z
WHERE S.ID_BUDYNEK = B.ID_BUDYNEK AND 