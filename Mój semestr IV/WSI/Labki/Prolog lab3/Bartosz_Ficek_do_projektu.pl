

menu :-
  nl, write('*********************************************************************'), nl,
  write('*                                                                   *'), nl,
  write('*                Witaj, jestem Dobry Duszek Kacper.                 *'), nl,
  write('*       Pomoge Ci dobrac muzyke do Twoich aktualnych potrzeb.       *'), nl,
  write('*                                                                   *'), nl,
  write('*********************************************************************'), nl, nl.
 
main :-
  menu,
  odswiez,
  szukpiosenka(Wybor),
  odpowiedz(Wybor), nl.


szukpiosenka(Wybor) :-
  piosenka(Wybor), !.

:- dynamic(progress/2).

% ***************************************************************
% Czyscimy postep uzytkownika. 

odswiez :-
  retract(progress(_, _)),
  fail.
odswiez.


    
    % ***************************************************************
% Formatowanie, listy odpowiedzi

od([], _).
od([Pierwszy|Reszta], Index) :-
  write(Index), write(' '), answer(Pierwszy), nl,
  NextIndex is Index + 1,
  od(Reszta, NextIndex).


% ***************************************************************
% Analizuje index i zwraca odpowiednia odpowiedz


parse(0, [Pierwszy|_], Pierwszy).
parse(Index, [Pierwszy|Reszta], Response) :-
  Index > 0,
  NextIndex is Index - 1,
  parse(NextIndex, Reszta, Response).


% ***************************************************************
% Zadaje pytanie i zapisuje odpowiedz
ask(Question, Odpo, Choices) :-
  question(Question),
  od(Choices, 0),
  read(Index),
  parse(Index, Choices, Response),
  asserta(progress(Question, Response)),
  Response = Odpo.

% ***************************************************************
% Wszystkie mozliwe odpowiedzi


% ****************************************
% dla dziecka
answer(dla_dziecko) :-
  write('Dla dziecka').
 
answer(usypiam) :-
  write('Usypiam je').
  
answer(tancze) :-
  write('Tańczę z nim').
 
answer(maluszek) :-
  write('Mniej niż 5 lat').
 
answer(przedszkolak) :-
  write('Między 5 lat, a 9 lat'). 
  
answer(podstawczak) :-
  write('Więcej niz 9 lat').   

answer(bawie) :-
  write('Bawimy się').
  
answer(spiew) :-
  write('Chcemy pośpiewać').

% ****************************************
% do auta
  answer(auto) :-
  write('Do auta').
  
  answer(szybka) :-
  write('Jeżdżę szybko i dynamicznie').
  
  answer(normalna) :-
  write('Jeżdżę normalnie').
  
  answer(spokojna) :-
  write('Jeżdżę bardzo spokojnie').
  
  answer(niedzielny) :-
  write('Jestem niedzielnym kierowcą').

% ****************************************
% na impreze
answer(impreza) :-
  write('Na impreze').
  
  answer(wiej_po) :-
  write('Wiejska potańcówka').
  
   answer(wiej_po_tak) :-
  write('Tak').
  
   answer(wiej_po_nie) :-
  write('Nie').
  
  answer(klub_disco) :-
  write('Klub Disco-Polo').
  
  answer(domowka) :-
  write('Domówka').
  
  answer(bankiet) :-
  write('Kameralny bankiet').
  
  
  
 
% ****************************************
% na romantyczna kolacje
answer(kolacja) :-
  write('Romantyczna kolacja').
  
  answer(zona) :-
  write('Z żoną').
  
  answer(przyjaciolka) :-
  write('Z przyjaciółką').
  
  answer(kochanka) :-
  write('Z kochanką').

% ****************************************
% na silownie 
answer(silownia) :-
  write('Na siłownie').
  
% ****************************************  
% do relaksu
answer(relaks) :-
  write('Dla relaksu').
  
  answer(swietnie) :-
  write('Świetnie, mam mnóstwo energii. Potrzebuje czegoś żwawego').
  
  answer(dobrze) :-
  write('Dość dobrze, ale może być lepiej').
  
  answer(srednio) :-
  write('Średnio').
  
  answer(slabo) :-
  write('Fatalnie').
  
  answer(mysli) :-
  write('Mam myśli samobójcze').
  
  answer(pol) :-
  write('Polska').
  
  answer(zagra) :-
  write('Zagraniczna').
  
  answer(reg) :-
  write('Reggae').
  
  answer(jaz) :-
  write('Jazz').
  
  answer(trp) :-
  write('Trąbka').
  
  answer(klarn) :-
  write('Klarnet').
  
  answer(skrzyp) :-
  write('Skrzypce').
  
  answer(puz) :-
  write('Puzon').
  
  answer(saks) :-
  write('Saksofon').
  
  answer(git) :-
  write('Gitara').
  
  answer(cal) :-
  write('Cala orkiestra').
  
  answer(klas) :-
  write('Klasyczna').
  
  answer(elektro) :-
  write('Electro').
  
  answer(hip) :-
  write('Hip-Hop').

  answer(gosp) :-
  write('Gospel').

  answer(pope) :-
  write('Pop').


% ***************************************************************
% Warunki dla koncowych odpowiedzi

piosenka(kolysanka) :-
  do_czego(dla_dziecko),
  dziecko(usypiam).

piosenka(nied) :-
  do_czego(dla_dziecko),
  dziecko(tancze),
  lata_dziecka(maluszek).
  
piosenka(pszczol) :-
  do_czego(dla_dziecko),
  dziecko(tancze),
  lata_dziecka(przedszkolak).
  
  piosenka(popik) :-
  do_czego(dla_dziecko),
  dziecko(tancze),
  lata_dziecka(podstawczak).
  
piosenka(zabawa) :-
  do_czego(dla_dziecko),
  dziecko(bawie).
  
  piosenka(karaoke) :-
  do_czego(dla_dziecko),
  dziecko(spiew).
  
piosenka(szyb) :-
  do_czego(auto),
  styl(szybka).

piosenka(norm) :-
  do_czego(auto),
  styl(normalna).
 
  piosenka(spokoj) :-
  do_czego(auto),
  styl(spokojna).
  
  piosenka(niedz) :-
  do_czego(auto),
  styl(niedzielny).
  
  piosenka(baciar) :-
  do_czego(impreza),
  typ_imprezy(wiej_po),
  typ_wiej(wiej_po_nie).
  
  piosenka(rozne) :-
  do_czego(impreza),
  typ_imprezy(wiej_po),
  typ_wiej(wiej_po_tak).
  
  piosenka(disco) :-
  do_czego(impreza),
  typ_imprezy(klub_disco).
  
  piosenka(domow) :-
  do_czego(impreza),
  typ_imprezy(domowka).

   piosenka(bank) :-
  do_czego(impreza),
  typ_imprezy(bankiet).

    piosenka(kancelaria) :-
    do_czego(kolacja),
    z_kim(zona).
    
    piosenka(przyj) :-
    do_czego(kolacja),
    z_kim(przyjaciolka).

    piosenka(kochan) :-
    do_czego(kolacja),
    z_kim(kochanka).

    piosenka(silownie) :-
    do_czego(silownia).

    piosenka(stir) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(reg).
    
    piosenka(maleo) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(reg).
    
    piosenka(jazz_trp_pol) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(jaz),
    instr(trp).
    
     piosenka(jazz_trp_zagra) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(jaz),
    instr(trp).
    
    piosenka(jazz_klar_pol) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(jaz),
    instr(klarn).
    
     piosenka(jazz_klar_zagra) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(jaz),
    instr(klarn).
    
    piosenka(jazz_skrzy_pol) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(jaz),
    instr(skrzyp).
    
     piosenka(jazz_skrzy_zagra) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(jaz),
    instr(skrzyp).
    
     piosenka(jazz_puzon_pol) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(jaz),
    instr(puz).
    
     piosenka(jazz_puzon_zagra) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(jaz),
    instr(puz).
    
     piosenka(jazz_saksof_pol) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(jaz),
    instr(saks).
    
     piosenka(jazz_saksof_zagra) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(jaz),
    instr(saks).
    
    piosenka(jazz_gitara_pol) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(jaz),
    instr(git).
    
     piosenka(jazz_gitara_zagra) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(jaz),
    instr(git).
    
    piosenka(jazz_cala_pol) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(jaz),
    instr(cal).
    
     piosenka(jazz_cala_zagra) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(jaz),
    instr(cal).
    
    piosenka(swietnie_polska_klasyka) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(klas).
    
    piosenka(swietnie_zagra_klasyka) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(klas).
    
    piosenka(dosc_dobrze_polska_klasyka) :-
    samopoczucie(dobrze),
    krajo(pol),
    jaki_typ(klas).
    
    piosenka(dosc_dobrze_zagra_klasyka) :-
    samopoczucie(dobrze),
    krajo(zagra),
    jaki_typ(klas).
    
    piosenka(srednio_polska_klasyka) :-
    samopoczucie(srednio),
    krajo(pol),
    jaki_typ(klas).
    
    piosenka(srednio_zagra_klasyka) :-
    samopoczucie(srednio),
    krajo(zagra),
    jaki_typ(klas).
    
  piosenka(fatalnie_polska_klasyka) :-
    samopoczucie(slabo),
    krajo(pol),
    jaki_typ(klas).
    
    piosenka(fatalnie_zagra_klasyka) :-
    samopoczucie(slabo),
    krajo(zagra),
    jaki_typ(klas).
  
    piosenka(mysli_samo) :-
    samopoczucie(mysli).
    
    piosenka(swietnie_polska_electro) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(elektro).
    
    piosenka(swietnie_zagra_electro) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(elektro).
    
    piosenka(dosc_dobrze_polska_electro) :-
    samopoczucie(dobrze),
    krajo(pol),
    jaki_typ(elektro).
    
    piosenka(dosc_dobrze_zagra_electro) :-
    samopoczucie(dobrze),
    krajo(zagra),
    jaki_typ(elektro).
    
    piosenka(srednio_polska_electro) :-
    samopoczucie(srednio),
    krajo(pol),
    jaki_typ(elektro).
    
    piosenka(srednio_zagra_electro) :-
    samopoczucie(srednio),
    krajo(zagra),
    jaki_typ(elektro).
    
  piosenka(fatalnie_polska_electro) :-
    samopoczucie(slabo),
    krajo(pol),
    jaki_typ(elektro).
    
    piosenka(fatalnie_zagra_electro) :-
    samopoczucie(slabo),
    krajo(zagra),
    jaki_typ(elektro).
    
    
    
    piosenka(dosc_dobrze_polska_rege) :-
    samopoczucie(dobrze),
    krajo(pol),
    jaki_typ(reg).
    
    piosenka(dosc_dobrze_zagra_rege) :-
    samopoczucie(dobrze),
    krajo(zagra),
    jaki_typ(reg).
    
    piosenka(srednio_polska_rege) :-
    samopoczucie(srednio),
    krajo(pol),
    jaki_typ(reg).
    
    piosenka(srednio_zagra_rege) :-
    samopoczucie(srednio),
    krajo(zagra),
    jaki_typ(reg).
    
  piosenka(fatalnie_polska_rege) :-
    samopoczucie(slabo),
    krajo(pol),
    jaki_typ(reg).
    
    piosenka(fatalnie_zagra_rege) :-
    samopoczucie(slabo),
    krajo(zagra),
    jaki_typ(reg).
  
    
    
    piosenka(swietnie_polska_hiphop) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(hip).
    
    piosenka(swietnie_zagra_hiphop) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(hip).
    
    piosenka(dosc_dobrze_polska_hiphop) :-
    samopoczucie(dobrze),
    krajo(pol),
    jaki_typ(hip).
    
    piosenka(dosc_dobrze_zagra_hiphop) :-
    samopoczucie(dobrze),
    krajo(zagra),
    jaki_typ(hip).
    
    piosenka(srednio_polska_hiphop) :-
    samopoczucie(srednio),
    krajo(pol),
    jaki_typ(hip).
    
    piosenka(srednio_zagra_hiphop) :-
    samopoczucie(srednio),
    krajo(zagra),
    jaki_typ(hip).
    
  piosenka(fatalnie_polska_hiphop) :-
    samopoczucie(slabo),
    krajo(pol),
    jaki_typ(hip).
    
    piosenka(fatalnie_zagra_hiphop) :-
    samopoczucie(slabo),
    krajo(zagra),
    jaki_typ(hip).
    
    
    
    piosenka(swietnie_polska_gospel) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(gosp).
    
    piosenka(swietnie_zagra_gospel) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(gosp).
    
    piosenka(dosc_dobrze_polska_gospel) :-
    samopoczucie(dobrze),
    krajo(pol),
    jaki_typ(gosp).
    
    piosenka(dosc_dobrze_zagra_gospel) :-
    samopoczucie(dobrze),
    krajo(zagra),
    jaki_typ(gosp).
    
    piosenka(srednio_polska_gospel) :-
    samopoczucie(srednio),
    krajo(pol),
    jaki_typ(gosp).
    
    piosenka(srednio_zagra_gospel) :-
    samopoczucie(srednio),
    krajo(zagra),
    jaki_typ(gosp).
    
  piosenka(fatalnie_polska_gospel) :-
    samopoczucie(slabo),
    krajo(pol),
    jaki_typ(gosp).
    
  piosenka(fatalnie_zagra_gospel) :-
    samopoczucie(slabo),
    krajo(zagra),
    jaki_typ(gosp).
    
    
    
     piosenka(swietnie_polska_pop) :-
    samopoczucie(swietnie),
    krajo(pol),
    jaki_typ(pope).
    
    piosenka(swietnie_zagra_pop) :-
    samopoczucie(swietnie),
    krajo(zagra),
    jaki_typ(pope).
    
    piosenka(dosc_dobrze_polska_pop) :-
    samopoczucie(dobrze),
    krajo(pol),
    jaki_typ(pope).
    
    piosenka(dosc_dobrze_zagra_pop) :-
    samopoczucie(dobrze),
    krajo(zagra),
    jaki_typ(pope).
    
    piosenka(srednio_polska_pop) :-
    samopoczucie(srednio),
    krajo(pol),
    jaki_typ(pope).
    
    piosenka(srednio_zagra_pop) :-
    samopoczucie(srednio),
    krajo(zagra),
    jaki_typ(pope).
    
  piosenka(fatalnie_polska_pop) :-
    samopoczucie(slabo),
    krajo(pol),
    jaki_typ(pope).
    
    piosenka(fatalnie_zagra_pop) :-
    samopoczucie(slabo),
    krajo(zagra),
    jaki_typ(pope).
    
    

% ***************************************************************
% Wszystkie pytania

question(do_czego) :-
  write('Do czego muzyki potrzebujesz ?'), nl, nl.

question(dziecko) :-
  nl, write('Co robisz z dzieckiem ?'), nl, nl.
  
question(lata_dziecka) :-
  nl, write('Ile dziecko ma lat ?'), nl, nl.  
  
question(styl) :-
  nl, write('Jaki jest styl twojej jazdy ?'), nl, nl.

question(typ_imprezy) :-
  nl, write('Jakiego typu impreze planujesz ?'), nl, nl.
  
  question(typ_wiej) :-
  nl, write('Czy ta potańcówka jest organizowana w jakimś stylu?'), nl, nl.
  
question(z_kim) :-
  nl, write('Z kim ta kolacja?'), nl, nl.
  
  question(samopoczucie) :-
  nl, write('Jak się czujesz?'), nl, nl.
  
  question(krajo) :-
  nl, write('Jakiego wykonawce wybierasz ?'), nl, nl.
  
  question(jaki_typ) :-
  nl, write('Jaki typ muzyki wybierasz?'), nl, nl.
  
  question(instr) :-
  nl, write('Jaki instrument ma wystąpić w roli solisty?'), nl, nl.
  
  
    



% ***************************************************************
% Wszystkie odpowiedzi koncowe

  odpowiedz(kolysanka) :-
  nl, write('Może kołysanka, hmm? '), nl,
  write('Polecam kołysankę: \"Na Wojtusia z popielnika\" ').
  
  odpowiedz(nied) :-
  nl, write('Myślę, że najlepsza będzie zabawa z muzyką.'), nl,
  write('Spróbuj włączyć: \"Stary niedźwiedź mocno śpi\"').
  
  odpowiedz(pszczol) :-
  nl, write('Potańczcie do muzyki z bajek !'), nl,
  write('Proponuje: \"Zbigniew Wodecki - Pszczółka Maja\"').
  
  odpowiedz(popik) :-
  nl, write('Zatańczcie Taniec Belgijski !').
  
  odpowiedz(zabawa) :-
  nl, write('Poprostu cicho włącz radio i bawcie się w najlepsze.'), nl,
  write('(Podczas zabawy słuchaj dziecka, a nie muzyki !!! )').
  
  odpowiedz(karaoke) :-
  nl, write('Proponuję piosenki z zabaw takie jak \"Stary niedźwiedź\"'),nl,
  write('Warto też poszukać w serwisie YouTube, podkładów do karaoke'),nl,
  write('To będzie świetna zabawa !!!').
  
  odpowiedz(szyb) :-
  nl, write('Jestes wariatem na drodze, posłuchaj czegoś od pozytywnych wariatów'),nl,
  write('ACDC - Highway to hell ').
  
  odpowiedz(spokoj) :-
  nl, write('Dobrym wyborem będzie Jazz albo Blues'),nl,
  write('Miles Davis - Kind of Blue').
  
  odpowiedz(norm) :-
  nl, write('Posłuchaj Popu.'),nl,
  write('John Legend - All of Me').
  
  odpowiedz(niedz) :-
  nl, write('Nie myśl o muzyce !! Skup się na drodze !!!!').
  
  odpowiedz(baciar) :-
  nl, write('Skoro to zwykła wiejska potańcówka to puść jakąś biesiadę'),nl,
  write('\"Baciary - Lato\"').
  
  odpowiedz(rozne) :-
  nl, write('Przykro mi, że nie mogę znać wszystkich styli w których może być twoja impreza.'),nl,
  write('Mam jednak parę propozycji:'),nl,
  write('Lata 60-te \" Skaldowie - Prześliczna Wiolonczelistka \"'),nl,
  write('Lata 70-te \" Abba - Dancing Queen \"'),nl,
  write('Lata 80-te \" Queen - I Want to Break Free \"'),nl,
  write('Lata 90-te \" Mr. President - Coco Jambo \"'),nl,
  write('Impreza w stylu Hawajskim \" Israel Kamakawiwoʻole - Somewhere over the rainbow \"'),nl.

  odpowiedz(disco) :-
  nl, write('Nazwa klubu zobowiązuje.'),nl,
  write('\" Łobuzy - Ona czuje we mnie piniądz \"').
  
  odpowiedz(domow) :-
  nl, write('Proponuję coś co ruszy wszystkich do dobrej zabawy.'),nl,
  write('\" Sławomir - Miłość w Zakopanem \"').

  odpowiedz(bank) :-
  nl, write('Na taką okazję, musi być coś eleganckiego.'),nl,
  write('\" Zbigniew Wodecki - Opowiadaj mi tak \"').
  
  odpowiedz(kancelaria) :-
  nl, write('Zabierz żone tam, gdzie zasługuje !'),nl,
  write('\" Kancelaria - Zabiorę Cię \"').
  
  odpowiedz(przyj) :-
  nl, write('Wystarczy coś spokojnego i miłego !'),nl,
  write('\" Barry Manilow – Mandy \"').
  
  odpowiedz(kochan) :-
  nl, write('Lepiej się nie przyznawaj. NIE ŁADNIE !'),nl,
  write('\" Pectus – To, co chciałbym Ci dać \"').
  
  odpowiedz(silownie) :-
  nl, write('Do ćwiczeń polecam zabrać coś co do Ciebie przemawia,'),nl,
  write('Coś co Cię motywuję. Ja polecam: '),nl,
  write('\" DM4 - Bosski, Agresywna Masa - IDĘ NA CAŁOŚĆ \"'),nl,
  write('\" Kanye West – Stronger \"'),nl,
  write('\" Christina Aguilera – Candy Man \"').
  
  odpowiedz(stir) :-
  nl, write('\" Bob Marley - Stir it up \"').
  
  odpowiedz(maleo) :-
  nl, write('\" Maleo Reggae Rockers - Alibi \"').
  
  odpowiedz(jazz_trp_pol) :-
  nl, write('\" Piotr Wojtasik „Circle” \"').
  
  odpowiedz(jazz_trp_zagra) :-
  nl, write('\" Backrow Politics: Gordon Goodwin\'s Big Phat Band \"').
  
   odpowiedz(jazz_klar_pol) :-
  nl, write('\" Cafe Jazz Trio - Autumn leaves \"').
  
  odpowiedz(jazz_klar_zagra) :-
  nl, write('\" Sweet Georgia Brown - Benny Goodman \"').
  
   odpowiedz(jazz_skrzy_pol) :-
  nl, write('\" Michał Urbaniak - Manhattan Dream \"').
  
  odpowiedz(jazz_skrzy_zagra) :-
  nl, write('\" Ellington\'s Jazz Violin Session: Take The A Train \"').
  
   odpowiedz(jazz_puzon_pol) :-
  nl, write('\" Szymon Klekowicki - Puzon \"').
  
  odpowiedz(jazz_puzon_zagra) :-
  nl, write('\" Wycliffe Gordon plays SWING THAT MUSIC at CancerBlows 2015 \"').
  
  odpowiedz(jazz_saksof_pol) :-
  nl, write('\" Very Early ( Bill Evans) - Szymon Kamykowski \"').
  
  odpowiedz(jazz_saksof_zagra) :-
  nl, write('\" Epic Sax Guy \"').
  
  odpowiedz(jazz_gitara_pol) :-
  nl, write('\" Artur Lesicki Trio - demo \"').
  
  odpowiedz(jazz_gitara_zagra) :-
  nl, write('\" Victor Wooten wows with his performance of The Lesson solo live on EMGtv \"').
  
  odpowiedz(jazz_cala_pol) :-
  nl, write('\" Mack the knife (Warsaw Academic Big Band) \"').
  
  odpowiedz(jazz_cala_zagra) :-
  nl, write('\" Big Phat Band - The Jazz Police \"').
  
  odpowiedz(swietnie_polska_klasyka) :-
  nl, write('\" Wojciech Kilar - Polonez \"').
  
  odpowiedz(swietnie_zagra_klasyka) :-
  nl, write('\" Pirates of the Caribbean - He\'s a Pirate \"').
  
  odpowiedz(dosc_dobrze_polska_klasyka) :-
  nl, write('\" Michał Ogiński - Polonez F-dur "Todespolonäze" \"').
  
  odpowiedz(dosc_dobrze_zagra_klasyka) :-
  nl, write('\" Wynton Marsalis - Carnival of Venice \"').
  
  odpowiedz(srednio_polska_klasyka) :-
  nl, write('\" Fryderyk Chopin - Walc Des-dur op. 64 no. 1 \"').
  
  odpowiedz(srednio_zagra_klasyka) :-
  nl, write('\" W.A.Mozart Eine kleine Nachtmusik \"').
  
  odpowiedz(fatalnie_polska_klasyka) :-
  nl, write('\" F.F. Chopin - Preludium c-moll op. 28 nr 20 \"').
  
  odpowiedz(fatalnie_zagra_klasyka) :-
  nl, write('\" Ludwig van Beethoven - Sonata Księżycowa \"').
  
  odpowiedz(mysli_samo) :-
  nl, write('\" Porozmawiaj z kimś bliskim. Tutaj muzyka nie pomoże :/ \"').
  
  odpowiedz(swietnie_polska_electro) :-
  nl, write('\" 15 Minut Projekt - Najdluzszy Chillout W Miescie \"').
  
  odpowiedz(swietnie_zagra_electro) :-
  nl, write('\" Electro-Light - Symbolism \"').
  
  odpowiedz(dosc_dobrze_polska_electro) :-
  nl, write('\" Citadel of Mo - Desert of the Ice \"').
  
  odpowiedz(dosc_dobrze_zagra_electro) :-
  nl, write('\" Martin Garrix - Animals \"').
  
  odpowiedz(srednio_polska_electro) :-
  nl, write('\" Dziady Żoliborskie - 05 Tu stacja lęgowa bocian \"zero\" \"').
  
  odpowiedz(srednio_zagra_electro) :-
  nl, write('\" SKRILLEX - Bangarang \"').
  
  odpowiedz(fatalnie_polska_electro) :-
  nl, write('\" Electric Rudeboyz - Rzeczywistość \"').
  
  odpowiedz(fatalnie_zagra_electro) :-
  nl, write('\" Robert Miles - Children \"').
  
  odpowiedz(dosc_dobrze_polska_rege) :-
  nl, write('\" Junior Stress - Znam Ten Stan \"').
  
  odpowiedz(dosc_dobrze_zagra_rege) :-
  nl, write('\" Bob Marley - A lalala long \"').
  
  odpowiedz(srednio_polska_rege) :-
  nl, write('\" Junior Stress - Biały Miś \"').
  
  odpowiedz(srednio_zagra_rege) :-
  nl, write('\" Bob Marley - Is This Love \"').
  
  odpowiedz(fatalnie_polska_rege) :-
  nl, write('\" KAMIL BEDNAREK - JAK DŁUGO JESZCZE \"').
  
  odpowiedz(fatalnie_zagra_rege) :-
  nl, write('\" Peter Tosh - Why Must I Cry \"').
  
  odpowiedz(swietnie_polska_hiphop) :-
  nl, write('\" Gang Albanii - Riki Tiki \"').
  
  odpowiedz(swietnie_zagra_electro) :-
  nl, write('\" Eminem - River ft. Ed Sheeran \"').
  
  odpowiedz(dosc_dobrze_polska_hiphop) :-
  nl, write('\" Peja - Szczęście \"').
  
 odpowiedz(dosc_dobrze_zagra_hiphop) :-
  nl, write('\" Eminem - Rap God \"').
  
  odpowiedz(srednio_polska_hiphop) :-
  nl, write('\" Tede - Air Max Classic \"').
  
  odpowiedz(srednio_zagra_hiphop) :-
  nl, write('\" Migos - Bad and Boujee ft Lil Uzi Vert \"').
  
  odpowiedz(fatalnie_polska_hiphop) :-
  nl, write('\" Pezet - Gdyby miało nie być jutra \"').
  
  odpowiedz(fatalnie_zagra_hiphop) :-
  nl, write('\" Drake - God’s Plan \"').
  
  
 
  
  odpowiedz(swietnie_polska_gospel) :-
  nl, write('\" GOSPEL RAIN - W TOBIE JEST ŻYCIE \"').
  
  odpowiedz(swietnie_zagra_gospel) :-
  nl, write('\" God Is My Everything - Chicago Mass Choir \"').
  
  odpowiedz(dosc_dobrze_polska_gospel) :-
  nl, write('\" Gospel Rain, Łukasz Golec, Paweł Golec - OGIEŃ \"').
  
  odpowiedz(dosc_dobrze_zagra_gospel) :-
  nl, write('\" Soul with a capital S - Tower Of Power \"').
  
  odpowiedz(srednio_polska_gospel) :-
  nl, write('\" Gospel Rain, Beata Bednarz, Piotr Cugowski - EMMANUEL \"').
  
  odpowiedz(srednio_zagra_gospel) :-
  nl, write('\" Chicago Mass Choir - Jesus Promised \"').
  
  odpowiedz(fatalnie_polska_gospel) :-
  nl, write('\" Gospel Rain. Olga Szomańska - NIGDY NIE BĘDZIESZ SAM \"').
  
  odpowiedz(fatalnie_zagra_gospel) :-
  nl, write('\" Chicago Mass Choir - Whatever You Want \"').
  
  
  
  odpowiedz(swietnie_polska_pop) :-
  nl, write('\" Parostatek - Krzysztof Krawczyk \"').
  
  odpowiedz(swietnie_zagra_pop) :-
  nl, write('\" Ed Sheeran - Thinking Out Loud \"').
  
  odpowiedz(dosc_dobrze_polska_pop) :-
  nl, write('\" Ania Dabrowska - Porady Na Zdrady \"').
  
  odpowiedz(dosc_dobrze_zagra_pop) :-
  nl, write('\" Ed Sheeran - Perfect \"').
  
  odpowiedz(srednio_polska_pop) :-
  nl, write('\" Poluzjanci - Nie ma nas \"').
  
  odpowiedz(srednio_zagra_pop) :-
  nl, write('\" Meghan Trainor - Like I\'m Gonna Lose You \"').
  
  odpowiedz(fatalnie_polska_pop) :-
  nl, write('\" Perfect - Wszystko ma swój czas \"').
  
  odpowiedz(fatalnie_zagra_pop) :-
  nl, write('\" Sam Smith - Lay Me Down \"').
 

% ***************************************************************
% Dopasowanie odpowiedzi do pytan

  do_czego(Odpo) :-
  progress(do_czego, Odpo).
  do_czego(Odpo) :-
  \+ progress(do_czego, _),
  ask(do_czego, Odpo, [dla_dziecko, auto, impreza, kolacja, silownia, relaks]).
  
  dziecko(Odpo) :-
  progress(dziecko, Odpo).
  dziecko(Odpo) :-
  \+ progress(dziecko, _),
  ask(dziecko, Odpo, [usypiam, tancze, bawie, spiew]).
  
  lata_dziecka(Odpo) :-
  progress(lata_dziecka, Odpo).
  lata_dziecka(Odpo) :-
  \+ progress(lata_dziecka, _),
  ask(lata_dziecka, Odpo, [maluszek, przedszkolak, podstawczak]).
  
  styl(Odpo) :-
  progress(styl, Odpo).
  styl(Odpo) :-
  \+ progress(styl, _),
  ask(styl, Odpo, [szybka, normalna, spokojna, niedzielny]).

  typ_imprezy(Odpo) :-
  progress(typ_imprezy, Odpo).
  typ_imprezy(Odpo) :-
  \+ progress(typ_imprezy, _),
  ask(typ_imprezy, Odpo, [wiej_po, klub_disco, domowka, bankiet]).
  
  typ_wiej(Odpo) :-
  progress(typ_wiej, Odpo).
  typ_wiej(Odpo) :-
  \+ progress(typ_wiej, _),
  ask(typ_wiej, Odpo, [wiej_po_tak,wiej_po_nie]).
  
  z_kim(Odpo) :-
  progress(z_kim, Odpo).
  z_kim(Odpo) :-
  \+ progress(z_kim, _),
  ask(z_kim, Odpo, [zona, przyjaciolka, kochanka]).
  
  samopoczucie(Odpo) :-
  progress(samopoczucie, Odpo).
  samopoczucie(Odpo) :-
  \+ progress(samopoczucie, _),
  ask(samopoczucie, Odpo, [swietnie ,dobrze ,srednio ,slabo ,mysli ]).

  krajo(Odpo) :-
  progress(krajo, Odpo).
  krajo(Odpo) :-
  \+ progress(krajo, _),
  ask(krajo, Odpo, [pol,zagra]).
  
  jaki_typ(Odpo) :-
  progress(jaki_typ, Odpo).
  jaki_typ(Odpo) :-
  \+ progress(jaki_typ, _),
  ask(jaki_typ, Odpo, [reg, jaz, klas, elektro, hip, gosp, pope]).
  
  instr(Odpo) :-
  progress( instr, Odpo).
   instr(Odpo) :-
  \+ progress( instr, _),
  ask( instr, Odpo, [trp, klarn, skrzyp, puz, saks, git, cal]).

