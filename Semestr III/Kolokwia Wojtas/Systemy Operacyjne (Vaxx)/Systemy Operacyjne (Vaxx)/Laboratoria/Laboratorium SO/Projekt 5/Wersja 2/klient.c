#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "pthread.h"
#define MAX_LENGTH 25  //maksymalna dlugosc wiadomosci

typedef struct StrukturaWiadomosci
{
    long int odbiorca; // serwer==1
    
    long int nadawca;
   
    char wiadomosc[MAX_LENGTH];
} StrukturaWiadomosci;

void msg_rem(int msg_id);

//funkcja zmieniajaca wielkosc liter
char *powiekszacz(char *gdzie, char *skad, size_t n);

int kol_id;
void my_exit();
void *nadawca();
void *odbiorca();


//***********MAIN******************

int main()
{
	key_t key;
    key = ftok(".", 'Z');
    if(key == -1)
    {
        perror("Blad utworzenia klucza!");
        exit(-1);
    }
    
    
    kol_id = msgget(key, 0600);
    if(-1 == kol_id)
    {
        perror("Blad podlaczenia do kolejki komunikatow! \n Najprawdopodobniej nie uruchomiles najpierw serwera!");
        exit(-1);
    }
    printf("Podlaczylem sie do kolejki! \n");
    
    
    signal(SIGINT, my_exit);
    signal(SIGTERM, my_exit);
    
    //tworzymy odzielne watki dla nadawcy i odbiorcy
    pthread_t watek_nadawcy, watek_odbiorcy;
    
    if(pthread_create(&watek_nadawcy, NULL, nadawca, NULL) != 0)
    {
        perror("Blad utowrzenia watku nadawcy!");
        exit(-2);
    }
    
    if(pthread_create(&watek_odbiorcy, NULL, odbiorca, NULL) != 0)
    {
        perror("Blad utworzenia watku odbiorcy!");
        exit(-2);
    }
    
    if(pthread_join(watek_nadawcy, NULL) != 0)
    {
        perror("Blad podlaczenia watku nadawcy!");
        exit(-1);
    }
    
    if(pthread_join(watek_odbiorcy, NULL) != 0)
    {
        perror("Blad podlaczenia watku odbiorcy!");
        exit(-1);
    }
    
}

void my_exit()
{
   
    printf("\n KLIENT: %d SIE ZAKONCZYL!.\n", getpid());
    exit(0);
}

void *nadawca()
{
    struct msqid_ds buf;
    
    StrukturaWiadomosci msg;
    int r;
    
    // obliczamy maksymalna pojemnosc kolejki
    msgctl(kol_id, IPC_STAT, &buf);
    int max = buf.msg_qbytes / (sizeof(StrukturaWiadomosci)-sizeof(long int));
    // pojemnosc musi byc mniejsza o jeden, tak by serwer zawsze mial miejsce
    // na swoja odpowiedz
    max--;
    
    while(1)
    {
        
        printf("Podaj wiadomosc: \n");
        fgets(msg.wiadomosc, MAX_LENGTH, stdin);
        
        msg.odbiorca = 1;       // serwer
        msg.nadawca = getpid();  // klient
        
        
        //Sprawdzamy czy mamy miejsce w kolejce na wyslanie nowej wiadomosci 
        msgctl(kol_id, IPC_STAT, &buf);
        
        // numer wiadomosci jest rowny limitowi
        if(buf.msg_qnum >= max)
        {
            printf("Brak miejsca na nowa wiadomosc!!\n Musimy poczekac na zwolnienie miejsca!\n");
            
            // sprawdzamy czy juz mamy miejsce
            while(buf.msg_qnum >= max)
            {
                msgctl(kol_id, IPC_STAT, &buf);
                sleep(1);
            }
        }
        
        // wysylanie wiadomosci
        while((r = msgsnd(kol_id, &msg, (sizeof(StrukturaWiadomosci)-sizeof(long int)), IPC_NOWAIT)) == -1 && errno == EINTR);
        
        if(-1 == r)
        {
            if(errno == EAGAIN)
            {
                
                perror("Zapchano kolejke!\n Nie mozna wyslac wiadomosci!");
                
                // czekamy na odblokowanie kolejki i probujemy ponownie wyslac wiadomosc
                while((r = msgsnd(kol_id, &msg, (sizeof(StrukturaWiadomosci)-sizeof(long int)), 0)) == -1 && (errno == EAGAIN || errno == EINTR));
                
                if(r == -1)
                {
                    
                    perror("Blad wysylania wiadomosci!");
                    exit(-1);
                }
            }
            else
            {
                perror("Blad wysylania wiadomosci!");
                exit(-1);
            }
        }
        
        printf("***************Wyslalem wiadomosc!*********************\n");
        
    }
}

void *odbiorca()
{
    StrukturaWiadomosci msg;
    int r;
    
    while(1)
    {
      
        while((r = msgrcv(kol_id, &msg, (sizeof(StrukturaWiadomosci)-sizeof(long int)), getpid(), 0)) == -1 && errno == EINTR);
        
        if(-1 == r)
        {
            perror("Blad odbioru wiadomosci!");
            exit(-1);
        }
        
        printf("Otrzymalem odpowiedz:\n%s\n", msg.wiadomosc);
    }
}

//******************definicje funkcji*********************

void msg_rem(int msg_id)
{
    if(-1 == msgctl(msg_id, IPC_RMID, 0))
    {
        perror("Blad usuwania kolejki komunikatow!");
        exit(-2);
    }
    
    printf("Usunalem kolejke komunikatow: %d\n", msg_id);
}


char *powiekszacz(char *gdzie, char *skad, size_t n)
{
    if(!n)
    {
        return 0;
    }
    
    char *d = gdzie;
    
    while(*skad && --n>0)
    {
        *d++ = toupper(*skad++);
    }
    
    *d = 0;
    return gdzie;
}
