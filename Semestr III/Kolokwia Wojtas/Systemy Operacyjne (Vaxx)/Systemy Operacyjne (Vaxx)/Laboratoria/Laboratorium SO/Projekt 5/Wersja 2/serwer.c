#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#define MAX_LENGHT 25  //maksymalna dlugosc wiadomosci

typedef struct StrukturaWiadomosci
{
    long int odbiorca; // serwer==1
    
    long int nadawca;
   
    char wiadomosc[MAX_LENGHT];
} StrukturaWiadomosci;

void msg_rem(int msg_id);

//funkcja zmieniajaca wielkosc liter
char *powiekszacz(char *gdzie, char *skad, size_t n);

int kol_id; //id kolejki

// przechwytuje ctrl+c
void my_exit();

//************MAIN***************

int main()
{
	key_t key;
    key = ftok(".", 'Z');
    if(key == -1)
    {
        perror("Blad utworzenia klucza!");
        exit(-1);
    }
    
    
    kol_id = msgget(key, IPC_CREAT|IPC_EXCL|0600);
    if(-1 == kol_id)
    {
        perror("Blad utworzenia kolejki komunikatow!");
        exit(-1);
    }
    printf("Utworzylem kolejke komunikatow!\n");
    
    signal(SIGINT, my_exit);
    signal(SIGTERM, my_exit);

    StrukturaWiadomosci msg;
    int r;
    
    while(1)
    {
        printf("Czekam na wiadomosc... \n");
        while((r = msgrcv(kol_id, &msg,(sizeof(StrukturaWiadomosci)-sizeof(long int)), 1, 0)) == -1 && errno == EINTR);
      
        if(-1 == r)
        {
            perror("Blad odbioru wiadomosci!");
            msg_rem(kol_id);
            exit(-1);
        }
        
        printf("Mam wiadomosc od %ld o tresci:\n%s", msg.nadawca, msg.wiadomosc);
        msg.odbiorca = msg.nadawca;
        powiekszacz(msg.wiadomosc, msg.wiadomosc, sizeof(msg.wiadomosc));
        
        while((r = msgsnd(kol_id, &msg, (sizeof(StrukturaWiadomosci)-sizeof(long int)), IPC_NOWAIT)) == -1 && errno == EINTR);
        
        if(-1 == r)
        {
            perror("Blad wysylania wiadomosci!");
            msg_rem(kol_id);
            exit(-1);
        }
        
        printf("Wyslalem wiadomosc zwrotna do %ld\n", msg.nadawca);
    }
}

void my_exit()
{
    msg_rem(kol_id);
    
    printf("\n KONIEC PRACY SERWERA!! \n");
    exit(0);
}


//******************definicje funkcji*********************

void msg_rem(int msg_id)
{
    if(-1 == msgctl(msg_id, IPC_RMID, 0))
    {
        perror("Blad usuwania kolejki komunikatow!");
        exit(-2);
    }
    
    printf("Usunalem kolejke komunikatow: %d\n", kol_id);
}


char *powiekszacz(char *gdzie, char *skad, size_t n)
{
    if(!n)
    {
        return 0;
    }
    
    char *d = gdzie;
    
    while(*skad && --n>0)
    {
        *d++ = toupper(*skad++);
    }
    
    *d = 0;
    return gdzie;
}
