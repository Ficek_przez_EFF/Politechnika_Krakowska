#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<errno.h>
#include<string.h>
#include<signal.h>

#define MAX_MSG_SIZE 256			//maksymalny rozmiar wiadomosci

key_t klucz;					//klucz kolejki
int kolejka_id;					//identyfikator kolejki
pid_t my_pid;					//zmienne pomocnicze

void sig_ctrlc();

/********** STRUKTURA KOMUNIKATOW **********/

struct komunikat {
long   typ;
pid_t  nadawca;
char   wiadomosc[MAX_MSG_SIZE];
};

//----------- main --------------

int main(int argc, char * argv[])
{

/********** SYGNAL  CTRL + c **********/

 	signal(SIGINT,sig_ctrlc);

/********** TWORZENIE KLUCZA KOLEJKI**********/

    	klucz = ftok(".", 1234);

	if (klucz == (key_t)-1)
	{
		printf("SERWER: Blad tworzenia klucza \n");
		exit(1);
	}

/********** TWORZENIE KOLEJKI **********/

    	kolejka_id = msgget(klucz, 0600 | IPC_CREAT );

	if (kolejka_id == -1)
	{
		printf("SERWER: Blad tworzenia kolejki \n");
		exit(1);
	}

    	my_pid = getpid();

/********** ID SERWERA i KOLEJKI **********/
    	printf("PID serwera: %d\n", my_pid);
    	printf("ID kolejki: %d\n", kolejka_id);

	struct komunikat msg_buf;			//tworzenie - struktura komunikatu

	while (1)
	{
		memset(&msg_buf, 0, sizeof(msg_buf));	//wypelnienie zerami

		printf("Czekam na wiadomosc....\n");

/********** POBIERANIE KOMUNIKATU **********/

        if (msgrcv(kolejka_id, &msg_buf, (sizeof(msg_buf) + sizeof(long)-1), 1/*mtype*/, 0/*flags*/) == (ssize_t)-1)
		{
			printf("SERWER: Blad pobrania komunikatu od klienta. \n");
			exit(1);
		}

        printf("\tOdebrana wiadomosc od procesu z PID'em' %d: %s\n", msg_buf.nadawca, msg_buf.wiadomosc);

/********** ZAMIANA na DUZE LITERY **********/

		int k;
		for(k=0; k<strlen(msg_buf.wiadomosc); k++)
		{
			msg_buf.wiadomosc[k] = toupper(msg_buf.wiadomosc[k]);
		}

/********** WYSYLANIE KOMUNIKATU DO KLIENTA **********/

		msg_buf.typ = msg_buf.nadawca;
		msg_buf.nadawca = my_pid;

        if (msgsnd(kolejka_id, &msg_buf, sizeof(msg_buf) + sizeof(long)-1, 0) == -1)
		{
			printf("SERWER: Blad wyslania komunikatu \n");
			exit(1);
		}
	}
}


/********** Funkcja Ctrl + c **********/

void sig_ctrlc()
{
/********** USUWANIE KOLEJKI **********/

    if((msgctl(kolejka_id, IPC_RMID,0)) == -1)
    {
            printf("SERWER: Blad usuwania kolejki komunikatow: %i (%s)\n",errno,strerror(errno));
            exit(-1);
        }
    else
    {
        printf("Koniec pracy serwera. Kolejka zostala usunieta.");
        exit(0);
    }
}
