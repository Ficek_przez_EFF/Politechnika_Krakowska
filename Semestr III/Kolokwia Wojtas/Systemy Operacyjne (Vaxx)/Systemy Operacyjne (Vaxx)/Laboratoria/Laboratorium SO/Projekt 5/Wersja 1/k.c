#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<sys/sem.h>
#include<errno.h>
#include<pthread.h>

#define MAX_MSG_SIZE 256		//maksymalny rozmiar wiadomosci

//Globalne

key_t klucz;				//klucz do kolejki
int kolejka_id;				//identyfikator kolejki
pid_t server_pid, my_pid;		//zmienna pomocnicza


/********** TWORZENIE WATKOW **********/
pthread_t tid;
pthread_t tid2;

/********** STRUKTURA KOMUNIKATU **********/

struct komunikat {
    long   typ;
    pid_t  nadawca;
    char   wiadomosc[MAX_MSG_SIZE];
};

void * wysylanie_komunikatu();
void * odbieranie_komunikatu();


// --------- main ---------------

int main(int argc, char * argv[])
{
        my_pid = getpid();

/********** TWORZENIE KLUCZA dla KOLEJKI KOMUNIKATOW**********/

        klucz = ftok(".", 1234);
  
	if (klucz == (key_t)-1)
	{
		printf("KLIENT: Blad tworzenia klucza\n");
		exit(1);
	}
	
/********** TWORZENIE KOLEJKI **********/

        kolejka_id = msgget(klucz, 0600 | IPC_CREAT);

	if (kolejka_id == -1)
	{
		printf("KLIENT: Blad tworzenia kolejki komunikatow\n");
		exit(1);
	}

/********** WYPISANIE ID KLIENTA i KOLEJKI **********/

        printf("PID klienta: %d\n", getpid());
        printf("ID kolejki: %d\n",kolejka_id);

/********** TWORZENIE - WATEK NADAJACY KOMUNIKAT **********/ 
    
        if (pthread_create(&tid2, NULL, wysylanie_komunikatu, NULL) != 0)
        {
       		printf("KLIENT: Blad tworzenia watku (wysylanie_komunikatu)\n");
        	exit(1);
        }

/********** TWORZENIE - WATEK ODBIERAJACY KOMUNIKAT **********/ 

    	if (pthread_create(&tid, NULL, odbieranie_komunikatu, NULL) != 0)
    	{
        	printf("KLIENT: Blad tworzenia watku (odbieranie_komunikatu)\n");
        	exit(1);
    	}

/********** PRZYLACZANIE WATKOW **********/

    	if(pthread_join(tid, NULL)) 
    	{       
		printf("KLIENT: Blad przylaczenia watku (wysylanie_komunikatu):  %i (%s)\n", errno, strerror(errno));
        	exit(2);
        }

    	if(pthread_join(tid2, NULL)) 
    	{
        	printf("Blad przylaczenia watku (odbieranie_komunikatu):  %i (%s)\n", errno, strerror(errno));
        	exit(2);
    	}	


  return 0;
}

/********** WYSYLANIE **********/ 

void * wysylanie_komunikatu()
{

    struct komunikat msg_snd;			//struktura

    while(1)
    {

    	msg_snd.typ = 1;			//wszyscy klienci do jednego serwera
    	msg_snd.nadawca = my_pid;

    	char buf[MAX_MSG_SIZE];

    	//printf("Komunikat moze posiadac max: %d znakow.\n", (MAX_MSG_SIZE-1));


/**********  WYSLANIE KOMUNIKATU do SERWERA **********/

    	printf("Podaj komunikat: ");

    	int i=0;
    	int c;

    	while ( (c=fgetc(stdin)) != '\n' )
    	{

        	if ( i<(MAX_MSG_SIZE - 1) )
		{
            		buf[i] = c;
        	}

        	i++;
    	}

    	if ( i>=(MAX_MSG_SIZE-1))				
    	{
        	buf[(MAX_MSG_SIZE - 1)] = '\0';			//Znak końca
    	}
    	else
    	{
        	buf[i] = '\0';
    	}

    	//printf("\nWprowadzonych znakow: %d\n", i);

    	if ( i>=(MAX_MSG_SIZE-1) )
    	{
        	printf("Pominietych: %d\n", i - (MAX_MSG_SIZE-1));
    	}

    	printf("\tTresc komunikatu KLIENTA: \"%s\" \n",buf);

    	memset(msg_snd.wiadomosc, 0, MAX_MSG_SIZE); 		//Wypelnienie bajtow zerami
    	strcpy(msg_snd.wiadomosc, buf);				//Kopiowanie komunikatu

    	if (msgsnd(kolejka_id, &msg_snd, sizeof(msg_snd) +  sizeof(long)-1,0) == -1)
    	{
        	perror("KLIENT : Blad podczas wysylania \n");
        	exit(1);
    	}

	usleep(1);
 }

 pthread_exit((void *) 0);

}

/********** ODBIERANIE **********/ 

void * odbieranie_komunikatu(void * arg)
{
    struct komunikat msg_rcv;

    while(1)
    {
            
        if (msgrcv(kolejka_id, &msg_rcv, (sizeof(msg_rcv) + sizeof(long)-1),getpid(),0) ==(ssize_t) -1)
        {
            printf("KLIENT: Blad pobrania komunikatu \n");
            exit(1);
        }

        printf("\tOdpowiedz SERWERA: %s \n", msg_rcv.wiadomosc);
      
    }
pthread_exit((void *) 0);

}
