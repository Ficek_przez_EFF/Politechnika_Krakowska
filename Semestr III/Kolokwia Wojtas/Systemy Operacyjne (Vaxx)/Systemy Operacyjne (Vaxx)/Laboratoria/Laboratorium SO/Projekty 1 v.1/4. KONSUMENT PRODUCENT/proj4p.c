#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>

/* 

   JWojtas pyta� do czego s�u�� u�yte semafory i gdzie korzystam z semafora sygnalizacuj�cego koniec pracy konsumenta
   
   Uruchamianie: ./proj4k& ./proj4p lub ./proj4k (w jednym terminalu) ./proj4p (w drugim terminalu) 

*/


static void opusc_semafor(int, short);
static void podnies_semafor(int, short);

int main() 
{
//tworzenie klucza
key_t klucz;
if(! (klucz = ftok(".",'F')))
        {
        printf("Blad tworzenia klucza /producent");
        exit(-2);
        }

//uzyskanie dostepu do zbioru semaforow
int id_semafora;
if((id_semafora = semget(klucz, 4, 0600 | IPC_CREAT))==-1)
	{
	printf("Blad uzyskania dostepu do zbioru semaforow /producent\n");
	exit(-3);
	}	
	else
	printf("Proces %d uzyskal dostep do zbioru semaforow /producent\n", getpid());

//inicjowanie semaforow
int tab[4]={1,0,1,0};
/* 
 * 0: semafor producenta
 * 1: semafor konsumneta
 * 2: sygnalizacja zakonczenia prodecenta
 * 3: sygnalizacja odczytu ostatniego
*/

int ii;
for(ii=0;ii<4;ii++)
{
	if(semctl(id_semafora,ii,SETVAL,tab[ii])==-1) 
	{ 
	printf("Blad inicjalizacji sem.!\n"); exit(-1); }
}	

//kojarzenie zmiennej z plikiem "wejscie"
FILE *wejscie, *bufor;
if((wejscie=fopen("wejscie", "r"))==NULL) {
	printf("Nie moge otworzyc pliku wejscie do odczytu! /producent\n");
	exit(1);
	}

//zapisywanie znak po znaku zawartosci pliku wejsciowego do buforu
char znak;
while(znak!=EOF)
{
	opusc_semafor(id_semafora, 0);
	if((bufor=fopen("bufor", "w"))==NULL) 
	{
        	printf("Nie moge otworzyc pliku bufor do nadpisania /producent!\n");
        	exit(1);
        }
	znak = fgetc(wejscie);
	if(ferror(wejscie)==EOF) 
	{
		printf("Blad odczytu znaku /producent\n"); 
		exit(-9);
	}
	printf("Odczytany znak: %c /producent\n", znak);
	fputc(znak, bufor);
	if(ferror(bufor)==EOF) 
	{
		printf("Blad zapisu znaku do bufora /producent\n"); 
		exit(-9);
	}
	if(fclose(bufor)==EOF) 
	{
		printf("Blad zamykania bufora /producent\n"); 
		exit(-1);
	}	
	
	if(znak==EOF)
		opusc_semafor(id_semafora, 2);
	podnies_semafor(id_semafora, 1);
}

//opuszczenie semafora po przekazaniu ostatniego znaku do bufora
printf("Zakonczenie kopiowania do bufora /producent\n");
//opusc_semafor(id_semafora, 2);
opusc_semafor(id_semafora, 3);

//zamykanie pliku wejsciowego
if(fclose(wejscie)==EOF) 
{
	printf("Blad zamykania wejscia /producent\n"); 
	exit(-1);
}

//usuwanie semafora
if (semctl(id_semafora,4,IPC_RMID)==-1)
{
        printf("Nie mozna usunac zbioru semaforow /producent\n");
        exit(-6);
}
else
        printf("Zbior semaforow zostal usuniety, id: %d /producent\n", id_semafora);
                        

return 0;
}

static void opusc_semafor(int id_semafora, short nr_sem)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=nr_sem;
    bufor_sem.sem_op=-1;
    bufor_sem.sem_flg=0;

    while(1) {
    zmien_sem=semop(id_semafora,&bufor_sem,1);
    if (zmien_sem==0 || errno != 4) //eerno == 4 -> Interrupted system call
		break;
    }
	
    if(zmien_sem==-1) {
	if(errno != 4) {
		perror("Blad opuszczania semafora /producent\n");
		exit(-5);
		}
	}
    else
    printf("Semafor %d zostal opuszczony /producent\n", nr_sem);


}

static void podnies_semafor(int id_semafora, short nr_sem)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=nr_sem;
    bufor_sem.sem_op=1;
    bufor_sem.sem_flg=0;

    while(1) {
    zmien_sem=semop(id_semafora,&bufor_sem,1);
    if (zmien_sem==0 || errno !=4) //eerno == 4 -> Interrupted system call
		break;
    }
	
    if(zmien_sem==-1) {
    if(errno != 4) {
        perror("Blad podnoszenia  semafora /producent\n");
		exit(-5);
		
                }
        }
    else
	printf("Semafor %d podniesiony /producent\n", nr_sem);
}

