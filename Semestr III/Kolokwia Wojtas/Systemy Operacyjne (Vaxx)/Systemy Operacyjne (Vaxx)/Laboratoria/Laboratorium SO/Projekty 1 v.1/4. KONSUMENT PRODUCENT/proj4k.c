#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>

static void opusc_semafor(int, short);
static void podnies_semafor(int, short);

int main() 
{
//tworzenie klucza
key_t klucz;
if(! (klucz = ftok(".",'F')))
        {
        printf("Blad tworzenia klucza /konsument");
        exit(-2);
        }

//uzyskanie dostepu do zbioru emaforow
int id_semafora;
if((id_semafora = semget(klucz, 4, 0600 | IPC_CREAT))==-1)
	{
	printf("Blad uzyskania dostepu do zbioru semaforow /konsument\n");
	exit(-3);
	}	
	else
	printf("Proces %d uzyskal dostep do zbioru semaforow /konsument\n", getpid());

//kojarzenie zmiennej z plikiem "wejscie"
FILE *wyjscie, *bufor;
if((wyjscie=fopen("wyjscie", "w"))==NULL) {
	printf("Nie moge otworzyc pliku wyjscie do zapisu! /konsument\n");
	exit(1);
	}

//inicjowanie semafora
if(semctl(id_semafora,2,SETVAL,1)==-1) { printf("Blad inicjowania sem /konsument\n"); exit(-1); }

//zapisywanie znak po znaku zawartosci pliku buforu do wyjscia
char znak;
int flaga; //semafor (flaga) oznaczajacy zakonczenie producenta

if((flaga=semctl(id_semafora,2,GETVAL,NULL))<0) 
{
	printf("Blad pobierania wartosci semafora /konsument\n"); 
	exit(-7);
}

while(flaga>0)
{
	opusc_semafor(id_semafora, 1);
	if((bufor=fopen("bufor", "r"))==NULL) 
	{
        	printf("Nie moge otworzyc pliku bufor do odczytu /konsument!\n");
        	exit(1);
        }
	if((znak=fgetc(bufor))!=EOF) 
	{
		if(ferror(bufor)==EOF) 
		{
			printf("Blad odczytu znaku /konsument\n"); 
			exit(-9);
		}
        printf("Odczytany znak: %c /konsument\n", znak);
        fputc(znak, wyjscie);
	}
	
	if(ferror(wyjscie)==EOF) 
	{
		printf("Blad zapisu znaku do bufora /konsument\n"); 
		exit(-9);
	}
	if(fclose(bufor)==EOF) 
	{
		printf("Blad zamykania bufora /konsument\n"); 
		exit(-1);
	}	

	podnies_semafor(id_semafora, 0);
	if((flaga=semctl(id_semafora,2,GETVAL,NULL))<0)
	{
        	printf("Blad pobierania wartosci semafora /konsument\n");
        	exit(-7);
	}
}

//zamykanie pliku wejsciowego
if(fclose(wyjscie)==EOF) 
{
	printf("Blad zamykania wyjscia /konsument\n"); 
	exit(-1);
}

//opuszczenie semafora po przekazaniu ostatniego znaku do wyjscie
podnies_semafor(id_semafora, 3);

return 0;
}

static void opusc_semafor(int id_semafora, short nr_sem)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=nr_sem;
    bufor_sem.sem_op=-1;
    bufor_sem.sem_flg=0;

    while(1) {
    zmien_sem=semop(id_semafora,&bufor_sem,1);
    if (zmien_sem==0 || errno != 4) //eerno == 4 -> Interrupted system call
                break;
    }

    if(zmien_sem==-1) {
        if(errno != 4) {
                perror("Blad opuszczania semafora /konsument\n");
                exit(-5);
                }
        }
    else
    printf("Semafor %d zostal opuszczony /konsument\n", nr_sem);


}

static void podnies_semafor(int id_semafora, short nr_sem)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=nr_sem;
    bufor_sem.sem_op=1;
    bufor_sem.sem_flg=0;
	
        while(1) {
    zmien_sem=semop(id_semafora,&bufor_sem,1);
    if (zmien_sem==0 || errno !=4) //eerno == 4 -> Interrupted system call
                break;
    }

    if(zmien_sem==-1) {
    if(errno != 4) {
                perror("Blad podnoszenia  semafora /konsument\n");
                exit(-5);
                }
        }
    else
        printf("Semafor %d podniesiony /konsument\n", nr_sem);


}

