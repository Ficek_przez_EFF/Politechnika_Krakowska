#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

int id_semafor;

static void opusc_semafor(void)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=0;
    bufor_sem.sem_op=-1;
    bufor_sem.sem_flg=SEM_UNDO;

    while(1) {
    zmien_sem=semop(id_semafor,&bufor_sem,1);
    if (zmien_sem==0 || errno != 4) //eerno == 4 -> Interrupted system call
		break;
    }
	
    if(zmien_sem==-1) {
	if(errno != 4) {
		printf("Blad opuszczania semafora\n");
		exit(-5);
		}
	}
    else
    printf("\nSemafor zostal zamkniety.\n");

}

static void podnies_semafor(void)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num=0;
    bufor_sem.sem_op=1;
    bufor_sem.sem_flg=SEM_UNDO;

    while(1) {
    zmien_sem=semop(id_semafor,&bufor_sem,1);
    if (zmien_sem==0 || errno !=4) //eerno == 4 -> Interrupted system call
		break;
    }
	
    if(zmien_sem==-1) {
    if(errno != 4) {
                printf("Blad podnoszenia  semafora\n");
                exit(-5);
                }
        }
    else
	printf("Semafor zostal otwarty.\n\n");
}

static void liczba_procesow_czekajacych()
{
	int i;
	i = semctl(id_semafor, 0, GETNCNT);
	if( i==-1) 
	{
		printf("Blad odczytu liczby procesow czekajacych na podniesienie semafora");
		exit(-5);
	}
	else
		printf("Liczba procesow czekajacych na podniesienie semafora: %d\n", i);	
}
      

int main(int argc, char* argv[])
{
if(argc < 2) 
{ printf("Za malo argumentow\nwywolaj program_glowny liczba_sk\n"); exit(-1); }

int liczba_sk = atoi(argv[1]);

//kontrola przekazanych argumentow do programu 
if(!liczba_sk)
	{
	printf("Argument musi byc liczba\n");
	exit(-1);
	}
	

//tworzenie klucza
key_t klucz;
if(! (klucz = ftok(".",'F')))
        {
        printf("Blad tworzenia klucza /program_potomny");
        exit(-2);
        }

//uzyskanie dostepu do semafora
if((id_semafor = semget(klucz, 1, 0600 | IPC_CREAT))==-1)
	{
	printf("Blad uzyskania dostepu do semafora /program_potomny\n");
	exit(-3);
	}	
	else
	printf("Proces %d uzyskal dostep do semafora\n", getpid());

//dzialanie na semaforze
int i;
for(i=1;i<=liczba_sk; i++)
{
	opusc_semafor(); //zmiana stanu semafora na 0
	printf("Sekcja krytyczna procesu: %d\n",getpid());
	
	liczba_procesow_czekajacych();	//wypisanie liczby procesow czekajacych na podniesienie semafora
	podnies_semafor(); //zmiana stanu semafora na 1
}

return 0;
}
