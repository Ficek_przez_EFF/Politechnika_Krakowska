#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>

void main(int argc, char* argv[])
{
if(argc<4)
	{
	printf("Zla liczba argumentow\n wywolaj program_glowny program_potomny liczba_pp liczba_sk\n");
	exit(-1);	
	}

/*	FUNKCJA ATOI GUBI SIE PRZY DUZYCH WARTOSCIACH, NALEZY WYPISAC ERRNO I SPRAWDZIC CO SIE KONKRETNIE DZIEJE */


int 	liczba_pp = atoi(argv[2]),	
/* maksymalna liczbe procesow wyswietla poloecenie "ulimit -a" w wierszu "max user procceses"
   wyluskanie: ulimit -a | grep "max user processes" | tr -s ' ' | cut -d' ' -f5
*/
	liczba_sk = atoi(argv[3]);


// kontrola przekazanych argumentow do programu
if (!liczba_pp)
	{
	printf("Argument 2 musi byc liczba\n");
	exit(-2);
	}

if (!liczba_sk)
	{
	printf("Argument 3 musi byc liczba\n");
	exit(-2);
	}

if(liczba_pp <0 || liczba_sk <0)
	{
	printf("Argumenty liczbowe musza byc dodatnie\n");
	exit(-3);
	}

//wypisanie argumentow
printf("Program potomny: %s, Liczba_pp: %d, Liczba_sk: %d\n", argv[1],liczba_pp,liczba_sk);

//tworzenie klucza i semafora
key_t klucz;
int id_semafor;

if(! (klucz = ftok(".",'F'))) 
	{
	printf("Blad tworzenia klucza");
	exit(-4);
	}

id_semafor = semget(klucz, 1, 0600|IPC_CREAT|IPC_EXCL);
if (id_semafor==-1)
	{
	printf("Blad tworzenia semafora");
	exit(-5);
	}
	else
	printf("Semafor zostal utworzony, id: %d z kluczem: %d\n",id_semafor,klucz);

//ustwanie semafora

if(semctl(id_semafor, 0, SETVAL, 1)==-1)
	{
	printf("Nie moge ustawic semafora\n");
	exit(-7);
	}
	else
	printf("Semafor zostal ustawiony\n");

//utworzenie liczba_pp procesow
int i;
for(i=1; i<=liczba_pp; i++)
{
	switch(fork()) 
	{
	case -1:
		printf("Blad tworzenia procesu potomnego\n");
		exit(-4);
	case 0:
	if(execl(argv[1], argv[1], argv[3], NULL)==-1)
		{
	 	printf("Blad exec dla potomnego programu glownego\n");
		exit(-4);
		}
	}
}

//czekanie na zakonczenie procesow potomnych
int kod_powrotu,pid_potomny;
for(i=1; i<=liczba_pp; i++)
{
	pid_potomny = wait(&kod_powrotu);
	if(pid_potomny == -1) {
		printf("Blad funkcji wait\n");
		exit(-6);
	}
	printf("Proces potomny %d zakonczyl sie z kodem powrotu %d\n",pid_potomny, kod_powrotu);
}

//usuwanie semafora
if (semctl(id_semafor,0,IPC_RMID)==-1)
	{
	printf("Nie mozna usunac semafora\n");
	exit(-6);
	}
	else
	printf("Semafor zostal usuniety, id: %d\n", id_semafor);
	
return;
}
