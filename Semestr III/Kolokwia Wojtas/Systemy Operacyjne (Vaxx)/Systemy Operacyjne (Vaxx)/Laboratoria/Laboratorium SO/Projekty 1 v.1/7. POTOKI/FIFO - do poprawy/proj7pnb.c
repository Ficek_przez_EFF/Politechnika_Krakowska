#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>


/*

Ten projekt nie dzia�a tak jak powinien.
Jwojtas uruchomi� dw�ch producent�w w dw�ch r�nych terminalach. Nast�pnie ich zatrzyma�. W trzecim
terminalu uruchomi� konsumenta i on od razu si� zako�czy�. Zada� mi jedno pytanie: Dlaczego tak si� sta�o?
Odpowiedzia� za mnie "poniewa� program jest z�y" :D

-----------------------

Do programu przekazujemy jeden argument liczbe, ktora odpowiada pliku z ktorego producent pobiera znaki.
Jesli podamy liczbe 1 to producentowi zostanie przyporzadkoany plik "wejscie1" itd.

*/

int main(int argc, char* argv[]) 
{
char nazwa_pliku[10];
sprintf(nazwa_pliku, "wejscie%s", argv[1]);      

//kojarzenie zmiennej z plikiem "wejscie"
FILE *wejscie;
if((wejscie=fopen(nazwa_pliku, "r"))==NULL) {
	perror(nazwa_pliku);
	exit(1);
	}

//maksymalny rozmiar w potoku
if(sizeof(nazwa_pliku)>PIPE_BUF*8)
{
	perror("Plik jest za duzy");
	exit(-2);
}

//stworzenie kolejki fifo
if(mkfifo("fifo", 0700)== -1)
{
	if(errno == EEXIST)
		printf("Kolejka juz istnieje\n");
	else
     	{
		perror("Tworzenie kolejki FIFO");
     		exit(1);
	}
}
 else
   printf("Stworzylem kolejke FIFO\n");


//otwarcie kolejki tylko do zapisu
int sprawdz_potok;
sprawdz_potok = open("fifo", O_WRONLY);
if(sprawdz_potok == -1)
{
     printf("Otwarcie potoku do zapisu: %i (%s)\n", errno, strerror(errno));
     exit(1);
}


//zapis zawartosci pliku wejsciowego do potoki znak po znaku
char znak;

while(znak != EOF)
{
        znak = fgetc(wejscie);
        if(znak != EOF)
        {
                int uchwyt_mac;
                uchwyt_mac=write(sprawdz_potok ,&znak,sizeof(char));
                if (uchwyt_mac==-1)
                {
                        perror("Blad przy zapisie /producent");
                        exit(EXIT_FAILURE);
                }
        }
}

//zamykanie pliku wejsciowego
if(fclose(wejscie)==EOF) 
{
	perror("Blad zamykania wejscia /producent"); 
	exit(-1);
}

close(sprawdz_potok);

return 0;
}
