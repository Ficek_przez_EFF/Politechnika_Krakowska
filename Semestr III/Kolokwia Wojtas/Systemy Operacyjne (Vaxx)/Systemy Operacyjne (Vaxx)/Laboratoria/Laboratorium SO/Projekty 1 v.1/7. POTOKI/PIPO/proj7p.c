#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>
#include <limits.h>

int main(int argc, char* argv[]) 
{

char nazwa_pliku[10];
sprintf(nazwa_pliku, "wejscie%s", argv[0]);      

//kojarzenie zmiennej z plikiem "wejscie"
FILE *wejscie;
if((wejscie=fopen(nazwa_pliku, "r"))==NULL) {
	perror(nazwa_pliku);
	exit(1);
	}

if(sizeof(nazwa_pliku)>PIPE_BUF*8)
{
	perror("Plik jest za duzy");
	exit(-2);
}
//zapis zawartosci pliku wejsciowego do potoki znak po znaku
char znak;

while(znak != EOF)
{
	znak = fgetc(wejscie);
	if(znak != EOF)
	{
		//zapis bufora do potoku
		int uchwyt_mac;
		uchwyt_mac=write(1 ,&znak,sizeof(char));
		if (uchwyt_mac==-1)
		{
			perror("Blad przy zapisie /producent");
        		exit(EXIT_FAILURE);
		}
	}
}

//zamykanie pliku wejsciowego
if(fclose(wejscie)==EOF) 
{
	perror("Blad zamykania wejscia /producent"); 
	exit(-1);
}

return 0;
}
