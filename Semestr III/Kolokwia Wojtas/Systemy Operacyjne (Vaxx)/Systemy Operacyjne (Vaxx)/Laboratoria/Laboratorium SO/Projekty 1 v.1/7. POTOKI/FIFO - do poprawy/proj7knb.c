#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>

//JWojtas powiedzia�, �e program jest b��dny
/*
Do programu przekazujemy jeden argument liczbe, ktora odpowiada pliku do ktorego konsument zapisuje znaki.
Jesli podamy liczbe 1 to konsumentowi zostanie przyporzadkowany plik "wyjscie1" itd.
*/

int main(int argc, char* argv[]) 
{

char nazwa_pliku[10];
sprintf(nazwa_pliku, "wyjscie%s", argv[1]);


//kojarzenie zmiennej z plikiem "wyjscie"
FILE *wyjscie;
if((wyjscie=fopen(nazwa_pliku, "w"))==NULL) {
	perror("Nie moge otworzyc pliku wyjscie do zapisu! /konsument\n");
	exit(1);
	}

//otworzenie kolejki tylko do odczytu z flaga
// NONBLOCK - operacje, ktorych wykonanie nie moze byc natychmiast wykonane nie maja byc blokowane

int sprawdz_potok;
sprawdz_potok = open("fifo", O_RDONLY | O_NONBLOCK);

/*
 *	Dzialanie funkcji read z flaga O_NONBLOCK
 *
 *	piste lacze = sa pisarze - EAGAIN, nie ma ich - 0
 *	
 *
 */
	

if(sprawdz_potok == -1) {
                  perror("Otwarcie potoku do odczytu\n");
                  exit(1);
}

//odczytanie z potoku i zapis do pliku wyjsciowego
char bufor;
int uchwyt_pot;

while(1)
{
	sleep(1);
	if(read(sprawdz_potok,&bufor,sizeof(bufor))==0) 
	{
		break;
	}
	printf("Odczytane dane to: %c / konsument%d \n", bufor,atoi(argv[1]));

	        if(fputc(bufor, wyjscie) == EOF)
        {
                perror("Blad zapisuje do pliku /konsument\n");
                exit(-10);
        }
}

//zamykanie pliku wejsciowego
if(fclose(wyjscie)==EOF) 
{
	printf("Blad zamykania wyjscia /konsument\n"); 
	exit(-1);
}

close(sprawdz_potok);
return 0;
}

