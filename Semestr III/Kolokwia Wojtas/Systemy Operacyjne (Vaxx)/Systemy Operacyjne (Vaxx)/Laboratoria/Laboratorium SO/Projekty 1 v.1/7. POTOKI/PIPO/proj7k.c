#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>
#include <limits.h>

int main(int argc, char* argv[]) 
{
char nazwa_pliku[10];
sprintf(nazwa_pliku, "wyjscie%s", argv[0]);

//kojarzenie zmiennej z plikiem "wyjscie"
FILE *wyjscie;
if((wyjscie=fopen(nazwa_pliku, "w"))==NULL) 
{
	perror("Nie moge otworzyc pliku wyjscie do zapisu! /konsument\n");
	exit(1);
}

//odczytanie z potoku i zapis do pliku wyjsciowego
char bufor;
int uchwyt_pot;
while((uchwyt_pot=read(0,&bufor,sizeof(bufor))) > 0)
{
   	printf("Odczytane dane to : %c /konsument %d\n",bufor,atoi(argv[0]));
   	
	if(fputc(bufor, wyjscie) == EOF)
	{
		perror("Blad zapisuje do pliku /konsument\n");
		exit(-10);
	}

	if (uchwyt_pot==-1)
        {
                perror("Blad przy czytaniu.\n");
                exit(EXIT_FAILURE);
        }
}

//zamykanie pliku wejsciowego
if(fclose(wyjscie)==EOF) 
{
	printf("Blad zamykania wyjscia /konsument\n"); 
	exit(-1);
}

return 0;
}

