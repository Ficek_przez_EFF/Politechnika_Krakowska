#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <errno.h>
#include <limits.h>

int main(int argc, char* argv[]) 
{
//sprawdzanie argumentow przekazanych do programu
if(argc!=3)
{
	printf("Zla liczba argumentow\n wywolaj proj7 liczba_producentow liczba_konsumentow\n");
	exit(-1);	
}
	
int liczba_p = atoi(argv[1]), liczba_k = atoi(argv[2]);
if(!liczba_p)
{
	printf("Podany argument1 musi byc liczba\n");
	exit(4);
}

if(liczba_p<=0)
{
	printf("Podany arugment1 musi byc liczba dodatnia\n");
	exit(4);
}

if(!liczba_k)
{
        printf("Podany argument2 musi byc liczba\n");
        exit(4);
}

if(liczba_k<=0)
{
        printf("Podany arugment1 musi byc liczba dodatnia\n");
        exit(4);
}


if(liczba_p+liczba_k>511)
{
        printf("Przekroczono maksymalna liczbe procesow\n");
        exit(4);
}


//tworzenie procesow potomnych
int i;
int potok[2]; //deskryptory potoku
int sprawdz_potok;
char bufor[5];
FILE *test_wejscia;
char nazwa_pliku[10]; 
printf("Maksymalna liczba bitow w potoku %d\n",PIPE_BUF*8);

sprawdz_potok = pipe(potok);    //tworzenie potoku PIPE
if(sprawdz_potok == -1)
{
        printf("Blad tworzenia potoku: %i (%s)\n",errno,strerror(errno));
        exit(-2);
}

//tworzenie procesow konsumenta
for(i=0; i<liczba_k; i++)
{
        sprintf(bufor, "%d", i+1);
        

        switch(fork())
        {
                case -1:
                        printf("Blad tworzenia konsumenta\n");
                        exit(-4);
                case 0:
                        if(close(potok[1]) == -1)
                        {
                                printf("Blad close (konsument): %i (%s)\n",errno,strerror(errno));
                                exit(-5);
                        }

                        if(dup2(potok[0], 0)< 0)                                {                       
                                printf("Blad dup2 konsument):: %i (%s)\n",errno,strerror(errno));
                                exit(-5);
                        }

                        if(execl("proj7k", bufor , NULL)==-1)
                        {
                                 printf("Blad exec (konsument): %i (%s)\n",errno,strerror(errno));
                                exit(-4);
                        }
        }
}

//tworzenie procesow producentow
for(i=0; i<liczba_p; i++)        
{
	sprintf(bufor, "%d", i+1);
	sprintf(nazwa_pliku, "wejscie%d", i+1);
	if((test_wejscia=fopen(nazwa_pliku, "r"))==NULL) {
        	printf("Plik %s nie istnieje!\n", nazwa_pliku);
        	continue;
	}

	switch(fork())
	{
        	case -1:
                	printf("Blad tworzenia producenta: %i (%s)\n", errno,strerror(errno));
                	exit(-4);
        	case 0:
			if(dup2(potok[1], 1) < 0) //powielanie deskryptora  
			{
				printf("Blad dup2 (producent):: %i (%s)\n",errno,strerror(errno));
				exit(-5);
			}		
        		if(execl("proj7p", bufor, NULL)==-1)
                	{
                		printf("Blad exec (producent): %i (%s)\n",errno,strerror(errno));
                		exit(-4);
                	}
	}
}

close(potok[0]);
close(potok[1]);

//oczekiwanie na zakonczenie procesow potomnych
for(i=0;i<liczba_p+liczba_k;i++)
	wait(NULL);

return 0;
}
