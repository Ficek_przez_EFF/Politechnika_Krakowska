#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
int i;

printf("Proces macierzysty! PID: %d, PPID: %d\n", getpid(), getppid());
int pidmacierzysty = getpid();
char polecenie[35];

for(i=1; i<=3; i++) 
{
	switch(fork())
	{
		case -1:
		perror("fork error");
		exit(1);
		case 0:
		printf("POTOMEK %d:\n", i);
		printf("UID: %d, GID: %d, PID: %d, PPID: %d\n\n", getuid(), getgid(), getpid(), getppid()); 
 		
		break;
		default:
		printf("Proces macierzysty\n");
	}
}

sprintf(polecenie, "pstree -Ap %d", pidmacierzysty);
system(polecenie);

/*
  JWojtas zapyta� sk�d proces "sh" w drzewku proces�w.
  Odp.: Proces sh tworzy funkcja system i ten proces przekszta�ca si� w drzewko (pstree)

 */

return 0;
}
