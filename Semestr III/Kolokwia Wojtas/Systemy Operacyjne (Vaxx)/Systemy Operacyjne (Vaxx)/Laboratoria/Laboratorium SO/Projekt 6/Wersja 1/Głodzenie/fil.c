#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

void sem_p2(int sem_num1, int sem_num2);
void sem_v2(int sem_num1, int sem_num2);

int sem_id;

int main(int argc, char *argv[])
{
    key_t _key = ftok(".", 'Z');
    if(_key == -1)
    {
        perror("Ftok error!");
        exit(-1);
    }
    
    int eaten = 0;
    
    sem_id = semget(_key, 5, 0600);
    if(sem_id == -1)
    {
        printf("[%d] ", getpid());
        perror("Blad przy podlaczaniu sie do zbioru semaforow!");
        exit(-1);
    }
    
    printf("[%d] Podlaczono do zbioru semaforow %d\n", getpid(), sem_id);
    
    int left, right;
    
    left = atoi(argv[1]);
    right = left == 4 ? 0 : left + 1;
    
    sleep(1);
    
    while(1)
    {
        printf("[%d] Czekam na widelce!\n", getpid());
        sem_p2(left, right);
        printf("[%d] Mam oba widelce!\n", getpid());
        
        printf("[%d] Jem....\n", getpid());
        
        if(left == 0) sleep(2);
        if(left == 2) sleep(3);
        
        eaten++;
        printf("[%d] Zjadlem juz %d\n", getpid(), eaten);
        
        sem_v2(right, left);
        printf("[%d] Oddalem dwa widelce\n", getpid());
    }
    
    
    exit(0);
}

void sem_p2(int sem_num1, int sem_num2)
{
    struct sembuf buf[2];
    int semop_ret;
    
    buf[0].sem_num = sem_num1;
    buf[0].sem_op = -1;
    buf[0].sem_flg = 0;
    
    buf[1].sem_num = sem_num2;
    buf[1].sem_op = -1;
    buf[1].sem_flg = 0;
    
    // ignorujemy EINTR
    while((semop_ret = semop(sem_id, buf, 2)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Nie udalo sie opuscic semafora!");
        exit(-1);
    }
}

void sem_v2(int sem_num1, int sem_num2)
{
    struct sembuf buf[2];
    int semop_ret;
    
    buf[0].sem_num = sem_num1;
    buf[0].sem_op = 1;
    buf[0].sem_flg = 0;
    
    buf[1].sem_num = sem_num2;
    buf[1].sem_op = 1;
    buf[1].sem_flg = 0;
    
    // ignorujemy EINTR
    while((semop_ret = semop(sem_id, buf, 2)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Nie udalo sie podniesc semafora!");
        exit(-2);
    }
}
