#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>


void sem_p(int sem_num);
void sem_v(int sem_num);

int sem_id;


//**************MAIN****************
int main(int argc, char *argv[])
{
    key_t _key = ftok(".", 'Z');
    if(_key == -1)
    {
        perror("Ftok error!");
        exit(-1);
    }
    
    int eaten = 0;
    
    sem_id = semget(_key, 5, 0600);
    if(sem_id == -1)
    {
        printf("[%d] ", getpid());
        perror("Blad przy podlaczaniu sie do zbioru semaforow!");
        exit(-1);
    }
    
    printf("[%d] Podlaczono do zbioru semaforow %d\n", getpid(), sem_id);
    
    int left, right;
    left = atoi(argv[1]);
    right = left == 4 ? 0 : left + 1;
    
    sleep(1);
    
    while(1)
    {
        printf("[%d] Czekam na widelce!\n", getpid());
        sem_p(left);
        printf("[%d] Podnioslem lewego!\n", getpid());
        sem_p(right);
        printf("[%d] Podnioslem prawego!\n", getpid());
        
        printf("[%d] Jem.....\n", getpid());
        
        eaten++;
        printf("[%d] Skonczyłem: %d\n", getpid(), eaten);
        
        sem_v(right);
        printf("[%d] Odlozylem lewego!\n", getpid());
        sem_v(left);
        printf("[%d] Odlozylem prawego!\n", getpid());
    }
    
  
    exit(0);
}


void sem_p(int sem_num)
{
    struct sembuf buf;
    int semop_ret;
    
    buf.sem_num = sem_num;
    buf.sem_op = -1;
    buf.sem_flg = 0;
    
    // ignorujemy EINTR
    while((semop_ret = semop(sem_id, &buf, 1)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Nie udalo sie opuscic semafora!");
        exit(-1);
    }
}


void sem_v(int sem_num)
{
    struct sembuf buf;
    int semop_ret;
    
    buf.sem_num = sem_num;
    buf.sem_op = 1;
    buf.sem_flg = 0;
    
    // ignorujemy EINTR
    while((semop_ret = semop(sem_id, &buf, 1)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Nie udalo sie podniesc semafora!");
        exit(-2);
    }
}


