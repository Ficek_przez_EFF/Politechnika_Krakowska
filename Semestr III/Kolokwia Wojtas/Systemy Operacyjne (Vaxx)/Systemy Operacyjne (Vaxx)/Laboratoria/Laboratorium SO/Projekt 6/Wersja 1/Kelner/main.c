#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <signal.h>

void sem_rem();

// funkcja, ktora przechwytuje ctrl+c
void my_exit();
int sem_id;

//**************MAIN****************
int main()
{
    key_t _key = ftok(".", 'Z');
    if(_key == -1)
    {
        perror("Ftok error!");
        exit(-1);
    }
    
    sem_id = semget(_key, 6, IPC_CREAT|IPC_EXCL|0600);
    if(sem_id == -1)
    {
        printf("[%d] ", getpid());
        perror("Blad przy tworzeniu zbioru semaforow!");
        exit(-1);
    }
    
    printf("[%d] Stworzono zbior semaforow %d\n", getpid(), sem_id);
    
    signal(SIGINT, my_exit);
    signal(SIGTERM, my_exit);
    
    int i;
    for(i = 0; i < 5; i++)
    {
        if(semctl(sem_id, i, SETVAL, 1) == -1)
        {
            printf("Semafor %d: ", i);
            perror("Blad ustawiania startowej wartosci!");
            exit(-1);
        }
    }
    
    if(semctl(sem_id, 5, SETVAL, 4) == -1)
    {
        perror("Semafor kelnera: Blad ustawiania startowej wartosci");
        exit(-1);
    }
    
    printf("Semafory zostaly ustawione\n");
 
    
    char buf[2];
    
    for(i = 0; i < 5; i++)
    {
        switch(fork())
        {
            case -1:
                perror("Blad fork!");
                sem_rem();
                exit(-1);
                break;
            case 0:
                sprintf(buf, "%d", i);
                if(execl("./fil", "fil", buf, NULL) == -1)
                {
                    perror("Blad execl!");
                    sem_rem();
                    exit(-1);
                }
                break;
        }
    }
    
    int w, x;
    
    for(i = 0; i < 5; i++)
    {
        w = wait(&x);
        printf("Proces %d zakonczyl sie z kodem %d\n", w, x);
    }
    

    sem_rem();
    
    printf("\n Koncze prace! \n");
    exit(0);
}

void my_exit()
{
    sem_rem();
    
    printf("\n Koncze prace! \n");
    exit(0);
}

void sem_rem()
{
    if(semctl(sem_id, 0, IPC_RMID) == -1)
    {
        perror("Blad usuwania zbioru semaforow!");
        exit(-3);
    }
    printf("Usunieto zbior semaforow %d\n", sem_id);
}

