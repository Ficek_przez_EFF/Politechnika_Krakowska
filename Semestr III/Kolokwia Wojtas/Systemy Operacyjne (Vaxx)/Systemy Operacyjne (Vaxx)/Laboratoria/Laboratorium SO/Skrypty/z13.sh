#!/bin/bash

# Zadanie 13
# Wartosc zmiennej PATH zapisz w pliku, który jest pierwszym parametrem
# wywołania, natomiast zmiennej HOMe w pliku będącym trzecim parametrem
# wywołania. Połącz te dwa pliki do pliku, który jest drugim parametrem
# wywołania.

if [ $# -ne 3 ]; then
	echo "Nie podales 3 argumentow"
	exit
fi

if [ -f $1 ]; then
        echo "Istnieje plik $1."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi

if [ -f $3 ]; then
        echo "Istnieje plik $3."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi

echo $PATH > $1
echo $HOME > $3
cat $1 $3 > $2
