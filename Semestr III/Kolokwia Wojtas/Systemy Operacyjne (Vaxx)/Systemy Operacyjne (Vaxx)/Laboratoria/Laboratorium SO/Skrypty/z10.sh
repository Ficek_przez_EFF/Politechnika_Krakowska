#!/bin/bash

# Zadanie 10
# Z pliku będącego pierwszym parametrem wywołania linię 10 i 20 zapisz do pliku,# który jest drugim parametrem wywołania.

if [ $# -ne 2 ]; then
	echo "Nie podales 2 argumentow"
	exit
fi

if [ ! -f $1 ]; then
	echo "Nie istnieje plik $1"
	exit
fi
if [ -f $2 ]; then
        echo "Istnieje plik $2."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi

head -10 $1 | tail -1 > $2
head -20 $1 | tail -1 >> $2

