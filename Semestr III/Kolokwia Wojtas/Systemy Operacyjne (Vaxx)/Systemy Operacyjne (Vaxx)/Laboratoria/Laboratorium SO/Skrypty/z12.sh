#!/bin/bash

# Zadanie 12
# Z wszystkich plików źródłowych w C z bieżącego katalogu wybierz polecenia
# preprocesora i zapisz je do pliku, który jest pierwszym parametrem wyowałania.

if [ $# -ne 1 ]; then
	echo "Nie podales 1 argumentu"
	exit
fi
if [ -f $1 ]; then
        echo "Istnieje plik $1."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi

grep '^#' *.c > $1

