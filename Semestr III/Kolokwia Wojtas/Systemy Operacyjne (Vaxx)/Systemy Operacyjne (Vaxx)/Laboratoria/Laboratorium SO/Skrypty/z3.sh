#!/bin/bash
# Zadanie 3
# Wyświetl ilość razy (pierwszy parametr wywołania te linie, które zawierają
# poszukiwany tekst (drugi parametr wywołania) w pliku o nazwie alfa.c

if [ $# -ne 2 ]; then
	echo "Nie podales 2 argumentow"
	exit
fi

zm=0

while [ $zm -lt $1 ]
 do
	echo "`grep $2 alfa.c`"
	zm=$(($zm+1))
 done

