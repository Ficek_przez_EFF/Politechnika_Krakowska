#!/bin/bash

# Zadanie 9
# Posotuj dwa pliki bedące drugim i trzecim parametrem wywołania. Połącz
# posortowane pliki i zapisz je do pliku, który jest pierwszym parametrem
# wywołania.

if [ $# -ne 3 ]; then
	echo "Nie podales 3 argumentow"
	exit
fi
if [ ! -f $2 ]; then
	echo "Nie istnieje plik $2"
	exit
fi
if [ ! -f $3 ]; then
	echo "Nie istnieje plik $3"
	exit
fi

if [ -f $1 ]; then
        echo "Istnieje plik $1."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi

cat $2 | sort > $1
cat $3 | sort >> $1

