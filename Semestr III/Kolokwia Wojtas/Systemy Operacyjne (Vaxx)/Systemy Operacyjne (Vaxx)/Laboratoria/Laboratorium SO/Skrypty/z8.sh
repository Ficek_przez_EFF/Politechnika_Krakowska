#!/bin/bash

# Zadanie 8
# Znajdź w bieżącym katalogu pliki źródłowe C, zawierające teskt będący drugim
# parametrem wywołania. Znalezione linie wyświetl i zapisz w pliku, który jest
# pierwszym parametrem wywołania

if [ $# -ne 2 ]; then
	echo "Nie podales 2 argumentow"
	exit
fi
if [ -f $1 ]; then
	echo "Istnieje plik $1."
	echo -n "Nadpisac? T/N: "
	read rewrite
	
	if [ ! $rewrite = "T" ]; then
		echo "Nienadpisano"
		exit
	fi
fi

grep $2 *.c | cut -f 2 -d: > $1
cat $1


