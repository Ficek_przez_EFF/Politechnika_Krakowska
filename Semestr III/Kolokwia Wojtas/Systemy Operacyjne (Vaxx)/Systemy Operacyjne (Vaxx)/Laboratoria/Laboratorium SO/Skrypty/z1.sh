#!/bin/bash

if [ ! $# == 2 ]; then
	echo "Nie podales 2 argumentow"
	exit
fi

if [ ! -f $1 ]; then
	echo "Nie ma pliku $1"
	exit
fi

if [ ! -f $2 ]; then
	echo "Nie ma pliku $2"
	exit
fi

dl1=`cat $1 | wc -c`
dl2=`cat $2 | wc -c`

if [ $dl1 -gt $dl2 ]; then
	roz=$(($dl1-$dl2))
	echo "Plik $1 jest wiekszy od $2 o $roz znakow"
 else
 if [ $dl2 -gt $dl1 ]; then
	roz=$(($dl2-$dl1))
	echo "Plik $2 jest wiekszy od $1 o $roz znakow"
 else 
	echo "Pliki $1 i $2 sa rowne"
 fi
fi

