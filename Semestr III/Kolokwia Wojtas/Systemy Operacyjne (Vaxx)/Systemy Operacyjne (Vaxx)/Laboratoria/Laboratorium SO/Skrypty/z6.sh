#!/bin/bash

# Zadanie 6
# Znajdź w pliku, który jest pierwszym parametrem wywołania linie zawierające
# tekst będący drugim parametrem wyowalania. Linie te zapisz w pliku, który
# jest trzecim parametrem wywołania.

if [ $# -ne 3 ]; then
	echo "Nie podales 3 argumentow"
	exit
fi
if [ ! -f $1 ]; then
	echo "Nie istnieje plik $1"
	exit
fi
if [ -f $3 ]; then
        echo "Istnieje plik $3."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi


`grep $2 $1 > $3`

