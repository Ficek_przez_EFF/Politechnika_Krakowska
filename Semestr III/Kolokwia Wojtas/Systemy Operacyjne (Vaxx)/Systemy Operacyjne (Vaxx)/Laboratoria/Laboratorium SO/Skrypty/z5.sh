#!/bin/bash

# Zadanie 5
# Połącz i posortuj dwa pliki (będące pierwszym i drugim parametrem wyowłania)
# do pliku, który jest trzecim parametrem wywołania.

if [ $# -ne 3 ]; then
	echo "Nie podales 3 argumentow"
	exit
fi
if [ ! -f $1 ]; then
	echo "Nie ma pliku $1"
	exit
fi
if [ ! -f $2 ]; then
	echo "Nie ma pliku $2"
	exit
fi
if [ -f $3 ]; then
        echo "Istnieje plik $3."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi

`cat $1 $2 | sort > $3`

