#!/bin/bash
# Zadanie 4
# Zamienia nazwy plikow

if [ ! $#==2 ]; then
	echo "Nie podales 2 arg"
	exit
fi

if [ ! -f $1 ]; then
	echo "Nie ma pliku $1"
	exit
fi
if [ ! -f $2 ]; then
	echo "Nie ma pliku $2"
	exit
fi

mv $1 tmp
mv $2 $1
mv tmp $2

