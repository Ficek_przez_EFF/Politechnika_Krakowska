#!/bin/bash

# Zadanie 11
# Z pliku będącego drugim parametrem wywołania linię o numerze, który jest
# trzecim parametrem wywołania, powtórz 5 razy. Oprócz wyświetlenia na ekranie
# zapisz te powtórzone linie do pliku, który jest pierwszy parametrem.

if [ $# -ne 3 ]; then
	echo "Nie podales 3 argumentow"
	exit
fi
if [ ! -f $2 ]; then
	echo "Nie istnieje plik $2"
	exit
fi
if [ -f $1 ]; then
        echo "Istnieje plik $1."
        echo -n "Nadpisac? T/N: "
        read rewrite

        if [ ! $rewrite = "T" ]; then
                echo "Nienadpisano"
                exit
        fi
fi

rm -R $1

zm=0
while [ $zm -lt 5 ]
	do
	head -$3 $2 | tail -1
	head -$3 $2 | tail -1 >> $1
	zm=$(($zm+1))
done
