#include <stdio.h>
#include <stdlib.h>

int main()
{
  printf("UID = %d \t", getuid() );
  printf("GID = %d \t", getgid() );
  printf("PID = %d \t", getpid() );
  printf("PPID = %d \n", getppid() );

  return 0;
}
