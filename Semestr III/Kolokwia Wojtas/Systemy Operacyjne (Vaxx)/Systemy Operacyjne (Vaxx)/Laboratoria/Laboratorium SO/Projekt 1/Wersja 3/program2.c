#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
 int i;
 char cm[20];

 sprintf(cm, "pstree -ph %d", getpid() );

 printf("\nProces macierzysty: UID=%d\tGID=%d\tPID=%d\tPPID=%d\n\n",getuid(),getgid(),getpid(),getppid());

 for(i=1; i<=3; i++)
 {
  switch( fork() )
  {
    case -1:
      perror("Nie utworzono procesu potomnego");
      break;

    case 0:
      printf("Fork %d - UID=%d\tGID=%d\tPID=%d\tPPID=%d\n",i,getuid(),getgid(),getpid(),getppid());
      break;

    default:
      break;
  }
 }

 system(cm);
}
