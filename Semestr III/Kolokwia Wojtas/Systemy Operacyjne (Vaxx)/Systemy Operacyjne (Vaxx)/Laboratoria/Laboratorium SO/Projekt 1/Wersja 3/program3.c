#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include <errno.h>

int main()
{
int i,e,w,x;
char drzewo[35];

for(i=1; i<=3; i++)
{
	switch(fork())
	{
		case -1:
		perror("Blad fork");
		exit(1);
		case 0:
		e=execl("./program1","program1",NULL);
		if(e == -1)
		{
			perror("Blad execl");
			exit(2);
		}
		break;
		default:
		sleep(1);
	}
}

sprintf(drzewo,"pstree -p %d",getpid());
system(drzewo);


for(i=1; i<=3; i++)
{
	w=wait(&x);
	if(w==-1)
	{
		perror("Blad wait");
		exit(3);
	}
	printf("Kod powrotu potomka o numerze PID %d = %d\n", w,x );
}


return 0;
}
