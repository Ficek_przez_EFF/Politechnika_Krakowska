#include <stdio.h>
#include <stdlib.h>
#include <sys/errno.h>
#include <sys/types.h>

int main()
{   
    printf("PID=%d\n", getpid());
    printf("UID=%d\n", getuid());
    printf("GID=%d\n", getgid());
    printf("PPID=%d\n", getppid());
    exit(0);
}
