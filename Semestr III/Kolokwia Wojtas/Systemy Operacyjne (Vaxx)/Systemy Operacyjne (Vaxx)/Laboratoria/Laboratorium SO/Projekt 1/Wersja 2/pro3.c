#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

int main()
{
 int i,j, *x, waitpid, ex,a=0;
 char cm[3];

 sprintf(cm, "pstree -ph %d", getpid());


 for(i=1; i<=3; i++)
 {
  switch( fork() )
  {
    case -1:
      perror("Nie utworzono procesu potomnego");
      break;

    case 0:
      ex = execl("./program1", "program1", NULL);

      if(ex == -1)
      {
       perror("Blad funkcji execl\n");
      }

      break;

    default:
      break;

  }

 waitpid = wait(&x);
  printf("Proces %d zakonczyl sie z kodem %d\n", waitpid, x);

 
 }
 
 system(cm);



 return 0;
}
