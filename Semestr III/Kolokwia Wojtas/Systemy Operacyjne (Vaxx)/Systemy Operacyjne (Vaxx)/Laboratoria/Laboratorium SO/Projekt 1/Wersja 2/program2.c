#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
void id()
{
    printf("UID: %d \n", getuid());
    printf("GID: %d \n", getgid());
    printf("PID: %d \n", getpid());
    printf("PPID: %d \n ************************** \n", getppid());
}

int main()
{
    int i;
    printf("Proces macierzysty: \n");
    id();
    
    for (i=1; i<4; i++)
    {
        switch(fork())
        {
           case -1:
                perror("fork error - nie utworzono nowego procesu \n");
           case 0:
                printf("Proces potomny %d: \n", i);
                id();
                break;
           default:
                printf("Proces macierzysty \n");  
        }
        sleep(3);
    }
    
   // exit(0);
}
