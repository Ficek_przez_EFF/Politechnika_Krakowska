#include <stdio.h>
#include <pthread.h>

int tab[2][10];
int sum1 = 0, sum2 = 0;

void * count1()
{
	int i;
	for ( i = 0; i < 10; i++ )
		sum1 += tab[0][i];
	printf("Suma pierwszego wiersza: %d\n", sum1);
	pthread_exit(NULL);
}

void * count2()
{
	int i;
	for ( i = 0; i < 10; i++ )
		sum2 += tab[1][i];
	printf("Suma drugiego wiersza: %d\n", sum2);
	pthread_exit(NULL);
}

int main()
{
	int i, j;
	pthread_t th1, th2;
	
	srand(time(NULL));
	
	for ( i = 0; i < 2; i++ )
		for ( j = 0; j < 10; j++ )
			tab[i][j] = rand() % 100;
			 
	if ( pthread_create(&th1, NULL, count1, NULL) == -1 )
	{
		perror("Error creating thread 1");
		return 1;
	}
	
	if ( pthread_create(&th2, NULL, count2, NULL) == -1 )
	{
		perror("Error creating thread 2");
		return 2;
	}
	
	if ( pthread_join(th1, NULL) == -1 
		|| pthread_join(th2, NULL) == -1 )
	{
		perror("Error joining threads");
		return 3;
	}
	
	printf("Suma wszystkich elementow: %d\n", sum1 + sum2);
	
	pthread_exit(NULL);
}
