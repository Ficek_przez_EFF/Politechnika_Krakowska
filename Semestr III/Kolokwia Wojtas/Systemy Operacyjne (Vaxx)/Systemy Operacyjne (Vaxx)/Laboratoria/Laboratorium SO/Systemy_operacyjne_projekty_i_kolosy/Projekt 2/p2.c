#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#define REENTRANT
//zakomentowany kod kazal mi dorobi� -wypisanie id watkow, kod�w powrotu z watkow
int tab[2][10],sum1, sum2;

void *w1()
{
	sum1=0;
	int i;
	for(i=0; i<10; i++)
		sum1+=tab[0][i];

	printf("Suma elementow pierwszego wiersza tabeli: %d\n",sum1);
	//printf("id watku1=%lu\n",(unsigned long) pthread_self()); nie jsetem pewien jaki typ powinien by� - przy int jest ujemne, analogicnie w pozosta�ych
	//printf("id watku2=%d\n",(int) pthread_self());
	pthread_exit((void *)0);
}

void *w2()
{
        sum2=0;
        int i;
        for(i=0; i<10; i++)
                sum2+=tab[1][i];

        printf("Suma elementow drugiego wiersza tabeli: %d\n",sum2);
        //printf("id watku2=%d\n",(int) pthread_self());
        pthread_exit((void *)0);
}

int main()
{
int i,j;
//int *kod_powrotu1,*kod_powrotu2;
pthread_t tid1,tid2;
srand(time(NULL));

for(i=0; i<2; i++)
        for(j=0; j<10; j++)
                tab[i][j]=rand()%9;

for(i=0; i<2; i++)
{
        for(j=0; j<10; j++)
                printf("%d ",tab[i][j]);
printf("\n");
}
if (errno=pthread_create(&tid1, NULL,w1, NULL)){
		fprintf(stderr,"Blad tworzenia watku: %s ",strerror(errno));
		exit(1);
	}

if (errno=pthread_create(&tid2, NULL,w2, NULL)){
			fprintf(stderr,"Blad tworzenia watku: %s ",strerror(errno));
            exit(1);
        }

//if (errno=pthread_join(tid1, (void*) &kod_powrotu1 ))
if (errno=pthread_join(tid1,(void*) NULL))
        {       fprintf(stderr,"Blad przylaczenia watku: %s ",strerror(errno));
                exit(2);
        }

//if (errno=pthread_join(tid2,(void*) &kod_powrotu2  ))
if (errno=pthread_join(tid2,(void*) NULL))
        {       fprintf(stderr,"Blad przylaczenia watku: %s ",strerror(errno));
                exit(2);
        }
//printf("id watku glownego=%d\n",(int) pthread_self());
printf("Suma calkowita z sum czastkwych wyznaczonych przez watki: %d\n", sum1+sum2);
//printf("\nKod powrotu watku 1: %d, z watku 2: %d\n", kod_powrotu1,kod_powrotu2);
return 0;
}

