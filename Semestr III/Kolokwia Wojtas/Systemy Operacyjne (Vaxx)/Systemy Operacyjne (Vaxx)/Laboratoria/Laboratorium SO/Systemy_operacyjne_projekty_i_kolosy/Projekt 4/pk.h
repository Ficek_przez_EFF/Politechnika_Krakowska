#ifndef PK_H
#define PK_H

#include <stdio.h>
#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <errno.h>

#define KOLEJKA_FILE "kolejka.txt"
#define WYJSCIE_FILE "wyjscie.txt"
//#define BUFOR_FILE   "bufor.txt"

/*
 * Semafor 0: Pełny 
 * Semafor 1: Pusty
 * Semafor 2: Stan produkcji
 * Semafor 3: Sygnał ostatniego odczytu:
 * -- dopiero po ostatnim sprawdzeniu stanu produkcji 
 * -- przez konsumenta możemy usunąć semafory
 * -- w innym wypadku otrzymamy błąd semaforów
*/
#define PELNY     0
#define PUSTY     1
#define PRODUKCJA 2
#define KONIEC    3

int opusc_semafor(int semid, int semnum);
int podnies_semafor(int semid, int semnum);

#endif
