#include "pk.h"

int main()
{
	key_t klucz;
	int semid, i, produkt, produkcja, shmid;
	FILE *kolejka;
	char * pamiec;
	
	int semval[4] = {0, 1, 1, 0};
	 
	if ((klucz = ftok("./p", 'N')) == -1 )
	{
		perror("Producent: błąd tworzenia klucza\n");
		return 1;
	}
	
	printf("Producent: utworzono klucz %d\n", klucz);
	
	if ((semid = semget(klucz, 4, IPC_CREAT | 0600)) == -1 )
	{
		perror("Producent: błąd tworzenia semaforów\n");
		return 2;
	}

	printf("Producent: Semafory utworzone, id: %d\n", semid);
	
	for ( i = 0; i < 4; i++ )
	{
		if ( semctl(semid, i, SETVAL, semval[i]) == -1 )
		{
			perror("Producent: Błąd inicjalizjacji semaforów\n");
			return 3;
		}
	}

	// Pammięć dzielona
	if ( (shmid = shmget(klucz, 1*sizeof(char), IPC_CREAT | 0600 )) == -1 )
	{
		perror("Producent: błąd tworzenia pamięci dzielonej\n");
		return 10;
	}
	
	if ( (pamiec = (char *) shmat(shmid, NULL, 0)) == -1 )
	{
		perror("Producent: błąd dołączania pamięci dzielonej\n");
		return 11;
	}

	if ( ! (kolejka = fopen(KOLEJKA_FILE, "r")))
	{
		perror("Producent: Nie udało się otworzyć pliku z kolejką produkcji\n");
		return 4;
	}
	
	produkcja = semctl(semid, PRODUKCJA, GETVAL);		
	while ( produkcja )
	{
		produkt = fgetc(kolejka);
		
		opusc_semafor(semid, PUSTY);
		
		pamiec[0] = produkt;

		printf("Producent: zapis znaku %c do bufora (kod: %d)\n", produkt, produkt);
		
		/*if ( ! (bufor = fopen(BUFOR_FILE, "w")))
		{
			perror("Producent: Nie udało się otwowrzyć bufora\n");
			return 5;
		}
	
		if ( (fputc(produkt, bufor)) == EOF )
		{
			perror("Producent: błąd zapisu do bufora\n");
			return 6;
		}
		
		fclose(bufor);*/
			
		podnies_semafor(semid, PELNY);
		if ( produkt == EOF )
			opusc_semafor(semid, PRODUKCJA);
		
		if ( (produkcja = semctl(semid, PRODUKCJA, GETVAL)) == -1 )
		{
			perror("Producent: błąd odczytu flagi stanu produkcji\n");
		}
	}

	fclose(kolejka);
	
	opusc_semafor(semid, KONIEC);

	if ( shmdt(pamiec) == -1 )
	{
		perror("Producent: Błąd odłączania pamięci dzielonej\n");
		return 12;
	}

	if ( shmctl(shmid, IPC_RMID, NULL) == -1 )
	{
		perror("Producent: Błąd odłączania pamięci dzielonej\n");
		return 13;
	}

	if ( semctl(semid, 4,  IPC_RMID) == -1 )
	{
		perror("Producent: Nie udało się usunąć semaforów\n");
		return 7;
	}

	return 0;
}

int opusc_semafor(int semid, int semnum)
{
	struct sembuf options;
	int zmien_sem;
	options.sem_num = semnum;
	options.sem_op  = -1;
	options.sem_flg = 0;

	while(1) {
		zmien_sem=semop(semid,&options,1);
		if ( zmien_sem==0 || errno != 4 )
			break;
	}
	
		if ( zmien_sem==-1 ) {
			if( errno != 4 ) {
				perror("Producent: błąd opuszczania semafora\n");
				return 5;
			}
		}
		else
			printf("Producent: semafor %d opuszczony\n", semnum);
	

	return 0;
}

int podnies_semafor(int semid, int semnum)
{
	struct sembuf options;
	int zmien_sem;
	options.sem_num = semnum;
	options.sem_op  = 1;
	options.sem_flg = 0;
	
	while(1) {
		zmien_sem=semop(semid,&options,1);
		if ( zmien_sem==0 || errno != 4 )
			break;
		}
	
		if ( zmien_sem==-1 ) {
			if( errno != 4 ) {
				perror("Producent: błąd podnoszenia semafora\n");
				return 5;
			}
		}
		else
			printf("Producent: semafor %d podniesiony\n", semnum);
	
	
	return 0;
}
