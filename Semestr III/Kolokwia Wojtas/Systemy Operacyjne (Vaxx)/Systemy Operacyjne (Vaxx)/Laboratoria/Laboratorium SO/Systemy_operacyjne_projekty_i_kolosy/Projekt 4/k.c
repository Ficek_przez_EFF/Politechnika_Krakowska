#include "pk.h"

int main()
{
	key_t klucz;
	int semid, i, produkt, produkcja, shmid;
	FILE *wyjscie;
	char * pamiec;
	
	if ((klucz = ftok("./p", 'N')) == -1 )
	{
		perror("Konsument: błąd tworzenia klucza\n");
		return 1;
	}
	
	printf("Konsument: utworzono klucz %d\n", klucz);
	
	if ((semid = semget(klucz, 4, IPC_CREAT | 0600)) == -1 )
	{
		perror("Konsument: błąd dostępu do semaforów\n");
		return 2;
	}

	printf("Konsument: Semafory utworzone, id: %d\n", semid);
	
	if ( (wyjscie = fopen(WYJSCIE_FILE, "w")) == NULL )
	{
		perror("Konsument: Nie udało się otworzyć pliku wyjścia\n");
		return 3;
	}
	
	produkcja = semctl(semid, PRODUKCJA, GETVAL);
	
	// Pammięć dzielona
	if ( (shmid = shmget(klucz, 0, IPC_CREAT | 0600 )) == -1 )
	{
		perror("Producent: błąd tworzenia pamięci dzielonej\n");
		return 10;
	}
	
	if ( (pamiec = (char *) shmat(shmid, NULL, 0)) == -1 )
	{
		perror("Producent: błąd dołączania pamięci dzielonej\n");
		return 11;
	}

	while ( produkcja )
	{
		opusc_semafor(semid, PELNY);

		produkt = pamiec[0];
		
		/*if ( ! (bufor = fopen(BUFOR_FILE, "r")))
		{
			perror("Konsument: Nie udało się otwowrzyć bufora\n");
			return 4;
		}
		
		if ( (produkt = fgetc(bufor)) == EOF )
		{
			perror("Konsument: Nie udało się pobrać bufora\n");
			return 5;
		}*/
		if ( produkt != EOF )
		{
			if ( (fputc(produkt, wyjscie)) == EOF )
			{
				perror("Konsument: Błąd zapisu produktu do wyjścia\n");
				return 6;
			}
			
			printf("Konsument: zapis znaku %c na wyjście (kod: %d)\n", produkt, produkt);
			//fclose(bufor);
		}
	
		podnies_semafor(semid, PUSTY);
		
		if ( (produkcja = semctl(semid, PRODUKCJA, GETVAL)) == -1 )
		{
			perror("Konsument: błąd odczytu flagi stanu produkcji\n");
			return 7;
		}
	}
	
	fclose(wyjscie);
	
	podnies_semafor(semid, KONIEC);
	
	return 0;
}

int opusc_semafor(int semid, int semnum)
{
	struct sembuf options;
	int zmien_sem;
	options.sem_num = semnum;
	options.sem_op  = -1;
	options.sem_flg = 0;

	while(1) {
		zmien_sem=semop(semid,&options,1);
		if ( zmien_sem==0 || errno != 4 )
			break;
		}
	
		if ( zmien_sem==-1 ) {
			if( errno != 4 ) {
				perror("Konsument: błąd opuszczania semafora\n");
				return 5;
			}
		}
		else
			printf("Konsument: semafor %d opuszczony\n", semnum);
	

	return 0;
}

int podnies_semafor(int semid, int semnum)
{
	struct sembuf options;
	int zmien_sem;
	options.sem_num = semnum;
	options.sem_op  = 1;
	options.sem_flg = 0;
	
	while(1) {
		zmien_sem=semop(semid,&options,1);
		if ( zmien_sem==0 || errno != 4 )
			break;
		}
	
		if ( zmien_sem==-1 ) {
			if( errno != 4 ) {
				perror("Konsument: błąd podnoszenia semafora\n");
				return 5;
			}
		}
		else
			printf("Konsument: semafor %d podniesiony\n", semnum);
	
	
	return 0;
}
