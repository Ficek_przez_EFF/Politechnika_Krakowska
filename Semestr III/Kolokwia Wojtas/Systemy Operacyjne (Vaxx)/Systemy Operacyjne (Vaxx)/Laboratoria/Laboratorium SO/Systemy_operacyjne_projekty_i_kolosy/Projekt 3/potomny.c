#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <unistd.h>
#include <sys/ipc.h>
#include <errno.h>
#include <sys/sem.h>

// Globalny dentyfikator semafora
int semid;

int opusc(void)
{
	struct sembuf op_array;
	int sem_status;
	
	op_array.sem_num = 0;
	op_array.sem_op  = -1; 
	op_array.sem_flg = 0; 
	
	// Próba opuszczenia semafora
	//while ( 1 )
	//{
		sem_status = semop(semid, &op_array, 1);

		if ( sem_status == 0 )
		{
			printf("Semafor został zamknięty (opuszczony)\n");
			//break;
		}
		if ( sem_status == -1 &&  errno==EINTR)
		{
			semop(semid, &op_array, 1);


		}
		else{
			
			printf("Semop error\n");
			return 4;
		}

	//}
}

int podnies(void)
{
	struct sembuf op_array;
	int sem_status;
	
	op_array.sem_num = 0;
	op_array.sem_op  = 1; 
	op_array.sem_flg = 0; 
	
	// Próba podniesienia semafora
	//while ( 1 )
	//{
		sem_status = semop(semid, &op_array, 1);

		if ( sem_status == 0 )
		{
			printf("Semafor został otwarty (podniesiony)\n");
			//break;
		}
		if ( sem_status == -1 && errno==EINTR)
			{
			semop(semid, &op_array, 1);
			}
			else
			{

			printf("Semop error\n");
			return 4;
			}

	//}
}

int main(int argc, char* argv[])
{
	if ( argc < 2 )
	{
		printf("Nieprawidłowa liczba argumentów.\nWywołanie: ./potomny liczba_sk\n");
		return 1;
	}
	
	key_t semkey;
	int i;
	long liczba_sk;
	i = 0;
	
	liczba_sk = strtol(argv[1], NULL, 10);
	
	// Uzyskiwanie dostępu do klucza i semafora
	if ((semkey = ftok("./powielacz", 'X')) == (key_t) -1 )
	{
		printf("Nie udało się utworzyć klucza IPC dla semaforów\n");
		return 2;
	}
		
	if ((semid = semget(semkey, 1, 0600 | IPC_CREAT )) == -1 )
	{
		printf("Nie udało się utworzyć semafora lub semafor już istnieje\n");
		return 3;
	}
	
	for ( i = 0; i < liczba_sk; i++ )
	{
		opusc();
		
		printf("Sekcja krytyczna prodesu %d\n", getpid());
		printf("Liczba czekających: %d\n", semctl(semid, 0, GETNCNT));
		printf("Wartosc semafora: %d\n", semctl(semid, 0, GETVAL));
		//sleep(1);
		
		podnies();
	}

	return 0;
}
