#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#define MAX_SK 20000000

int main(int argc, char* argv[])
{
	if ( argc < 4 )
	{
		printf("Nieprawidłowe wywołanie.\nPoprawne: ./powielacz program_potomny liczba_pp liczba_sk\n");
		return 1;
	}
	else
	{
		int i, maxp, semid;
		long liczba_pp, liczba_sk;
		key_t semkey;
		char line[100];
		
		liczba_pp = strtol(argv[2], NULL, 10);
		liczba_sk = strtol(argv[3], NULL, 10);

		if ( liczba_pp <= 0 || liczba_sk <= 0 )
		{
			printf("Nieprawidłowa liczba procesów lub sekcji krytycznych.\n");
			return 10;
		}
		
		FILE * f = popen("ulimit -u", "r");
		fgets(line, sizeof line, f);
		sscanf(line, "%d", &maxp);
		pclose(f);

		if ( liczba_pp > maxp || liczba_sk > MAX_SK )
		{
			printf("Zbyt duża liczba procesów lub SK\n");
			return 11;
		}

		// Tworzenie klucza i semafora
		if ((semkey = ftok("./powielacz", 'X')) == (key_t) -1 )
		{
			printf("Nie udało się utworzyć klucza IPC dla semaforów\n");
			return 1;
		}
		
		if ((semid = semget(semkey, 1, 0600 | IPC_CREAT )) == -1 )
		{
			printf("Nie udało się utworzyć semafora lub semafor już istnieje\n");
			return 2;
		}
		
		// Ustawianie wartości początkowej semafora
		if ( semctl(semid, 0, SETVAL, 1) == -1 )
		{
			printf("Nie udało się ustawić wartości semafora.\n");
			return 3;
		}
	
		printf("semkey: %d\n", semkey);
		printf("semid: %d\n", semid);
		printf("Uruchamiam %d procesy(ów) rywalizujących o sk (każdy %d razy)\n", liczba_pp, liczba_sk);
		printf("Początkowa wartość semafora \tgetval: %d\n", semctl(semid, 0, GETVAL, 0));
		// Uruchamianie procesów potomnych
		for ( i = 0; i < liczba_pp; i++ )
		{
			switch(fork())
			{
				case -1:
					printf("Fork error\n");
					return 4;
					break;
				case 0:
					if ( execl(argv[1], argv[1], argv[3], NULL) == -1 )
					{
						printf("Exec error\n");
						return 5;
					}
					break;
			}
		}
		
		// Czekanie na zakoñczenie procesów potomnych
		int status, w_status;
		for ( i = 0; i < liczba_pp; i++ )
		{
			w_status = wait(&status);
			if ( w_status == -1 )
			{
				printf("Wait error\n");
				return 6;
			}
			
			printf("Proces potomny %d zakończył się z kodem powrotu %d\n", w_status, status);
		}
		
		// Usuwanie semafora
		if ( semctl(semid, 0, IPC_RMID) == -1 )
		{
			printf("Nie udało się usunąć semafora\n");
			return 7;
		}
		else
			printf("Semafor został usunięty\n");
		
		return 0;
	}
}
