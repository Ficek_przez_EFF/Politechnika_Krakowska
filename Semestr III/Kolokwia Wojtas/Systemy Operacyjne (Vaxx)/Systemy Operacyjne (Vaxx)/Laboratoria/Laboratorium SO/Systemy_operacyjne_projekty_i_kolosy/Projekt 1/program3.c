#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void processInfo(void);

int main()
{
	int i, status;
	
	for ( i = 0; i < 3; i++ )
	{
		switch(fork())
		{
			case -1:
				perror("fork error");
				return 1;
				break;
			case 0:
				execl("/bin/date", "date",  NULL);
				break;
			default:
				wait(&status);
				break;
		}
	}
	
	return 0;
}
