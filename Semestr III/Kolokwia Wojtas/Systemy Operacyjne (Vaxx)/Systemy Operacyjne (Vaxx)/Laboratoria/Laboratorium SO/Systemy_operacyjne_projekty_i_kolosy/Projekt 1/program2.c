#include <stdio.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <string.h>

void processInfo(void);

int main()
{
	int i, status, depth = 0;
	
	printf("Proces macierzysty:\n");
	processInfo();
	
	for ( i = 0; i < 3; i++ )
	{
		switch(fork())
		{
			case -1:
				perror("fork error");
				return 1;
				break;
			case 0:
			    /* Drzewo procesów
				depth++;
				int a;
				char buf[30];
				char buf2[20];
				for ( a = 0; a < depth*3; a++ )
					buf[a] = '-';
				buf[a] = '\0';
				sprintf(buf2, "%ld (PPID: %ld)\n", getpid(), getppid());
				strcat(buf, buf2);
				printf("%s", buf);
				*/
				
				//processInfo();
				printf("A\n");
				break;
			default:
			
				switch (fork()){
				case -1:
				perror("fork error");
				return 1;
				break;
			case 0:
				
				printf("A\n");
				break;
			
				}
			
			
				wait(&status);
				break;
		}
	}
	
	return 0;
}

void processInfo()
{
	printf("UID:\t%ld\tidentyfikator użytkownika (rzeczywisty)\n", getuid());
	printf("GID:\t%ld\tidentyfikator grupy użytkownika (rzeczywisty)\n", getgid());
	printf("PID:\t%ld\tidentyfikator procesu\n", getpid());
	printf("PPID:\t%ld\tidentyfikator procesu macierzystego\n", getppid());
}
