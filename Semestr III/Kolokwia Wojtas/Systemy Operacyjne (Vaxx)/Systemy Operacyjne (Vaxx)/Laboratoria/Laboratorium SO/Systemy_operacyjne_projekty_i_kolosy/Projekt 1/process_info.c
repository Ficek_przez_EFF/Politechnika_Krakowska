#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
	printf("UID:\t%ld\tidentyfikator użytkownika (rzeczywisty)\n", getuid());
	printf("GID:\t%ld\tidentyfikator grupy użytkownika (rzeczywisty)\n", getgid());
	printf("PID:\t%ld\tidentyfikator procesu\n", getpid());
	printf("PPID:\t%ld\tidentyfikator procesu macierzystego\n", getppid());
	
	return 0;
}
