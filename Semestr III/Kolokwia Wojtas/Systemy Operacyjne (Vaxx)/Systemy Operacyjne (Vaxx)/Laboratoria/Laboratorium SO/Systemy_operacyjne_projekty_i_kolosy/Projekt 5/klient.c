#include "klient_serwer.h"

int client_id;
int pause_pos;
char * generate_message();
void * wysylanie();
void * odbieranie();

int main()
{
	pause_pos = 0;
	pthread_t th_wysylanie, th_odbieranie;
	
	srand(time(NULL));
	client_id = getpid();

	if ( (klucz = ftok("./klient", 'J')) == -1 )
	{
		perror("Klient: Błąd tworzenia klucza");
		return 1;
	}
	
	if ( (msgid = msgget(klucz, IPC_CREAT | 0600 )) == -1 )
	{
		perror("Klient: Bład tworzenia kolejki komunikatów");
		return 2;
	}

	if ( pthread_create(&th_wysylanie, NULL, wysylanie, NULL) == -1 )
	{
		perror("Error creating thread 1");
		return 1;
	}
	
	if ( pthread_create(&th_odbieranie, NULL, odbieranie, NULL) == -1 )
	{
		perror("Error creating thread 2");
		return 2;
	}
	
	if ( pthread_join(th_wysylanie, NULL) == -1 
		|| pthread_join(th_odbieranie, NULL) == -1 )
	{
		perror("Error joining threads");
		return 3;
	}
	
	return 0;	
}

char * generate_message()
{
	char * message = (char*)malloc(MAX_LENGTH-1);
	int i, len;
	
	sprintf(message, "%d:", client_id);
	len = strlen(message);
	
	// Losowanie znaków (w ASCII znaki a-z: 97-122)
	for ( i = len; i < MSG_LENGTH + len; i++ )
		message[i] = (char)(97 + rand() % (122-97));
		
	message[i] = '\0';
	
	return message;
}

void * wysylanie()
{
	while ( 1 )
	{
		struct msgbuf msg;
		
		msg.msg_type = 1;
		strcpy(msg.text, generate_message());
		
		if ( msgsnd(msgid, &msg, MAX_LENGTH, 0) != 0 )
		{
			perror("Klient: błąd wysyłania komunikatu do serwera");
			return 3;
		}
			
		pause_pos = strchr(msg.text, ':')-msg.text+1;
		printf("Klient [%d]: wysłano komunikat  \"%s\"\n", client_id, msg.text+pause_pos);
		
		// Bez sleepa kolejka szybko się zapycha i serwer 
		// nie może zwrócić przerobionej wiadomości
		// co powoduje wstrzymanie całego programu i uniemożliwia
		// jego dalsze działanie (kolejka się zapycha, serwer nic nie zwraca)
		sleep(1);
	}
}

void * odbieranie()
{
	while ( 1 )
	{
		struct msgbuf msg;
		
		msg.msg_type = client_id;
		if ( (msgrcv(msgid, &msg, MAX_LENGTH, msg.msg_type, 0)) == -1 )
		{
			perror("Klient: Błąd odbierania wiadomości od klienta");
			return 3;
		}
			
		pause_pos = strchr(msg.text, ':')-msg.text+1;
		printf("Klient [%d]: odebrano komunikat \"%s\"\n", client_id, msg.text+pause_pos);
	}
}
