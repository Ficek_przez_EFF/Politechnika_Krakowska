#include "klient_serwer.h"

int pid, pause_pos;
void to_upper(char * msgtext);
int parse_pid(char * msgtext);
void termination_handler(int sig_n);

int main()
{
	struct msgbuf msg;
	
	if ( (klucz = ftok("./klient", 'J')) == -1 )
	{
		perror("Serwer: Błąd tworzenia klucza");
		return 1;
	}
	
	if ( (msgid = msgget(klucz, IPC_CREAT | 0600 )) == -1 )
	{
		perror("Serwer: Bład uzyskiwania dostępu do kolejki komunikatów");
		return 2;
	}
	
	signal(SIGCLD, SIG_IGN);
	signal(SIGINT, termination_handler);
	
	while ( 1 )
	{
		msg.msg_type = 1;
		if ( (msgrcv(msgid, &msg, MAX_LENGTH, msg.msg_type, 0)) == -1 )
		{
			perror("Serwer: Błąd odbierania wiadomości od klienta");
			return 3;
		}
		
		pid = parse_pid(msg.text);
		pause_pos = strchr(msg.text, ':')-msg.text+1;
		printf("Serwer: Odebrano komunikat od klienta [%d]: \"%s\"\n", pid, msg.text+pause_pos);
		
		to_upper(msg.text);
		msg.msg_type = pid;
		if ( (msgsnd(msgid, &msg, MAX_LENGTH, 0)) == -1 )
		{
			perror("Serwer: Błąd wysyłania komunikatu");
			return 4;
		}
		
		printf("Serwer: Wysłano komunikat do klienta  [%d]: \"%s\"\n", pid, msg.text+pause_pos);
	}
	
	return 0;
}

void to_upper(char * msgtext)
{
	int i;
	char * ptrc;
	
	for ( i = 0, ptrc = msgtext; i < MAX_LENGTH; ++i, ++ptrc )
		if ( (int)*ptrc > 96  )
			(*ptrc) = (*ptrc)-32;
}

int parse_pid(char * msgtext)
{
	int pid;
	pid = strtol(msgtext, NULL, 10);
	
	return pid;
}

void termination_handler(int sig_n)
{
	if ( msgctl(msgid, IPC_RMID, 0) == -1 )
	{
		perror("Serwer: Błąd usuwania kolejki komunikatów");
		exit(5);
	}
	
	exit(0);
}
