#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/types.h> 
#include <sys/ipc.h>
#include <signal.h>

#define MSG_LENGTH 30
#define MAX_LENGTH ((MSG_LENGTH)+(15))

int msgid;
key_t klucz;

struct msgbuf {
	long msg_type;
	char text[MAX_LENGTH];
};

