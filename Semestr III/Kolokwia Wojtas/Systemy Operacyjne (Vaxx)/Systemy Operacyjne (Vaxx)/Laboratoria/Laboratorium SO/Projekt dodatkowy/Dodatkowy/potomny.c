#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#define UP   0
#define DOWN 1

int count_type = UP;
int ppid;

// Funkcja obsługująca sygnał
void sig_handler()
{
	// Zmiana trybu odliczania
	printf("Wyłapano sygnał, zmiana trybu odliczania\n");
	count_type = DOWN;
}

int main(int argc, char **argv)
{
	int i = 0;
	printf("Proces potomny o PID: %d zaczyna działanie\n", getpid());
	
	// Przekazanie obsługi sygnału SIGUSR1 do funkcji sig_handler
	signal(SIGUSR1, sig_handler);
	
	while ( 1 )
	{
		i = (count_type == UP) ? i+1 : i-1;
		
		printf("%d\n", i);
		sleep(1);
		
		if ( i <= 0 )
			break;
	}
	
	printf("Odliczanie zakończone\n");
	
	return 0;
}
