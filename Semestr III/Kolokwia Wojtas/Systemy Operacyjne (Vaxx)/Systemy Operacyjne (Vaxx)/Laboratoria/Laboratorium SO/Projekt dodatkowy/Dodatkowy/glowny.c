#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

int child_pid;

// Funkcja obsługująca sygnał SIGUSR1 (10)
void loop_signal_handler()
{
	// Wysłanie sygnału do procesu potomnego
	kill(child_pid, SIGUSR1);
}

int main()
{
	int status = 0;
	printf("Proces główny, PID: %d\n", getpid());
	printf("Aby wysłać sygnał użyj polecenia\nkill -10 %d\n", getpid());
	
	// Przekazanie obsługi sygnału funkcji loop_signal_handler
	signal(SIGUSR1, loop_signal_handler);
	
	switch ( child_pid = fork())
	{
		case -1:
			perror("Fork error");
			return 1;
		case 0:
			// Wywołanie procesu potomnego
			execl("./potomny", "potomny", NULL);
			break;
		default:
			break;
	}
	
	// Czekanie na zakończenie procesu potomnego
	wait(&status);
	
	printf("Proces potomny (%d) zakończył się z kodem powrotu %d\n", child_pid, status);
	return 0;
}
