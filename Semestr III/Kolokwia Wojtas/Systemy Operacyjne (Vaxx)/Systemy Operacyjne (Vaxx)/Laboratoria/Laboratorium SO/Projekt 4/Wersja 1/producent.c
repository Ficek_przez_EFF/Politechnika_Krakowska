#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <errno.h>

int semafor;
key_t klucz;

/**************** TWORZENIE SEMAFORA ********************/

static void tworzenie_semafora()
{
  semafor = semget(klucz, 2, 0600 | IPC_CREAT | IPC_EXCL);  //Minimalne prawa 'rw'
  
  if(semafor == -1)
  {
   printf("Blad tworzenia semafora (producent)\m");
   exit(-1);
  }
  else
   printf("Semafor zostal utworzony, id = %d z kluczem = %d\n", semafor, klucz);
}
/**************** UZYSKANIE DOSTEPU DO SEMAFORA ********************/

static uzyskaj_dostep()
{
 semafor = semget(klucz, 2, 0600 | IPC_CREAT);
 
 if(semafor == -1)
 {
  printf("Blad uzyskania dostepu do semafora (producent)\n");
  exit(-1);
 }
 else
  printf("Proces %d uzyskal dostep do semafora (producent)\n", getpic());
  
}

/**************** USTAWIENIE SEMAFORA ********************/

static void ustaw_semafor(int sem, int na)
{
 int ustaw_semafor;
 
 ustaw_semafor = semctl(semafor, sem, SETVAL, na);
 
 if(ustaw_semafor == -1)
 {
  printf("Nie mozna ustawic semafora (producent)\n");
  exit(-1);
 }
 else
  printf("Semafor zostal ustawiony\n");
  
}
/**************** OPUSC SEMAFOR ********************/

static void opusc_semafor(int nr_sem)
{
 int status;
 struct sembuf bufor_semafora;
 
 bufor_semafora.sem_num = nr_sem;
 bufor_semafora.sem_op = -1;
 bufor_semafora.sem_flg = SEM_UNDO;
 
 while(1)
 {
  status = semop(semafor, &bufor_semafora, 1);
  
  if(status == 0 | errno != 4)
   break;
 }
 
 if (status == -1 && errno != 4)
 {
  printf("Blad opuszczenia semafora (producent)\n");
  exit(-1);
 }
 else
  printf("Opuszczenie semafora\n");
 
}

/**************** PODNIES SEMAFOR ********************/

static void podnies_semafor(int nr_sem)
{
 int status;
 struct sembuf bufor_semafora;
 
 bufor_semafora.sem_num = nr_sem;
 bufor_semafora.sem_op = 1;
 bufor_semafora.sem_flg = SEM_UNDO;
 
 while(1)
 {
  status = semop(semafor, &bufor_semafora, 1);
  
  if(status == 0 | errno != 4)
   break;
 }
 
 if (status == -1 && errno != 4)
 {
  printf("Blad podniesienia semafora (producent)\n");
  exit(-1);
 }
 else
  printf("Podniesienie semafora\n");
 
}

/**************** USUNIECIE SEMAFORA ********************/

static void usun_semafor()
{
 int status;
 
 status = semctl(semafor, 0, IPC_RMID);
 
 if(status == -1)
 {
  printf("Nie mozna usunac semafora (producent)\n");
  ecit(-1);
 }
 else
  printf("Semafor zostal usuniety: %d\n", semafor);
  
}


int main() 
{
/**************** SEGMENT PAMIECI DZIELONEJ ********************/
 char *adres_pamieci;
 int segment;
 key_t klucz_segmentu;
 
 klucz_segmentu = ftok(".", 'Z');
 
 segment = shmget(klucz_segmentu, sizeof(char), IPC_CREAT | IPC_EXCL | 0600);
 
 if(segment == -1)
 {
  printf("Nie mozna utworzyc segmentu pamieci (producent)\n");
  exit(-1);
 }
 else
  printf("Utworzono segment pamieci (producent)\n");
  
  adres_pamieci = (char *) shmat(segment, 0, 0);
  
  if(adres_pamieci == -1)
  {
   printf("Nie mozna uzyskac adresu segmentu pamieci (producent)\n");
   exit(-1);
  }
  else
   printf("Uzyskano adres segmentu pamieci (producent)\n");

/**************** TWORZENIE KLUCZA ********************/

 klucz = ftok(".",'A');
 
 if(klucz == -1)
 {
  printf("Blad tworzenia klucza (producent)\n");
  exit(-1);
 }
 else
  printf("Zoastal utworzony klucz: %d (producent)\n", klucz);
  
/**************** TWORZENIE SEMAFORA ************/

 tworzenie_semafora();
 
/**************** USTAWIENIE SEMAFORA ************/
 
  ustaw_semafor(0, 1);
  ustaw_semafor(1, 0);

/**************** PLIK WEJSCIE ************/
 char znak;
 FILE *wejscie;

 wejscie = fopen("wejscie", "r");
 
 if(wejscie == NULL)
 {
  printf("Nie mozna otworzyc pliku wejscie dla odczytu (producent)\n");
  exit(-1);
 }
 else
 {
     while(!feof(wejscie)
     {
      znak = fgetc(wejscie);
     
      if(znak != EOF)
      {
       opusc_semafor(0);
      
       *adres_pamieci = znak;
       printf("Znak = %c, adres_pamieci = %c (producent)\n", znak, *adres_pamieci);
      
       podnies_semafor(1);
      }
     }
     
     opusc_semafor(0);
     *adres_pamieci = EOF;
     podnies_semafor(1)

     
 }

 fclose(wejscie);
 
/**************** ODŁĄCZENIE PAMIECI DZIELONEJ ************/

 shmdt(adres_pamieci);
 
 printf("Producent zakonczyl dzialanie\n");

 return 0;

}
