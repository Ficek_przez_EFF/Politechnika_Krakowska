#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <errno.h>

int semafor;
key_t klucz;

/**************** TWORZENIE SEMAFORA ********************/

static void tworzenie_semafora()
{
  semafor = semget(klucz, 2, 0600 | IPC_CREAT | IPC_EXCL);  //Minimalne prawa 'rw'
  
  if(semafor == -1)
  {
   printf("Blad tworzenia semafora (konsument)\n");
   exit(-1);
  }
  else
   printf("Semafor zostal utworzony, id = %d z kluczem = %d\n", semafor, klucz);
}
/**************** UZYSKANIE DOSTEPU DO SEMAFORA ********************/

static uzyskaj_dostep()
{
 semafor = semget(klucz, 2, 0600 | IPC_CREAT);
 
 if(semafor == -1)
 {
  printf("Blad uzyskania dostepu do semafora (konsument)\n");
  exit(-1);
 }
 else
  printf("Proces %d uzyskal dostep do semafora (konsument)\n", getpic());
  
}

/**************** USTAWIENIE SEMAFORA ********************/

static void ustaw_semafor(int sem, int na)
{
 int ustaw_semafor;
 
 ustaw_semafor = semctl(semafor, sem, SETVAL, na);
 
 if(ustaw_semafor == -1)
 {
  printf("Nie mozna ustawic semafora (konsument)\n");
  exit(-1);
 }
 else
  printf("Semafor zostal ustawiony\n");
  
}
/**************** OPUSC SEMAFOR ********************/

static void opusc_semafor(int nr_sem)
{
 int status;
 struct sembuf bufor_semafora;
 
 bufor_semafora.sem_num = nr_sem;
 bufor_semafora.sem_op = -1;
 bufor_semafora.sem_flg = SEM_UNDO;
 
 while(1)
 {
  status = semop(semafor, &bufor_semafora, 1);
  
  if(status == 0 | errno != 4)
   break;
 }
 
 if (status == -1 && errno != 4)
 {
  printf("Blad opuszczenia semafora (konsument)\n");
  exit(-1);
 }
 else
  printf("Opuszczenie semafora\n");
 
}

/**************** PODNIES SEMAFOR ********************/

static void podnies_semafor(int nr_sem)
{
 int status;
 struct sembuf bufor_semafora;
 
 bufor_semafora.sem_num = nr_sem;
 bufor_semafora.sem_op = 1;
 bufor_semafora.sem_flg = SEM_UNDO;
 
 while(1)
 {
  status = semop(semafor, &bufor_semafora, 1);
  
  if(status == 0 | errno != 4)
   break;
 }
 
 if (status == -1 && errno != 4)
 {
  printf("Blad podniesienia semafora (konsument)\n");
  exit(-1);
 }
 else
  printf("Podniesienie semafora\n");
 
}

/**************** USUNIECIE SEMAFORA ********************/

static void usun_semafor()
{
 int status;
 
 status = semctl(semafor, 0, IPC_RMID);
 
 if(status == -1)
 {
  printf("Nie mozna usunac semafora (konsument)\n");
  ecit(-1);
 }
 else
  printf("Semafor zostal usuniety: %d\n", semafor);
  
}


int main() 
{
/**************** SEGMENT PAMIECI DZIELONEJ ********************/
 char *adres_pamieci;
 int segment;
 key_t klucz_segmentu;
 
 klucz_segmentu = ftok(".", 'Z');
 
 segment = shmget(klucz_segmentu, 0, 0600 | IPC_CREAT);
 
 if(segment == -1)
 {
  printf("Nie mozna uzyskac dostepu do segmentu pamieci (konsument)\n");
  exit(-1);
 }
 else
  printf("Uzyskano dost�p segment pamieci (konsument)\n");
  
  adres_pamieci = (char *) shmat(segment, 0, 0);
  
  if(adres_pamieci == -1)
  {
   printf("Nie mozna uzyskac adresu segmentu pamieci (konsument)\n");
   exit(-1);
  }
  else
   printf("Uzyskano adres segmentu pamieci (konsument)\n");

/**************** TWORZENIE KLUCZA ********************/

 klucz = ftok(".",'A');
 
 if(klucz == -1)
 {
  printf("Blad tworzenia klucza (producent)\n");
  exit(-1);
 }
 else
  printf("Zoastal utworzony klucz: %d (producent)\n", klucz);
  
/**************** UZYSKANIE DOSTEPU DO SEMAFORA ************/

 uzyskaj_dostep();
 
/**************** PLIK WYJSCIE ************/
 char znak;
 FILE *wyjscie;

 wyjscie = fopen("wejscie", "w");
 
 if(wyjscie == NULL)
 {
  printf("Nie mozna otworzyc pliku wyjscie dla zapisu (konsument)\n");
  exit(-1);
 }
 else
 {
     
     while(znak != EOF)
     {
      opusc_semafor(1);
      
      znak = *adres_pamieci;
      
      if(znak != EOF)
      {
       fputc(znak, wyjscie);
       printf("Znak = %c (konsument)\n", znak);
       
       podnies_semafor(0);
      }
      
     }
     
     //fputc('\n\, wyjscie);
     
     //podnies_semafor(1);
 }

 fclose(wyjscie);
 
/**************** OD��CZENIE PAMIECI DZIELONEJ ************/

 shmdt(adres_pamieci);
 
/**************** USUNIECIE PAMIECI DZIELONEJ ************/

 shmctl(segment, IPC_RMID, 0);
 
/**************** USUNIECIE SEMAFORA ************/

 usun_semafor();
 
 printf("Konsument zakonczyl dzialanie \n");

 return 0;

}
