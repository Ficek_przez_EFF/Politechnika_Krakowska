#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <sys/shm.h>

int semafor;

static void opusc_semafor(int);
static void podnies_semafor(int);
static void usun_semafor(void);


/******************** UTWORZENIE NOWEGO SEMAFORA / KLUCZ ***********************/

static void utworz_nowy_semafor(void)
{
	key_t key;
	key = ftok(".", 'S');
	semafor = semget(key, 2, 0600|IPC_CREAT|IPC_EXCL); 
	if (semafor==-1) 
	{
		printf("Nie mozna utworzyc zbioru semaforow.\n");
		exit(-1);
	}
	else
	{
		printf("Zbior semaforow zostal utworzony = %d z kluczem = %u\n", semafor, key);
	}
}


/******************** UZYSKANIE DOSTEPU ***********************/

static void uzyskanie_dostepu(void)
{
	key_t key;
	key = ftok(".", 'S');

	semafor = semget(key, 2, 0600|IPC_CREAT); 
	if  (semafor == -1)
	{
		perror("Blad w sem.c, w semget dla dostepu do semafora\n");
		printf("Nie moge uzyskac dostepu do semafora.\n");
		exit(-1);
	} 
	else
	{
		printf("Proces %d uzyskal dostep do zbioru semaforow = %d\n",getpid(),semafor);
	}
}



/******************** USTAWIENIE SEMAFORA ***********************/

static void ustaw_semafor(int sem,int na)
{
	int ustaw_sem;
	ustaw_sem = semctl(semafor,sem,SETVAL,na);

	if (ustaw_sem == -1)
	{
		printf("Nie mozna ustawic semafora.\n");
		exit(-1);
	}
	else
	{
		printf("Semafor zostal ustawiony.\n");
	}
}


/******************** OPUSZCZENIE SEMAFORA ***********************/


static void opusc_semafor(int sem)
{
	int zmien_sem;
	struct sembuf bufor_sem;
	bufor_sem.sem_num=sem;
	bufor_sem.sem_op=-1;
	bufor_sem.sem_flg=0;

	while(1)
	{
		zmien_sem = semop(semafor,&bufor_sem,1);

		if(zmien_sem==0)
			break;

        }
  
	if(zmien_sem==-1)
	{
		uzyskanie_dostepu();
	}
	//else
	//{
	//	printf("Semafor %d zostal opuszczony.\n", sem);
	//}
}


/******************** PODNIESIENIE SEMAFORA ***********************/

static void podnies_semafor(int sem)
{
	int zmien_sem;
	struct sembuf bufor_sem;

	bufor_sem.sem_num = sem;
	bufor_sem.sem_op = 1;
	bufor_sem.sem_flg=0;

	while(1)
	{
	zmien_sem=semop(semafor,&bufor_sem,1);

	if(zmien_sem==0)
		break;
	}
	
	if(zmien_sem==-1)
	{
		uzyskanie_dostepu();
	}
        //else
	//{
	//	printf("Semafor %d zostal podniesiony.\n\n", sem);
        //}
}


/******************** USUNIECIE SEMAFORA ***********************/

static void usun_semafor()  
{
	int sem;
	sem = semctl(semafor, 0, IPC_RMID);	//IPC_RMID - usunięcie danego zbioru semaforów
	
        if (sem == -1)
	{
		printf("Nie mozna usunac semafora.\n");
		exit(-1);
	}
	else
	{
		printf("Semafor zostal usuniety : %d\n",sem);
	}
}


/******************** STAN SEMAFORA ***********************/

static void stan_semafora(void)
{
	int stan;

	if ((stan = semctl(semafor,0,GETVAL)) == -1)
	{
		perror ("Blad GETVAL\n");  
	} 
	
  printf("Stan semafora %d\n", stan);
}


/******************** OCZEKUJACE ***********************/

static void liczba_prcesow_czekajacych(void)
{
	int liczba;
	if ((liczba=semctl(semafor,0,GETNCNT)) == -1)
	{
		perror ("Blad GETCNT\n");
	}
	printf("Liczba semaforow czekajacych na podniesienie semafora %d\n",liczba);	
}
