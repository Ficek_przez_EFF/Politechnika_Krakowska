#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <sys/shm.h>
//#include <time.h>
#include"semafor.c"

int main(int argc,char *argv[])
{
	
	char znak;
	char *adres;
	int id_segmentu;

/******************** TWORZENIE KLUCZA ***********************/

	key_t key_seg = ftok(".", 'I');


	FILE *we;

/******************** PAMIEC DZIELONA ***********************/	
	
	if((id_segmentu = shmget(key_seg,sizeof(char),IPC_CREAT|IPC_EXCL|0600)) == -1)	//utworzenie segmentu pamieci dzielonej
	{
		printf("Blad tworzenie pamieci dzielonej (producent)\n");
		return -1;
	}
	else
	{
		printf("PRODUCENT:  utoworzono segment pamieci dzielonej\n");
	}
	
	
	if((adres = (char *) shmat(id_segmentu, 0, 0)) == -1)
	{
		printf("Blad dolaczania pamieci dzielonej (producent)\n");
	 	return -1;
	}
	else
	{
		printf("PRODUCENT: uzyskano adres segmantu pamieci dzielonej\n");
	}
	

/******************** TWORZENIE SEMAFORA ***********************/

	utworz_nowy_semafor();
	

/******************** USTAWIENIE SEMAFORA ***********************/

	ustaw_semafor(0,1);
	ustaw_semafor(1,0);
	

/******************** PLIK ***********************/
	
	we = fopen(argv[1],"r");
			
	if(we != NULL)
	{
		while(feof(we) == 0)
		{	
			znak = getc(we);
			
			if(znak != EOF)
			{
				printf("PR: ");
				opusc_semafor(0);
				*adres = znak;
				printf("PRODUCENT: pobrano '%c', wyprodukowano '%c'\n", znak, *adres);
				
				printf("PR.: ");
		  	podnies_semafor(1);
			}
		}
		
		printf("PR.: ");

		opusc_semafor(0);		//Opuszczenie semafora
		*adres = EOF;
		
		printf("PR.: ");
		podnies_semafor(1);		//Podniesienie semafora
	}
	else
	{
		printf("PRODUCENT: Blad otwarcia pliku %s do odczytu lub plik jest pusty!\n", argv[1]);
		shmdt(adres);
		shmctl(id_segmentu, IPC_RMID, 0);
		usun_semafor();
		printf("PRODUCENT: koniec dzialania\n");
		return 0;	
	}
	
	fclose(we);


/******************** ODLACZENIE PAMIECI DZIELONEJ ***********************/
	shmdt(adres);
	
	
	printf("PRODUCENT: koniec dzialania\n");
	
	return 0;
}
