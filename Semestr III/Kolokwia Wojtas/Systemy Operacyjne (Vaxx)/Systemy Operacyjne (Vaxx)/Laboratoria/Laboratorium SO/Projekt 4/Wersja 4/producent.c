#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>

int buff_write(int in);  // wypelnienie bufora

char buff_filename[6]="bufor";

#define SEM_PRODUCENT 0   // numer semafora producenta
#define SEM_KONSUMENT 1   // numer semafora konsumenta

void sem_p(int sem_id, int sem_num);    // opuszczanie semafora
void sem_v(int sem_id, int sem_num);    // podnoszenie semafora
void sem_rem(int sem_id);               // usuwanie zbioru semaforow


//****MAIN******
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("Program wywolany nieprawidlowo. Podaj:\n$ ./%s <plik_wejscia>\n", argv[0]);
        exit(-1);
    }
    
  
    FILE *in;
    in = fopen(argv[1], "r");
    if(in == NULL)
    {
        perror("Blad przy otwieraniu pliku wejsciowego!");
        exit(-1);
    }
    
    key_t key;
    int sem_id;
    
    key = ftok(".", 'Z');
    if(key == -1)
    {
        perror("Blad przy tworzeniu klucza!");
        exit(-1);
    }
    
    sem_id = semget(key, 2, IPC_CREAT|IPC_EXCL|0600);
    if(sem_id == -1)
    {
        perror("Blad przy tworzeniu zbioru semaforow!");
        exit(-1);
    }
    
    /*
     * Ustawiamy startowe wartosci semaforow:
     * - producent jest otwarty
     * - konsument jest zamkniety
     */
    if(semctl(sem_id, SEM_PRODUCENT, SETVAL, 1) == -1)
    {
        perror("Blad ustawiania startowej wartosci semafora producenta!");
        exit(-2);
    }
    
    if(semctl(sem_id, SEM_KONSUMENT, SETVAL, 0) == -1)
    {
        perror("Blad ustawiania startowej wartosci semafora konsumenta!");
        exit(-2);
    }
    
    printf("Stworzylem zbior semaforow! %d\n", sem_id);

    //otwieramy plik bufora
    FILE *fbuff; 
     
    fbuff = fopen(buff_filename, "w+");
    if(fbuff == NULL)
    {
        perror("Blad otwierania pliku bufora!");
        sem_rem(sem_id);
        exit(-3);
    }
    fclose(fbuff);
    printf("Stworzono bufor\n");
    
    
    printf("ROZPOCZYNAM PRODUKCJE!\n\n");
   
    int r;
    
    // licznik produktow
    int i = 0;
    
    while((r=fgetc(in))!=EOF)
    {
        
        printf("Oczekuje na pusty bufor\n");
        
        // opuszczamy semafor producenta
        sem_p(sem_id, SEM_PRODUCENT);
        
        // odczytana znak zapisywany jest do bufora
        buff_write(r);
       
        printf("Produkuje... wyprodukowalem: %c\n",r);
            i++;
        
        // podnosimy semafor konsumenta
        sem_v(sem_id, SEM_KONSUMENT);
       
    }
    sem_p(sem_id, SEM_PRODUCENT);
    buff_write(r);
    sem_v(sem_id, SEM_KONSUMENT);
    
    printf("KONIEC PRODUKCJI \t Wyprodukowano: %d\n\n", i);
    
    // czekamy na zebranie EOF przez konsumenta
    sem_p(sem_id, SEM_PRODUCENT);
   
    remove(buff_filename);
  
    sem_rem(sem_id);
    
    fclose(in);
   
    exit(0);
}


//******************************definicje funkcji**************************
//funkcja wpisujaca do bufora
int buff_write(int in)
{
    FILE *fbuff;
    fbuff = fopen(buff_filename, "w");
    if(fbuff == NULL)
    {
        return 0;
    }
    fputc(in, fbuff);
    fclose(fbuff);
    return 1;
}

//funkcja opuszczajaca semafor
void sem_p(int sem_id, int sem_num)
{
    struct sembuf buf;
    int semop_ret;
    
    buf.sem_num = sem_num;
    buf.sem_op = -1;
    buf.sem_flg = 0;
    
    while((semop_ret = semop(sem_id, &buf, 1)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Blad opuszczenia semafora!");
        exit(EXIT_FAILURE);
    }
}

//funkcja podnoszaca semafor
void sem_v(int sem_id, int sem_num)
{
    struct sembuf buf;
    int semop_ret;
    
    buf.sem_num = sem_num;
    buf.sem_op = 1;
    buf.sem_flg = 0;
    
    while((semop_ret = semop(sem_id, &buf, 1)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Blad podniesienia semafora!");
        exit(EXIT_FAILURE);
    }
}

//funkcja usuwajaca zbior semaforow
void sem_rem(int sem_id)
{
    if(semctl(sem_id, 0, IPC_RMID) == -1)
    {
        perror("Blad usuwania zbioru semaforow!");
        exit(EXIT_FAILURE);
    }
    printf("Usunieto zbior semaforow %d\n", sem_id);
}
