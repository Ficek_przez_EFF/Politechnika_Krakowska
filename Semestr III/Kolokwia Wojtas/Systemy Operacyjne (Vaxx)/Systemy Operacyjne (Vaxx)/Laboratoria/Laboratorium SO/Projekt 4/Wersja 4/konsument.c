#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>

int buff_read();        // odczytanie bufora

char buff_filename[6]="bufor";

#define SEM_PRODUCENT 0   // numer semafora producenta
#define SEM_KONSUMENT 1   // numer semafora konsumenta

void sem_p(int sem_id, int sem_num);    // opuszczanie semafora
void sem_v(int sem_id, int sem_num);    // podnoszenie semafora
void sem_rem(int sem_id);               // usuwanie zbioru semaforow


//***********MAIN**************
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("Program wywolany nieprawidlowo. Podaj:\n$ ./%s <plik_wyjscia>\n", argv[0]);
        exit(-1);
    }
 
    FILE *out;
    out = fopen(argv[1], "w+");
    if(out == NULL)
    {
        perror("Blad otwierania pliku wyjsciowego!");
        exit(-1);
    }
  
    key_t key;
    int sem_id;
    
    key = ftok(".", 'Z');
    if(key == -1)
    {
        perror("Blad utworzenia klucza!");
        exit(-1);
    }
    
    // podlaczamy sie do zbioru semaforow
    sem_id = semget(key, 2, IPC_CREAT|0600);
    if(sem_id == -1)
    {
        perror("Blad przy tworzeniu zbioru semaforow!");
        exit(-1);
    }
    
    printf("Podlaczylem sie do zbioru semaforow %d\n", sem_id);
    
    
    FILE *fbuff;

    
    // sprawdzamy czy bufor istnieje
    fbuff = fopen(buff_filename, "r");
    if(fbuff == NULL)
    {
        perror("Nie udalo sie otworzyc pliku z buforem");
        printf("Prawdopodobnie uruchomiles konsumenta przed producentem zly czlowieku!\n");
        sem_rem(sem_id);
        exit(-2);
    }
    fclose(fbuff);
    
    printf("Bufor istnieje!\n");

    
    printf("ZACZYNAM KONSUMPCJE!\n\n");
    
    // to jest znak odczytywany z bufora
    char r;
    
    int i = 0;
    
    while(1)
    {
        printf("Czekamy na produkt...\n");
        
        // opuszczamy semafor konsumenta, by kolejny konsument nie mogl wejsc do sekcji 
        sem_p(sem_id, SEM_KONSUMENT);
        
        // czytamy z bufora
        r = buff_read();
       
        // dostalismy EOFa, czyli producent zakonczyl produkcje
        if(r == EOF)
        {
            printf("Nie ma wiecej produktow!\n");
            break;
        }
    
        i++;
        
        printf("Odczytalem: %c\n", r);
       
        // wpisujemydo pliku wyjsciowego
        fputc(r, out);
        
        // podnosimy semafor producenta aby mogl produkowac dalej
        sem_v(sem_id, SEM_PRODUCENT);
    }
    printf("KONIEC KONSUMPCJI! \t Odczytalem: %d !\n\n", i);
    
    
     // podnosimy semafor producenta, ktory czeka juz po zakonczeniu produkcji na zebranie wszystkiego przez konsumenta
     
    sem_v(sem_id, SEM_PRODUCENT);
    
    fclose(out);

    exit(0);
}

//******************************definicje funkcji**************************
//funkcja odczytujaca z bufora
int buff_read()
{
    int out;
    
    FILE *fbuff;
    fbuff = fopen(buff_filename, "r");
    if(fbuff == NULL)
    {
        return EOF;
    }
    
    out = fgetc(fbuff);
    fclose(fbuff);
    return out;
}


//funkcja opuszczajaca semafor
void sem_p(int sem_id, int sem_num)
{
    struct sembuf buf;
    int semop_ret;
    
    buf.sem_num = sem_num;
    buf.sem_op = -1;
    buf.sem_flg = 0;
    
    while((semop_ret = semop(sem_id, &buf, 1)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Blad opuszczania semafora!");
        exit(EXIT_FAILURE);
    }
}

//funkcja podnoszaca semafor
void sem_v(int sem_id, int sem_num)
{
    struct sembuf buf;
    int semop_ret;
    
    buf.sem_num = sem_num;
    buf.sem_op = 1;
    buf.sem_flg = 0;
    
    while((semop_ret = semop(sem_id, &buf, 1)) == -1 && errno == EINTR);
    
    if(semop_ret == -1)
    {
        perror("Blad podnoszenia semafora!");
        exit(EXIT_FAILURE);
    }
}

//funkcja usuwajaca zbior semaforow
void sem_rem(int sem_id)
{
    if(semctl(sem_id, 0, IPC_RMID) == -1)
    {
        perror("Blad usuwania zbioru semaforow");
        exit(EXIT_FAILURE);
    }
    printf("Usunieto zbior semaforow %d\n", sem_id);
}

