#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <sys/shm.h>
#include"semafor.c"



int main(int argc,char *argv[])
{
	
	char znak;
	char *adres;
	int id_segmentu;


/******************** TWORZENIE KLUCZA ***********************/
	key_t key_seg = ftok(".", 'I');


	FILE *wy;

/******************** PAMIEC DZIELONA ***********************/

	if((id_segmentu = shmget(key_seg, 0, 0600|IPC_CREAT)) == -1)
	{
		printf("BLAD podczas uzyskania dostepu do pamieci dzielonej (KONSUMENT)\n");
		return -1;
	}
	else
	{
		printf("KONSUMENT: uzyskano dostep do segmantu pamieci dzielonej\n");
	}
	
	
	if((adres = (char *) shmat(id_segmentu, 0, 0)) == -1)
	{
		printf("BLAD dolaczania pamieci dzielonej (KONSUMENT) \n");
		return -1;
	}
	else
	{
		printf("KONSUMENT: uzyskano adres segmantu pamieci dzielonej\n");
	}
	

/******************** UZYSKANIE DOSTEPU DO SEMAFORA ***********************/

	uzyskanie_dostepu();
	

/******************** PLIK ***********************/

	wy = fopen(argv[1],"w");
	

	int t;
	
	if(wy != NULL)
	{
		while(znak != EOF)
		{
						
			printf("KON.: ");
			opusc_semafor(1);		//Opuszczenie semafora 

			znak=*adres;

			if(znak != EOF)
			{
				fputc(znak,wy);
				printf("KONSUMENT: odczytano '%c'\n",znak);
				
				printf("KON.: ");
				podnies_semafor(0);	//Podniesienie semafora
			}
				
				
		}
	}
	else
	{
		printf("KONSUMENT: Blad otwarcia pliku %s do zapisu lub plik zrodlowy pusty!\n", argv[1]);
		shmdt(adres);
		shmctl(id_segmentu, IPC_RMID, 0);
		usun_semafor();
		printf("KONSUMENT: koniec dzialania\n");
		return 0;	
	}
	

	fclose(wy);
	

/******************** ODLACZENIE PAMIECI ***********************/

	shmdt(adres);
	
/******************** USUNIECIE SEGM. PAMIECI ***********************/

	shmctl(id_segmentu, IPC_RMID, 0);

/******************** USUNIECIE SEMAFORA ***********************/
	
	usun_semafor();
	
	printf("KONSUMENT: koniec dzialania\n");
	
	return 0;
}
