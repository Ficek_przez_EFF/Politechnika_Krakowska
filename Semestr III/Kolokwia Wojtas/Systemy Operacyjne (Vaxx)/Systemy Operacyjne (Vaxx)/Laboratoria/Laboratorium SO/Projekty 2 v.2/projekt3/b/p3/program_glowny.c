#include "headers.h"
#include "sem.c"
#define MAX_Liczba_procesow_potomnych 266 // ulimit -u LPP

//ponieważ ta unia jest zdefiniowana tylko na niektórych systemach.
/*#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
#else
union semun
{
        int val;                        //value for SETVAL
        struct semid_ds *buf;        //buffer for IPC_STAT, IPC_SET
        unsigned short int *array;        //array for GETALL, SETALL
        struct seminfo *__buf;        //buffer for IPC_INFO
};
#endif
*/
int main(int argc, char *argv[]){
	int i,liczba_pp,liczba_sk,exec,str_long,waitpid,x;
    char *nazwa;
	poczatek();
	
	//wciecie = sprawdzanie poprawnosci wywolania programu
	//ustawienie zmiennych liczba_pp, liczba_sk, nazwa
		if(argc != 4){
			printf("Zla liczba argumentow\nwywolaj program_glowny program_potomny liczba_pp liczba_sk\n");
			exit(1);
		}
		liczba_pp = atoi(argv[2]);
		liczba_sk = atoi(argv[3]);
		if(liczba_pp > MAX_Liczba_procesow_potomnych){
			printf("Zla liczba procesow potomnych\nsprawdz maksymalna ilosc ulimit -u\n");
			exit(1);
		}
		str_long = strlen(argv[1])+1;
		nazwa=(char *)malloc(str_long*sizeof(char));
		if (nazwa == NULL){
			printf("Blad funkcji malloc progrm_glowny");
			exit(2);
		}
		sprintf(nazwa,"./%s",argv[1]);
	
	//wypisanie danych z wywolania dla sprawdenia poprawnosci	
	printf("liczba_pp: %d \tliczba_sk: %d\tnazwa potomnego: %s\n",liczba_pp,liczba_sk,nazwa); 
	
    utworz_nowy_semafor();
    ustaw_semafor();
  
    //tworzenie liczba_pp procesow (generalnie lekko zmodyfikowany projekt1_3.c)
    for (i=1; i <= liczba_pp; i++){
    	switch(fork()) {
		case -1:
			perror("Nie utworzono procesu potomnego\n");
			exit(3);
      	case 0:
			exec = execl(nazwa,argv[1],argv[2],NULL);
			if(exec == -1){
         		perror ("Blad exec'a dla potomnego program_glowny\n");
         		exit(4);  
			}
        }
    }

    for(i=1; i <= liczba_pp; i++){
      waitpid = wait(&x);
      if (waitpid == -1){
        perror("Blad funkcji wait\n");
        exit(5);
      }
      printf("Proces %d zakonczyl sie z kodem %d\n", waitpid, x);
    }
    usun_semafor();
    free(nazwa);
	return 0;
}