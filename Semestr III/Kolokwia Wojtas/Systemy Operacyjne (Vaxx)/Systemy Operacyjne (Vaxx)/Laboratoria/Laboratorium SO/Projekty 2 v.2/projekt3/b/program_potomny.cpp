#include "headers.h"
#include "sem.c"
int main(int argc, char *argv[]){
  int i,liczba_sk;

  liczba_sk=atoi(argv[1]);
  uzyskanie_dostepu();

  sleep(1);

  for(i=1;i<=liczba_sk;i++){ 
  	semafor_p(); //zmiana stanu semafora na 0 -> uniemozliwienie innym procesom dostepu do sk
  	printf("\nSekcja krytyczna procesu %d \n",getpid());
	stan_semafora(); //wypisanie stanu semafora 1 - otwarty dostep czy 0 - zamkniety dostep	
  	sleep(2);
	liczba_prcesow_czekajacych(); //wypisanie liczby procesow czekajacych na otwarcie semafora
  	semafor_v(); //zmiana stanu semafora na 1
	stan_semafora();
  }
  return 0; 
}