#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>

#include <errno.h>
#include "semafory.h"

#include <signal.h>

Semafory_IPC s;

void excatch(int sig) {
	printf("Usuwam zbior semaforow");
	if(sig)
		s.remove();
	for(int x=0; x<5; x++)
	{
		int status = 0;
		int pid=0;
		if((pid=wait(&status)) == -1)
			perror("Child process failed on exit!");
		printf("Kod powrotu %d PID(%d): %d\n",x,pid, status);
	}
	if(!sig)
	{
		s.remove();
		exit(0);
	}
	
	signal(SIGINT, SIG_DFL);
}

	
int main(int argc, char *argv[]) {
	(void) signal(SIGINT, excatch);
	if(argc != 2)
	{
		printf("Usage: %s tryb\n", argv[0]);
		exit(1);
	}

	int x;
	int tryb=atoi(argv[1]);
	printf("Program do uruchomienia: filozofowie\nTryb:%d\n\n", tryb);

	key_t key;

	key = ftok(".", 6);
	if( tryb == 1 || tryb == 2 )
	{
		if(s.create(key, 5) == -1)
			exit(4);
	}else if( tryb == 3 ) {
		if(s.create(key, 6) == -1)
			exit(4);
		s.init(4, SETVAL, 5); // LOKAJ!!!
	}else
		exit(3);
	int a;
	for(a=0; a<5; a++)
		s.init(1, SETVAL, a);
	printf("Lokaj val: %d\n", s.getval(5));

	char buf1[5], buf2[5];
	sprintf(buf1, "%d", tryb);
	for(x=0; x<5; x++)
	{
		switch(fork())
		{
			case -1:
				perror("fork(): error");
				exit(2);
			case 0:
				sprintf(buf2, "%d", x);
				execl("./filozof", "./filozof", (const char*)&buf2, (const char*)&buf1, NULL);
				perror("Exec failed\n");
				exit(3);
//			default:
//				sprintf(buf1, "Jestem macierzysty(%d)", 0);
//				introduce_myself(buf1);

				//Proces macierzysty
		}
//		printf("\n");	
//		sprintf(buf1, "pstree -p %d", getpid());
//		system(buf1);

	}
	
	
// Usuwanie semafora po wszystkim
	excatch(0);
	return 0;	
}

