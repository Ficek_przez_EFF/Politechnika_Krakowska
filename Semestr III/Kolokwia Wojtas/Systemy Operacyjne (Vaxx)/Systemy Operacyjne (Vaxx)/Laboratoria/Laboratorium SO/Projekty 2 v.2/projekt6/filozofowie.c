/*    ROZWIAZANIE NR 1 - ZAKLESZCZENIE/BLOKADA
 *    ROZWIAZANIE NR 2 - ZAGLODZENIE
 *   ROZWIAZANIE NR 3 - Z LOKAJEM
 */
#include "sem.c"

int main(int argc, char *argv[]){
	if(argc != 3){
		printf("Podano zla liczbe argumentow\n");
		return -1;
	}
	int i,j;
	int filozof = atoi(argv[1]);
	int rozwiazanie = atoi(argv[2]);
	key_t key = ftok(".",'Z');
	
	if(rozwiazanie == 3)
		uzyskanie_dostepu(6);
	else
		uzyskanie_dostepu(5);
		
	
	switch(rozwiazanie){
		case 1:
		for(j = 0; j <= MAX; j++){
				printf("filozof nr %d pid %d mysli\n",filozof,getpid());
				//sleep(1);
				printf("filozof nr %d pid %d skonczyl myslec i chce jesc\n",filozof,getpid());
				semafor_p(filozof);
				semafor_p((filozof+1)%5);	
				printf("filozof nr %d pid %d je \n",filozof,getpid());
				//sleep(1);
				printf("filozof nr %d pid %d skonczyl jesc\n",filozof,getpid());
				semafor_v(filozof);
				semafor_v((filozof+1)%5);
			}
			break;
		case 2:
			for(j = 0; j <= MAX; j++){
				printf("filozof nr %d pid %d mysli\n",filozof,getpid());
				//sleep(1);
				printf("filozof nr %d pid %d skonczyl myslec i chce jesc\n",filozof,getpid());
				semafor_p2(filozof);
				printf("filozof nr %d pid %d je \n",filozof,getpid());
				if(filozof == 2)
					sleep(2);
				semafor_v2(filozof);
			}
		case 3:
			for(j = 0; j <= MAX; j++){
				printf("filozof nr %d pid %d mysli\n",filozof,getpid());
				sleep(1);
				printf("filozof nr %d pid %d skonczyl myslec i chce jesc\n",filozof,getpid());
				semafor_p(5);//LOKAJ
				semafor_p(filozof);
				semafor_p((filozof+1)%5);
				printf("filozof nr %d pid %d je \n",filozof,getpid());
				sleep(1);
				printf("filozof nr %d pid %d skonczyl jesc\n",filozof,getpid());
				semafor_v(filozof);
				semafor_v((filozof+1)%5);
				semafor_v(5);//LOKAJ
			}
	}
	
	return 0;
}