#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#include <unistd.h>

#include <cstdio>
#include <cstdlib>

#include <cerrno>

#define SHMKEY_P "."
#define SHMKEY_N 15

#define SEMKEY_P "."
#define SEMKEY_N 14

#define DEFAULT_FLAG SEM_UNDO

union semun {
	int val;                /* wartość dla SETVAL */
	struct semid_ds *buf;   /* bufor dla IPC_STAT i IPC_SET */
	short *array;          /* tablica dla GETALL i SETALL */
};

class Semafory_IPC {
	key_t ckey;
	int cile, cchmod;
	int cremove;
	int semid;
	pid_t pid;
public:
	Semafory_IPC(){
		cremove = 1;
		ckey = -1;
		cile = 0;
		cchmod = 0;
		semid = -1;
		pid = getpid();
	};	

	virtual ~Semafory_IPC(){};
	int create( int key=12345, int ile=1, int chmod=0600 );
	void init( int liczba=1, int flg = SETVAL , int nrsem = 0);
	void access( int key=12345, int ile=1, int chmod=0600 );
	void signal( int numer=0, int flg=DEFAULT_FLAG, int liczba = 1 );
	void signal2( int numer=0, int flg=DEFAULT_FLAG, int liczba = 1 );
	void wait( int numer=0, int flg=DEFAULT_FLAG, int liczba = 1 );
	void wait2( int numer=0, int flg=DEFAULT_FLAG, int liczba = 1 );
	void remove();
	int getval(int num);
	void setval(int num);
};

int Semafory_IPC::create( int key, int ile, int chmod )
{
	ckey = key;
	cile = ile;
	cchmod = chmod;
	semid =	semget(ckey, cile, cchmod|IPC_CREAT|IPC_EXCL);

	if(semid != -1)
	{
		printf("PID: %d\tUtworzylem zbior semaforow (%d) o ID: %d\n",pid , cile, semid);
		return 0;
	}else if(errno == EEXIST){
		semid = semget(ckey, cile, cchmod);
		if(semid == -1)
		{
			perror("Blad semget");
			exit(1);
		}
		printf("PID: %d\tZbior semaforow istnieje! Aktualizuje wartosc.\n",pid);
		return -1;
	}else{
		perror("Blad tworzenia semaforow");
		exit(2);
	}

}

void Semafory_IPC::access( int key, int ile, int chmod ) {
	if(ckey == -1)
		ckey = key;
	if(!cile)
		cile = ile;
	if(!cchmod)
		cchmod = chmod;

	semid = semget(ckey,  cile, cchmod);

	if(semid == -1)
	{
		perror("Blad podlaczania semafora");
		exit(8);
	}	
}

void Semafory_IPC::init( int liczba, int flg , int nrsem ) {
	if(semctl(semid, nrsem, flg, liczba) == -1 )
	{
		perror("Blad inicjacji semafora");
		exit(3);
	}
}

void Semafory_IPC::remove() {
	if(cremove)
	{
		cremove = 0;
		printf("Usuwam zbior semaforow\n");
		if(semctl(semid, 0, IPC_RMID) == -1 )
		{
			perror("Blad przy usuwaniu semaforow");
			exit(4);
		}
	}else{
		printf("PID: %d\tSemafor juz usuniety!\n",pid);
	}
}

int Semafory_IPC::getval(int num) {
	int ret = semctl(semid, num, GETVAL);
	if(ret == -1)
		perror("GETVAL ERROR");
	return ret;
}

void Semafory_IPC::setval(int num) {
	union semun argg;
	argg.val = num;

	int ret = semctl(semid, num, SETVAL, argg);
	if(ret == -1)
		perror("SETVAL ERROR");
}

void Semafory_IPC::signal( int numer, int flg, int liczba ) {

	struct sembuf op;
       	
	op.sem_num = numer;
	op.sem_op = liczba;
	op.sem_flg = flg;

	if(semop(semid, &op, 1) == -1)
	{
		printf("errno: %d", errno);
		perror("semop");
		if(errno == ERANGE)
			exit(1);
	
		if(errno == EINTR)
		{
			if(semop(semid, &op,1) == -1)
			{
				perror("semop x2");
				exit(5); // Dla filozofow nie regenerujemy semaforow
				create(ckey,cile,cchmod);
				if(semop(semid, &op, 1) == -1)
				{
					perror("semop x3");	
					exit(5);
				}
			}
		}else{
			create(ckey, cile, cchmod);
			signal( flg, liczba );
		}
	}

}

void Semafory_IPC::signal2( int numer, int flg, int liczba ) {

	struct sembuf op[2];
       	
	op[0].sem_num = numer;
	op[0].sem_op = liczba;
	op[0].sem_flg = flg;
	
	op[1].sem_num = (numer+1)%5;
	op[1].sem_op = liczba;
	op[1].sem_flg = flg;


	if(semop(semid, op, 2) == -1)
	{
		printf("errno: %d", errno);
		perror("semop");
		if(errno == ERANGE)
			exit(1);

		if(errno == EINTR)
		{
			if(semop(semid, op,2) == -1)
			{
				perror("semop x2");
				exit(5); // Dla filozofow nie regenerujemy semaforow
				create(ckey,cile,cchmod);
				if(semop(semid, op, 2) == -1)
				{
					perror("semop x3");	
					exit(5);
				}
			}
		}else{
			create(ckey, cile, cchmod);
			signal( flg, liczba );
		}
	}

}

void Semafory_IPC::wait( int numer, int flg, int liczba ) {

	struct sembuf op;
       	
	op.sem_num = numer;
	op.sem_op = -liczba;
	op.sem_flg = flg;

	if(semop(semid, &op, 1) == -1)
	{
		printf("errno: %d", errno);
		perror("semop");
		if(errno == ERANGE)
			exit(1);
		if(errno == EINTR)
		{
			if(semop(semid, &op,1) == -1)
			{
				perror("semop x2");
				exit(5); // Dla filozofow nie regenerujemy semaforow
				create(ckey,cile,cchmod);
				if(semop(semid, &op, 1) == -1)
				{
					perror("semop x3");	
					exit(5);
				}
			}
		}else{
			create(ckey, cile, cchmod);
			wait( flg, liczba );
		}
	}

}

void Semafory_IPC::wait2( int numer, int flg, int liczba ) {

	struct sembuf op[2];
       	
	op[0].sem_num = numer;
	op[0].sem_op = liczba;
	op[0].sem_flg = flg;
	
	op[1].sem_num = (numer+1)%5;
	op[1].sem_op = liczba;
	op[1].sem_flg = flg;
	
	if(semop(semid, op, 2) == -1)
	{
		printf("errno: %d", errno);
		perror("semop");
		if(errno == ERANGE)
			exit(1);

		if(errno == EINTR)
		{
			if(semop(semid, op,2) == -1)
			{
				perror("semop x2");
				exit(5); // Dla filozofow nie regenerujemy semaforow
				create(ckey,cile,cchmod);
				if(semop(semid, op, 2) == -1)
				{
					perror("semop x3");	
					exit(5);
				}
			}
		}else{
			create(ckey, cile, cchmod);
			wait( flg, liczba );
		}
	}
}


