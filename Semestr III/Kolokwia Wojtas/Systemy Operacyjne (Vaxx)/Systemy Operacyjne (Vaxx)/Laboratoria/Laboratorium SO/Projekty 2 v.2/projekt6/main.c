#include "sem.c"

void sig_hand1(int sig_n){
		usun_zbior_5_semaforow();
}
void sig_hand2(int sig_n){
		usun_zbior_6_semaforow();
}
int main(int argc, char *argv[]){
	if(argc != 2)
	{
		printf("Podano zla liczbe argumentow\n");
		exit(1);
	}
	int i,j, *x, waitpid, exec,ile=5;
	int rozwiazanie = atoi(argv[1]);
	char ktory[1],rozw[1];
	/*printf("Podaj ktore rozwiazanie 1 - ZAKLESZCZENIE 2 - 3 - \n");
	scanf("%c",&rozw);//iazanie);*/
	if(rozwiazanie ==3)
		signal(SIGINT,sig_hand2);
	else
		signal(SIGINT,sig_hand1);
	if(rozwiazanie == 3){
		ile = 6;
		utworz_zbior_6_semaforow();
	}
	else
		utworz_zbior_5_semaforow();
		
	
	for(i = 0; i<ile; i++){
		ustaw_semafor(i);
	}
	for(i=0; i<5; i++) {
			sprintf(ktory,"%d",i);
        	switch(fork()) {
			case -1:
				perror("Nie utworzono procesu potomnego");
        		break;
			case 0:
			printf("MAIN : %s %s\n",ktory,rozw);
				exec = execl("./filozofowie", "filozofowie",ktory,argv[1],NULL);
				if(exec == -1) {
					perror("Nie udało się wykonać funkcji execl");
				}
                	break;
        	}
	}
	
	for(j=0;j<ile;j++) {
 		waitpid = wait(&x);
 		printf("Proces %d zakonczyl sie z kodem %d\n", waitpid, x);	
	}
	if(rozwiazanie ==3)
		usun_zbior_6_semaforow();
	else	
   		usun_zbior_5_semaforow();
	return 0;
}