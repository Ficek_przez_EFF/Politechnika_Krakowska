//Rozwiazanie z mozliwoscia zakleszczenia
// Podzial poprzez ify
//#define TRYB 1 // Zakleszczenie
//#define TRYB 2 // Zaglodzenie
//#define TRYB 3 // Lokaj
//
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "semafory.h"

const int kmax_zapetlenie=3000;

int main(int argc, char *argv[]) {
	if(argc != 3)
	{
		printf("Nie podano argumentow!\n %s nr_filozofa tryb_pracy\n", argv[0]);
		exit(2);
	}
	
	int nfil,tryb;
	nfil = atoi(argv[1]);
	tryb = atoi(argv[2]);
	pid_t pid = getpid();
	key_t key;
	Semafory_IPC s;
	
	key = ftok(".", 6);

	s.access(key);
	
switch(tryb) {
// Rozwiazanie z mozliwoscia zakleszczenia
case 1:
	for(int i=0; i<kmax_zapetlenie; i++) {
	//Myslenie
		printf("Jestem profesor nr: %d, z pidem: %d i mysle!\n", nfil, pid);
//		sleep(1);
	
	//Jedzenie:
		printf("Jestem profesor nr: %d, z pidem: %d i probuje dobrac sie do jedzenia!\n", nfil, pid);
		s.wait(nfil);
		s.wait((nfil+1)%5);
	
	//Jedzenie wlasciwe]
		printf("Jestem profesor nr: %d, z pidem: %d i jem!\n", nfil, pid);
	//		usleep(10);
		printf("Jestem profesor nr: %d, z pidem: %d i koncze jesc!: (%d)\n", nfil, pid,i);
	
		s.signal(nfil);
		s.signal((nfil+1)%5);
	}
	break;


// Rozwiazanie asymetryczne ( z zaglodzeniem )
case 2:

	for(int i=0; i<kmax_zapetlenie; i++) {
		//printf("Jestem profesor nr: %d, z pidem: %d i mysle!\n", nfil, pid);
	//	sleep(1);
	
	//Jedzenie:
		printf("Jestem profesor nr: %d, z pidem: %d i probuje dobrac sie do jedzenia!\n", nfil, pid);
		/*if(nfil%2) {
			s.wait(nfil);
			s.wait((nfil+1)%5);
		}else{
			s.wait((nfil+1)%5);
			s.wait(nfil);
		}*/
		s.wait2(nfil,0);
		if(nfil == 2) printf("---------------------------\n");
		printf("Jestem profesor nr: %d, z pidem: %d i jem! (%d)\n", nfil, pid, i);

	
	//Jedzenie wlasciwe
	//	usleep(100);
		//if(nfil == 3 || nfil == 1)
		if(nfil == 2)
			sleep(2);
		printf("Jestem profesor nr: %d, z pidem: %d i koncze jesc!\n", nfil, pid);
	
		s.signal2(nfil,0);
		//s.signal((nfil+1)%5);
	
	}
	break;
	
// Rozwiazanie z lokajem	
case 3:
	for(int i=0; i<kmax_zapetlenie; i++) {
	/* Wymagany zbior zlozony z 5 semaforow, wartosci poczatkowe:
0: 1
1: 1
2: 1
3: 1
4: 1
5: 4
*/
	//Myslenie
		printf("Jestem profesor nr: %d, z pidem: %d i mysle!\n", nfil, pid);
		sleep(1);
	//Jedzenie;
		printf("Jestem profesor nr: %d, z pidem: %d i probuje dobrac sie do jedzenia!\n", nfil, pid);
		s.wait(5); // Lokaj
		s.wait(nfil);
		s.wait((nfil+1)%5);
	// Mam widelce moge jesc!
		printf("Jestem profesor nr: %d, z pidem: %d i jem! (%d)\n", nfil, pid,i);
			sleep(1);
		printf("Jestem profesor nr: %d, z pidem: %d i koncze jesc!\n", nfil, pid);
		// Troche to nie higieniczne ale oddaje i daje szanse innym
		s.signal(nfil);
		s.signal((nfil+1)%5);
		s.signal(5);
	
	}
	break;
	default:
		printf("Nie ma takiego trybu, 1 2 lub 3\n");
}

}

