#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>


int main() {
	int i, mainpid;
	char cmd[20];

	mainpid = getpid();
	sprintf(cmd, "pstree -ph %d", mainpid);

	printf("Proces macierzysty\nUID: %d\tGID: %d\tPID: %d\tPPID: %d\n\n",getuid(),getgid(),getpid(),getppid());

 	for(i=1; i<=3; i++) {
        	switch(fork()) {
         		case -1:
                		perror("Nie utworzono procesu potomnego");
         		break;

         		case 0:
                		printf("Fork %d - UID: %d\tGID: %d\tPID: %d\tPPID: %d\n",i,getuid(),getgid(),getpid(),getppid());	
                	break;

         		default:
 				printf("macierzysty %d\n", i);
        	//fork(); //dopisane przez JW z pytaniem, ile procesow powstanie
        	}

  		system(cmd);
		//sleep(1);
	}

	return 0;

}

/*

|-5719
  | | |
  | | |-5720
  | |   |  |-5723
  | |   |      |-5726
  | |   |
  | |   |-5724
  | |
  | |-5721
  |     |-5725
  |
  |-5722

*/
