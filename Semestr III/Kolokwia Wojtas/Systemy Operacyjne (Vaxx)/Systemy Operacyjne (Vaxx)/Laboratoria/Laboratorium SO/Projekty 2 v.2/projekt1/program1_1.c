#include <stdio.h>
#include <stdlib.h>

int main() {

	printf("PID: %d\t",getpid()); //identyfikator procesu
	printf("PPID: %d\t",getppid()); //identyfikator procesu macierzystego
	printf("UID: %d\t",getuid()); //identyfikator uzytkownika
	printf("GID: %d\n",getgid()); //identyfikator grupy
	printf("PGID: %d\n",getpgid());  //JW pytal sie o ta funkcje

	return 0;

}
