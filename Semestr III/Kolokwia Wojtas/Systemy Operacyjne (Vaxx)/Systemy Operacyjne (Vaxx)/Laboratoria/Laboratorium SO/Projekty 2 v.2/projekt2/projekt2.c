#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

int s1,s2,tab[2][10];

void *r1(){
  int i=0;
  s1=0;
  for(i;i<10;i++){
	 s1+=tab[0][i];
	 printf("sumowany %d suma %d\n",i+1,s1);
	 sleep(1);
	}
  printf("id_watku=%u s1: %d\n",pthread_self(),s1);
  pthread_exit((void *)5);
}

void *r2(){
  int i=0;
  s1=0;
  for(i;i<10;i++){
    s2+=tab[1][i];
	printf("sumowany %d suma %d\n",i+1,s2);
	sleep(1);
	}
  printf("id_watku=%u s2: %d\n",pthread_self(),s2);
  pthread_exit((void *)0);
}

int main(){
  int i,j;
  pthread_t id1,id2;
  int *kod_powrotu;
  srand(time(NULL));
  
  for(i=0;i<2;i++)
    for(j=0;j<10;j++)
      tab[i][j] = rand()%20;
    
  if(pthread_create(&id1,NULL,r1,NULL)==-1){
    perror("Blad phread_create linia 44");
    exit(1);
  }
 
  if(pthread_create(&id2,NULL,r2,NULL)==-1){
    perror("Blad phread_create linia 49");
    exit(1);
  }
  
  if(pthread_join(id1,(void *)&kod_powrotu)==-1){
    perror("Blad phread_join linia 54");
    exit(2);
  }
  
  if(pthread_join(id2,NULL)==-1){
    perror("Blad phread_join linia 59");
    exit(2);
  }
  
  printf("s1+s2=%d\n",s1+s2);
  //printf("kod powrotu pierwszego: %u\n",(int)kod_powrotu);
  return 0;
}

//gcc -o projekt2 projekt2.c -lpthread
