#include "header.h"
char temp[15];
int wezpid(char text[]);
void sig_hand(int sig_n);
int main(){
	int IDkolejki,i,pid,msize;
	key_t key = ftok(".",21);
	komunikat kom;
	if((IDkolejki = msgget(key,IPC_CREAT|0660)) == -1){
		printf("BLAD msgget: %i (%s)\n",errno,strerror(errno));
		return -1;
	}
	signal(SIGINT,sig_hand);
	printf("^C konczy prace serwera \n\n");
	while(1){
		printf("Serwer czekam na komunikat...\n");
		kom.mtype = 1;
		if((msgrcv(IDkolejki, (struct msgbuf *)&kom,MAX,kom.mtype,0)) == -1){
			printf("BLAD msgrcv: %i (%s)\n",errno,strerror(errno));
			return -1;	
		}
		pid = wezpid(kom.mtext);
		kom.mtype = pid;
		printf("Serwer Odebrano od: %d\n",pid);
		
		msize = strlen(kom.mtext);
		for(i=0;i<msize;i++){
			kom.mtext[i] = toupper(kom.mtext[i]);
		}
		//wysylanie
		printf("Serwer Wysylanie... %s -> %ld\n",kom.mtext,kom.mtype);
		if((msgsnd(IDkolejki,(struct msgbuf *)&kom,strlen(kom.mtext)+1,0))==-1){
			printf("BLAD msgsnd: %i (%s)\n",errno,strerror(errno));
			return -1;
		}
	}
	return 0;
}

int wezpid(char text[MAX]){
	int i,pid,len,oldi;
	len = strlen(text);
	for(i=0;i<12;i++){
		temp[i] = text[i];
		if(temp[i]=='~'){
			temp[i+1] = '\n';
			break;
		}
	}
	for(i=0;text[i] !='~';i++)
	oldi = i+1;
	for(i=0;i<len-oldi;i++){
		text[i] = text[i+1+oldi];
	}
	pid = atoi(temp);
	return pid;
}

void sig_hand(int sig_n){
	key_t key = ftok(".",21);
	int IDkolejki;
	if((sig_n==SIGTERM || sig_n==SIGINT)){
		printf("Koniec pracy serwera...\n");
		if((IDkolejki = msgget(key,IPC_CREAT|0660)) == -1){
			printf("BLAD msgget: %i (%s)\n",errno,strerror(errno));
			exit(-1);
		}
	if((msgctl(IDkolejki, IPC_RMID,0)) == -1){
			printf("BLAD msgget: %i (%s)\n",errno,strerror(errno));
			exit(-1);
		}
		exit(0);
	}
}