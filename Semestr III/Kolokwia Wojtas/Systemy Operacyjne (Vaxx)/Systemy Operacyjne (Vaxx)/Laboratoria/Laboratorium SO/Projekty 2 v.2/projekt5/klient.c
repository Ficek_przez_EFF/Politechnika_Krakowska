#include "header.h"
int main(){
	int IDkolejki,tem,i;
	char tmptxt[10],wiadomosc[MAX];
	key_t key = ftok(".",21);
	komunikat kom;
	if((IDkolejki = msgget(key,IPC_CREAT|0660)) ==-1 ){
		printf("BLAD msgget: %i (%s)\n",errno,strerror(errno));
		return -1;
	}
	i = 0;
	while(1){
		kom.mtype = 1;
		sprintf(tmptxt,"%d~",getpid());
		strcpy(kom.mtext,tmptxt);
		printf("Klient[%d] Podaj tekst do wyslania: ",getpid());
		i = 0;
		while(1){
			wiadomosc[i] = getchar();
			if((wiadomosc[i] == '\n')||(i>=MAX-20)){
				wiadomosc[i] = '\0';
				break;
			}
			i++;
		}
		strcat(kom.mtext,wiadomosc);
		
		printf("Klient[%d] Wysylanie... \"%s\" -> SERWER\n",getpid(),&kom.mtext[strlen(kom.mtext) - strlen(wiadomosc)]);
		if((msgsnd(IDkolejki, (struct msgbuf *)&kom,strlen(kom.mtext)+1,0)) == -1){
			printf("BLAD msgsnd: %i (%s)\n",errno,strerror(errno));
			return -1;	
		}
		kom.mtype = getpid();
		if((msgrcv(IDkolejki,(struct msgbuf *)&kom,MAX,kom.mtype,0)) == -1){
			printf("BLAD msgrcv: %i (%s)\n",errno,strerror(errno));
			return -1;	
		}
		printf("Klient[%d] Odebrano: \"%s\" zaadresowane do %ld\n",getpid(),kom.mtext,kom.mtype);
	}
	return 0;
}