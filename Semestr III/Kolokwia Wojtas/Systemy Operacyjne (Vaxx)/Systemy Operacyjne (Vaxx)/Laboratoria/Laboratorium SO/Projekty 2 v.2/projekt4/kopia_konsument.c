
#include"header.h"
#include"sem.c"
int main(int argc,char *argv[]){

char znak;
char *adres_pamieci_wspolnej;
int id_segmentu;
key_t key_seg = ftok(".",'S');
FILE *wy;
//uzyskanie dostepu do segmantu pamieci dzielonej
if((id_segmentu = shmget(key_seg,0,0600)) == -1){
    printf("BLAD shmget: %i (%s)\n",errno,strerror(errno));
    return -1;
}
else{
    printf("KONSUMENT: uzyskano dostep do segmantu pamieci wspolnej\n");
}
if((adres_pamieci_wspolnej = (char *) shmat(id_segmentu,0,0)) == -1){
    printf("BLAD shmat : %i (%s)\n",errno,strerror(errno));
    return -1;
}
else{
    printf("KONSUMENT: uzyskano adres segmantu pamieci wspolnej\n");
}
//uzyskanie dostepu do zbioru semaforow
uzyskanie_dostepu();
//utworzenie uchwytu pliku do pisanie
wy = fopen(argv[1],"w");
//petla do momentu kiedy nie zostanie wczytany EOF (do ... while)
if(wy != NULL){
	do{
	//sleep(1);
	semafor_p(1);
	znak=*adres_pamieci_wspolnej;
	//printf("%c %c",znak,*adres_pamieci_wspolnej);
	if(znak != EOF){
	fputc(znak,wy);}
        semafor_v(0);
    }while(znak != EOF);	
fputc('\n',wy);  
}
else{
    printf("KONSUMENT: w pliku nie ma niczego lub sie nie otworzyl\n");
}
//zamkniecie pliku
fclose(wy);
//odlaczenie pamieci dzielonej
shmdt(adres_pamieci_wspolnej);
//usuniecie segmentu pamieci dzielonej
shmctl(id_segmentu,IPC_RMID,0);
//usuniecie zbioru semaforow
usun_semafor();
return 0;
}
