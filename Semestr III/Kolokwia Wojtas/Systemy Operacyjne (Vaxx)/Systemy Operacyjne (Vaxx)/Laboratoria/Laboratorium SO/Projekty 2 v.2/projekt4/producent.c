#include"header.h"
#include"sem.c"
int main(int argc,char *argv[]){

char znak;
char *adres_pamieci_wspolnej;
int id_segmentu;
key_t key_seg = ftok(".",'S');
FILE *we;
//utworzenie segmentu pamieci dzielonej
if((id_segmentu = shmget(key_seg,sizeof(char),IPC_CREAT|IPC_EXCL|0600)) == -1){
	printf("BLAD shmget: %i (%s)\n",errno,strerror(errno));
    return -1;
}
else{
	printf("PRODUCENT:  utworzone segmant pamieci wspolnej\n");
}

if((adres_pamieci_wspolnej = (char *) shmat(id_segmentu,0,0)) == -1){
    printf("BLAD shmat : %i (%s)\n",errno,strerror(errno));
    return -1;
}
else{
    printf("PRODUCENT: uzyskano adres segmantu pamieci wspolnej\n");
}
//utworzenie zbioru semaforow
utworz_nowy_semafor();
//ustawienie semaforow
ustaw_semafor(0,1);
ustaw_semafor(1,0);
//utworzenie uchwytu pliku do czytania
we = fopen(argv[1],"r");
//petla do momentu kiedy nie zostanie wczytany EOF (do ... while)
if(we != NULL){
		while((znak = fgetc(we)) != EOF){
		//opuszczenie semafora zapisu
		semafor_p(0);
		*adres_pamieci_wspolnej = znak;
		printf("%c %c",znak,*adres_pamieci_wspolnej);
		//podniesienie semafora odczytu
	    semafor_v(1);
	}
	*adres_pamieci_wspolnej = EOF;
	//podniesienie semafora odczytu
    semafor_v(1);
}
else{
    printf("PRODUCENT: w pliku nie ma niczego lub sie nie otworzyl\n");
}
//zamkniecie pliku
fclose(we);
//odlaczenie pamieci dzielonej
shmdt(adres_pamieci_wspolnej);
usun_semafor();
return 0;
}
