#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>

int semafor;
key_t klucz;

/************** OPUSC SEMAFOR *******************/

static void opusc_semafor()
{
 int status;
 struct sembuf bufor_semafora;

 bufor_semafora.sem_num = 0;			//numer semafora w zbiorze (numeracja od zera)
 bufor_semafora.sem_op = -1;			//Okresla co zrobic. Dla sem_op <0 operacja P - wstrzymuje 							//lub powoduje ,zmniejszenie semafora o sem_op
 bufor_semafora.sem_flg = SEM_UNDO;

 while(1)
 {
  status = semop(semafor, &bufor_semafora, 1);

  if(status == 0 || errno != 4)
   break;
 }

 if(status == -1 && errno != 4)
 {
  printf("Blad opuszczenia semafora\n");
  exit(1);
 }
 else
  printf("Opuszczenie semafora\n");
}

/************** PODNIES SEMAFOR *******************/

static void podnies_semafor()
{
 int status;
 struct sembuf bufor_semafora;

 bufor_semafora.sem_num = 0;			//numer semafora w zbiorze (numeracja od zera)
 bufor_semafora.sem_op = 1;			//Okresla co zrobic. Dla sem_op >0 operacja V - zwiekszenie 							//semafora o sem_op
 bufor_semafora.sem_flg = SEM_UNDO;

 while(1)
 {
  status = semop(semafor, &bufor_semafora, 1);

  if(status == 0 || errno != 4)
   break;
 }

 if(status == -1 && errno != 4)
 {
  printf("Blad podniesienia semafora\n");
  exit(1);
 }
 else
  printf("Podniesienie semafora\n");
}



/************** OCZEKUJACE PROCESY *******************/

static void oczekujace()
{
 int it;
 
 it = semctl(semafor, 0, GETNCNT);	//Odczytanie liczby procesow czekajacych na podniesienie sem.

 if(it == -1)
 {
  printf("Blad odczytu liczby procesow ktore czekaja na podniesienie semafora\n");
  exit(1);
 }
 else
  printf("Liczba procesow czekajaca na podniesienie semafora = %d \n", it);
}



int main(int argc, char *argv[])
{
 if(argc<2)
 {
  printf("Zla liczba argumentow\n");
  exit(1);
 }
 
 int liczba_skrytycznych = atoi(argv[1]);

 if(!liczba_skrytycznych || liczba_skrytycznych < 0)
  {
   printf("Argument musi byc liczba dodatnia!\n");
   exit(1);
  }
/************** TWORZENIE KLUCZA *******************/

 klucz = ftok("./projekt3", 'D');

 if(klucz == -1)
 {
  printf("Blad tworzenia klucza w programie potomnym \n");
  exit(1);
 }
  

/************** UZYSKANIE DOSTEPU DO SEMAFORA *******************/

 if( (semafor = semget(klucz, 1, 0600 | IPC_CREAT)) == -1)
 {
  printf("Blad uzyskania dostepu do semafora w programie potomnym \n");
  exit(1);
 }
 else
  printf("Proces %d uzyskal dostep do semafora\n", getpid());


/************** OPERACJE NA SEMAFORZE *******************/

 int i;
 
 for(i=0; i<liczba_skrytycznych; i++)
 {
  opusc_semafor();			//Zmienia sie stan semafora na 0.
  
  printf("Sekcja krytyczna procesu: %d \n", getpid());
  sleep(1);

  oczekujace();				//Liczba procesow oczekujacych.

  podnies_semafor();			//Zmienia stan semafora na 1.
 
 }

 return 0;

}









