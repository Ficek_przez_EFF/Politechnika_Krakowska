#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>

void opusc(int semid)
{
    struct sembuf buf;
    
    buf.sem_num = 0;
    buf.sem_op = -1;
    buf.sem_flg = SEM_UNDO;
    
    if(semop(semid, &buf, 1) == -1)
    {	if(errno==EINTR)
	{opusc(semid);}
	else
	{
        perror("Potomek: Blad przy opuszczaniu semafora!");
        exit(-2);
	}
    }
    else
    {
        printf("Potomek: Opuscilem semafor %d a moj PID: %d \n", semid, getpid());
    }
}
void podnies(int semid)
{
    struct sembuf buf;
    
    buf.sem_num = 0;
    buf.sem_op = 1;
    buf.sem_flg = SEM_UNDO;
    
    if(semop(semid, &buf, 1) == -1)
    {	if(errno==EINTR)
	{ podnies(semid);}
	else
	{
        perror("Potomek: Blad przy podnoszeniu semafora!");
        exit(-2);
	}
    }
    else
    {
        printf("Potomek: Podnioslem semafor %d a moj PID: %d \n", semid, getpid());
    }
}

int main(int argc, char* argv[])
{
long int i;
int semid;
key_t key;

if(argc!=2)
{printf("Potomek: Blad wywolania programu! \n");
printf("Podaj 2 argumenty wywolania: ./progr_potomny liczba_sekcji_krytycznych\n");
exit(1);
}

long int liczba_sk = atoi(argv[1]);
if((key=ftok(".",'Z'))==-1)
{
perror("Potomek: Blad utworzenia klucza!");
exit(2);
}
printf("Potomek: Utworzylem klucz \n");

if((semid=semget(key, 1, IPC_CREAT|0600))==-1)
{
perror("Potomek: Blad przy utworzeniu zbioru semaforów!");
exit(2);
}
printf("Potomek: Utworzylem zbior semaforow \n");

srand(time(0));
for(i=0;i<liczba_sk;i++)
{
printf("Potomek PID %d : Czekam na wejscie do sekcji krytycznej \n", getpid());

opusc(semid);
printf("Potomek PID %d : Jestem w sekcji krytycznej! ^___^ \n", getpid());
//sleep(rand()%7);
printf("Wartosc semafora: %d , ilosc czekajacych %d \n",semctl(semid,0,GETVAL),semctl(semid,0,GETNCNT));
podnies(semid);
printf("Potomek PID %d :Wyszedlem z sekcji krytycznej \n", getpid());
}
printf("Potomek PID %d : Konczy prace \n", getpid());
exit(0);
}
