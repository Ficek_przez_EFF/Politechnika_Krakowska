#include <stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/sem.h>
#include<unistd.h>

int main(int argc, char* argv[])
{
key_t key;
int semid, i, p, x;


if(argc!=4)
	{
	printf("Glowny: Blad wywolania programu! \n");
	printf("Podaj 4 argumenty wywolania: ./progr_glowny progr_potomny liczba_procesow_potomnych ilosc_wykonania_sekcji_krytycznej.\n");
	exit(1);
	}

int liczba_pp=atoi(argv[2]);

if((key=ftok(".",'Z'))==-1)
{
perror("Glowny: Blad utworzenia klucza!");
exit(2);
}
printf("Glowny: Utworzylem klucz \n");

if((semid=semget(key, 1, IPC_CREAT|IPC_EXCL|0600))==-1)
{
perror("Glowny: Blad przy utworzeniu zbioru semaforów!");
exit(2);
}
printf("Glowny: Utworzylem zbior semaforow \n");

if(semctl(semid, 0, SETVAL, 1)==-1)
{
perror("Glowny: Blad przy ustawianiu wartosci poczatowej semafora!");
exit(2);
}
printf("Glowny: Ustawilem wartosc poczatkowa semafora \n");

for(i=0;i<liczba_pp;i++)
{
	switch(fork())
	{
		case -1:
			perror("Blad przy tworzeniu procesow potomnych!");
			exit(3);
			break;
		case 0:
			if(execl(argv[1], argv[1], argv[3], NULL)==-1)
			{
			perror("Blad execl!");
			exit(3);
			}
			break;
	}	
}

for(i=0;i<liczba_pp;i++)
{
p=wait(&x);
printf("Glowny: proces %d zakonczyl sie z kodem %d \n",p,x);
}
if(semctl(semid, 0, IPC_RMID)==-1)
{
perror("Glowny: Blad przy usuwaniu zbioru semaforow!");
exit(4);
}
printf("Glowny: Usunalem zbior semaforow \n");
exit(0);
}
