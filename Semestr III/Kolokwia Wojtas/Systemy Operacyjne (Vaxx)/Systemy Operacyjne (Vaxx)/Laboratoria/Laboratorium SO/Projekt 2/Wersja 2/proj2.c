#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<pthread.h>
#include<time.h>

int suma1, suma2;
int t[2][10];

void *watek1()
{
int i;
for(i=0;i<10;i++) suma1 += t[0][i];
printf("suma elementów pierwszego wiersza tablicy = %d\n", suma1);
printf("TID1: %u \n",( unsigned int)pthread_self());
sleep(1);
pthread_exit((void*)20);
}

void *watek2()
{
int i;
for(i=0;i<10;i++) suma2 += t[1][i];
printf("suma elementów drugiego wiersza tablicy = %d\n", suma2);
pthread_exit((void*)10);
}


int main()
{
int razem,i,j,wynik,ret;
pthread_t id1, id2;

srand(time(NULL));
for(i=0;i<2;i++)
	for(j=0;j<10;j++)
		t[i][j]=rand()%20;

wynik=pthread_create(&id1, NULL, watek1, NULL);
if(wynik==-1)
	{
	perror("Blad przy tworzeniu watku pierwszego");
	exit(1);
	}

wynik=pthread_create(&id2, NULL, watek2, NULL);
if(wynik==-1)
	{
	perror("Blad przy tworzeniu watku drugiego");
	exit(1);
	}



ret=pthread_join(id1,NULL);
if(ret==-1)
	{
	perror("Blad przy przylaczaniu watku pierwszego");
	exit(2);
	}

ret=pthread_join(id2,NULL);
if(ret==-1)
	{
	perror("Blad przy przylaczaniu watku drugiego");
	exit(2);
	}


	razem=suma1+suma2;

printf("Suma elementow tablicy=%d\n",razem);

return 0;
}
