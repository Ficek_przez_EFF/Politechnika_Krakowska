#!/bin/bash

if [ "$#" -lt 2 ]
   then 
	echo "Za mala ilosc argumentow! (potrzeba 2)."
	exit 1
fi

zm_pl1=$(wc -m $1 |cut -d" " -f1)
zm_pl2=$(wc -m $2 |cut -d" " -f1)


echo "$1 -> $zm_pl1"
echo "$2 -> $zm_pl2"

if [ "$zm_pl1" -eq "$zm_pl2" ]
   then 
	echo "Oba pliki $1 i $2 sa rowne"
   elif  [ "$zm_pl1" -gt "$zm_pl2" ]
	 then 
		echo "Plik $1 jest dluzysz niz $2"
	 else 
		echo "Plik $2 jest dluzszy niz $2"
fi


