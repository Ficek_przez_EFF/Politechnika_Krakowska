#!/bin/bash

licznik=1
cat $1 | while read line
	    do
		if [ $licznik -eq 10 ] || [ $licznik -eq 20 ]
		   then 
			echo $line >>$2
		fi
		licznik=$(($licznik+1))
	    done

exit 0

