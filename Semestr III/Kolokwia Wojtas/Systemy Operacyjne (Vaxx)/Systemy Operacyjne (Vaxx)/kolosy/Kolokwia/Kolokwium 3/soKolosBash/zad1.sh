#!/bin/bash

for foo in *.$1
do
   new=$(basename $foo .$1)
   cp $foo "$new.$2"
done
