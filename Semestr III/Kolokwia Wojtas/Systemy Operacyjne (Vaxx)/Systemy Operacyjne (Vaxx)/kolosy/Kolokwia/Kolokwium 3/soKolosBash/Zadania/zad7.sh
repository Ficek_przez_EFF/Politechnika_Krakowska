#!/bin/bash

if [ "$#" -ne 2 ]
   then 
	echo "Blad argumentow"
   	exit 1
fi

dl_pl1=$(wc -l "$1" |cut -d" " -f1)
dl_pl2=$(wc -l "$2" |cut -d" " -f1)

if [ $dl_pl1 -gt $dl_pl2 ]
   then 
	roz=$(($dl_pl1-$dl_pl2))
	echo "Plik $1 jest wiekszy od $2 o $roz linii"
   else 
	roz=$(($dl_pl2-$dl_pl1))
	echo "Plik $2 jest dluzszy od $1 o $roz linii"
fi

exit 0

