Zadania, kt�re dostali przed kolosem. Brak tre�ci; rozwi�zania:

1.
#!/bin/bash

if [ $# -eq 2 ];then
plik1=`cat $1 | wc -c`
plik2=`cat $2 | wc -c`
if [ $plik1 -gt $plik2 ];then
echo "$1 jest dluzszy"
else
if [ $plik1 -lt $plik2 ];then
echo "$2 jest dluzszy"
else
echo "$1 i $2 sa rowne"
fi
fi
else
echo "Niewystarczajaca ilosc parametrow"
fi


2.
#!/bin/bash

i=0;
while [ $i -ne 5 ]
do
echo "cccccc"
i=$((i+1))
done


3.
#!/bin/bash

if [ $# -eq 2 ];then
wynik=`grep $2 alfa.c`
i=0;
while [ $i -ne $1 ];do
echo $wynik
i=$((i+1))
done
else
echo "Niewystarczajaca ilosc parametrow"
fi


4.
#!/bin/bash

tmp="tmp_$$_tmp"
mv alfa $tmp
mv beta alfa
mv $tmp beta


5.
#!/bin/bash

if [ $# -eq 3 ];then
cat $1 $2 | sort > $3
else
echo "Niewystarczajaca ilosc parametrow"
fi


6.
#!/bin/bash

if [ $# -eq 3 ];then
grep $2 $1 > $3
else
echo "Niewystarczajaca ilosc parametrow"
fi


7.
#!/bin/bash

dl1=`cat $1 | wc -l`
dl2=`cat $2 | wc -l`
echo "$dl1"
echo "$dl2"

if [ $dl1 -gt $dl2 ];then
r=$((($dl1 - $dl2)))
echo "Dluzszy plik to $1 o $r znakow"
else
if [ $dl1 -lt $dl2 ];then
r=$((($dl2 - $dl1)))
echo "Dluzszy plik to $2 o $r znakow"
else
echo "Pliki sa rowne"
fi
fi

exit 0


8.
#!/bin/bash

if [ $# == 2 ];then
grep $1 *.c > $2
grep $1 *.c
else
echo "Zla ilosc parametrow"
fi
exit 0


9.
#!/bin/bash

if [ $# == 3 ];then
cat $1 | sort > $1
cat $2 | sort > $1
cat $1 $2 > $3
else
echo "Zla ilosc parametrow"
fi
exit 0


10.
#!/bin/bash

if [ $# == 2 ];then
sed -n '1 p' < $1 > $2
sed -n '3 p' < $1 >> $2

else
echo "Zla ilosc parametrow"
fi
exit 0


11.
#!/bin/bash
n=0
if [ $# == 3 ];then

while [ $n -lt 5 ];do
head -n $3 $2 | tail -n 1 >> $1
head -n $3 $2 | tail -n 1
n=$((n+1))
done
else
echo "Zla ilosc parametrow"
fi
exit 0


12.
#!/bin/bash

if [ $# == 1];then
grep '^#' *.c >$1
else
echo "Zla ilosc parametrow"
fi
exit 0


13.
#!/bin/bash

if [ $# == 3];then
echo $PATH > $1
echo $HOME > $3
cat $1 $3 >$2
else
echo "Zla ilosc parametrow"
fi
exit 0