Kolos:

1. Wypisz wszystkie pliki specjalne do pliku ktory jest pierwszym parametrem wywolania a ich ilosc wypisz do pliku ktory jest drugim parametrem wywolania

Kod:
#!/bin/bash

if [ $# == 2 ];then
zm=`find /dev -type b -o -type c`
zm1=`find /dev -type b -o -type c | wc -l`
echo "$zm" > $1
echo "$zm1"> $2

else
echo "Zla ilosc parametrow"
fi
exit 0

2. Wylistuj wszystkie pliki z katalogu bed�cego 1. parametrem wywo�ania (oczywiscie na ekran). Oblicz liczb� podkatalog�w znajdujacych sie w tym katalogu. Oblicz ilo�� plik�w zwyk�ych w tym katalogu i wpisz do pliku bedacym 2. parametrem wywo�ania. Sprawdz zajmowana przestrzen dyskowa przez ten katalog, wyslij na ekran i do pliku bedacego 3. parametrem wywo�ania.

#!/bin/bash

if [ $# == 3 ];then

ls $1 -l > plik
grep '^-' plik #listowanie plikow

zmienna=`grep '^d' plik | wc -l` #obliczanie ilosci katalogow
echo "ilosc katalogow $zmienna"

zmienna1=`grep '^-' plik | wc -l` #obliczanie ilosci plikow

echo "Liczba plikow $zmienna1"
echo "Liczba plikow $zmienna1" >$2


echo "Przestrzen dyskowa: $1"
df $1 | tail -1
df $1 | tail -1 > $3
echo "przestrzen dyskowa: " >> $3

else
echo "Zla ilosc parametrow"
fi
exit 0

1) Z katalogu domowego wypisz na ekran oraz skieruj do pliku alfa kt�ry jest drugim parametrem wywo�ania pliki zwyk�e, kt�re nie by�y modyfikowane przez miesi�c, kt�rych nazwa ma pi�� znak�w i zaczyna si� na liter� P. Policz r�wnie� ilo�� plik�w posiadaj�cych pe�ny zestaw praw dost�pu, wyniki zapisz do pliku b�d�cego pierwszym pierwszym parametrem uruchomienia.

Kod:
#! /bin/bash

if [ $# -eq 2 ];
then
`find . -maxdepth 1 -mtime +30 -type f -name 'P????' | tee $2`
`ls -la | cut -d ' ' -f 1 | grep 'rwxrwxrwx' | wc -l > $1`
echo `cat $2`
else
echo "Zbyt malo argumentow (potrzebne 2)"
fi


W tym przypadku skrypt jest ju� w miar� poprawiony. Pierwotnie u�y�em polecenia ls -l, przez co nie bra� on pod uwag� plik�w ukrytych. Wersja z ls -la powinna by� dobra.


2) Odnajd� pliki z rozszerz prawami dost�pu, ich nazwy zapisz do pliku kt�ry jest drugim parametrem wywo�ania, w pliku kt�ry jest pierwszym parametrem wywo�ania zapisz ich ilo��.

Kod:
#! /bin/bash

if [ $# -eq 2 ];
then
`ls -la | grep 'rw[Ssx]rw[Ssx]rw[Ttx]' | tr -s ' ' | cut -d ' ' -f 8 >$2`
`cat $2 | wc -l >$1`
else
echo "Zbyt malo argumentow (wymagane 2)"
fi


Tu szanowny p. Wojtas wspomnia�, �e mo�e zaj�� sytuacja, w kt�rej wy�wietlone zostan� katalogi . oraz .. a jest to raczej niechcianym efektem. Ponadto zwr�ci� uwag� na prawa dost�pu, bo nie uwzgl�dni�em w pierwszej wersji, �e s oraz t mog� by� r�wnie� du�ymi literami.


1. znajd� wszystkie pliki specjalne i wypisz ich nazwy do pliku b�d�cego 2 parametrem wywo�ania a ich liczb� do pliku kt�ry jest drugim parametrem wywo�ania.


#!/bin/bash

if [ $# == 2 ];then
zm=`find /dev -type b -o -type c`
zm1=`find /dev -type b -o -type c | wc -l`
echo "$zm" > $1
echo "$zm1"> $2

else
echo "trzeba podac 2 argumenty"
fi
exit 0


2. z pliku b�d�cego 2 parametrem wywo�ania wybierz linie 3,8 od pocz�tku i 5 od ko�ca i zapisz je do pliku bed�cego 1 parametrem wywo�ania. posortuj plik i ponumeruj linie.

#!/bin/bash

if [ $# == 2 ];then
sed -n '3 p' < $2 > $1
sed -n '8 p' < $2 >> $1
tail -n 5 $2| head -n -4 >> $1
cat $1 | sort | nl >> $1
else
echo "trzeba podac 2 argumenty"
fi
exit 0

