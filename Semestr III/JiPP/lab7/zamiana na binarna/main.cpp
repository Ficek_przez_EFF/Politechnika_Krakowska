#include <cstdlib>
#include <iostream>
#include <bitset>
using namespace std;

template <class Input>

string binform(Input A)
{
    string bitv = bitset<sizeof(A)*8>(A).to_string() ;
    return bitv;

}



int main()
{
    cout<<endl;
    cout << binform<char>('A') << endl;
    cout << binform<char>(65) << endl;
    cout << binform<short>(65) << endl;
    cout << binform<int>(65) << endl;
    cout << binform<long>(65) << endl;
    cout << binform<float>(65) << endl;
    cout << binform<double>(65) << endl;
    //char s[]="65";
    //cout << binform<char*>(s) << endl;

    cout<<endl;
    system("pause");
    return 0;
}



