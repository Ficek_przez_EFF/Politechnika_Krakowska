// CZTERY.cpp : Defines the entry point for the console application.
//
 

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
 
#define PI 3.1415926
 
using std::cout;
using std::endl;
 
class Figura
{
public:
    virtual double obwod() = 0;
    virtual double pole() = 0;
 
};
 
/************************* Okrag *********************************/
 
 
class Okrag
    :public Figura
{
public:
    Okrag(double rr);
    double obwod();
    double pole();
private:
    double r;
 
};
 
Okrag::Okrag(double rr) : r(rr)
{}
 
double Okrag::obwod()
{
    return 2 * PI * r;
}
 
double Okrag::pole()
{
    return PI*r*r;
}
 
/************************* KWADRAT *********************************/
 
class Kwadrat
    :public Figura
{
public:
    Kwadrat(double bb);
    double obwod();
    double pole();
private:
    double b;
 
};
 
Kwadrat::Kwadrat(double bb) : b(bb)
{}
 
double Kwadrat::obwod()
{
    return 4 * b;
}
 
double Kwadrat::pole()
{
    return b*b;
}
 
/************************* PROSTOKAT *********************************/
 
 
class Prostokat
    :public Figura
{
public:
    Prostokat(double bb1, double bb2);
    double obwod();
    double pole();
private:
    double b1, b2;
 
};
 
Prostokat::Prostokat(double bb1, double bb2): b1(bb1), b2(bb2)
{}
 
double Prostokat::obwod()
{
    return 2 * b1 + 2 * b2;
}
 
double Prostokat::pole()
{
    return b1*b2;
}
 
 
/****************************** M A I N  ***********************************/
int main()
{
    std::vector <Figura *> figury;
   
    std::fstream file("wymiary.txt");
    char tempString;
    double x1, x2;
 
    while (file.good())
    {
        file >> tempString;
        switch (tempString)
        {
        case 'o':
            file >> x1;
            figury.push_back(new Okrag(x1));
            break;
        case 'k':
            file >> x1;
            figury.push_back(new Kwadrat(x1));
            break;
 
        case 'p':
            file >> x1;
            file >> x2;
            figury.push_back(new Prostokat(x1, x2));
            break;
        }
    }
   
    for (int i = 0; i < figury.size(); i++)
    {
        cout << "Figura nr: " << i+1 << endl;
        cout << "Obwod figury:"<< figury[i]->obwod() << endl;
        cout << "Pole figury: "<<figury[i]->pole() << endl;
        cout << endl;
    }
 
    getchar();
    return 0;
}
