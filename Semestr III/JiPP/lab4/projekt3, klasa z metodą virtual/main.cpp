// ConsoleApplication2.cpp: Definiuje punkt wej�cia dla aplikacji konsolowej.
//
 
#include <iostream>
#include <cmath>>
 using namespace std;
class Figura
{
public:
    virtual double pole() = 0;
 
};
 
/* KWADRAT */
 class Kwadrat : public Figura {
	public:
   		Kwadrat(double bok=1){a=bok;}
	    double pole(){return a*a;}
 
	protected:
    double a;
 
};
 

/* OKRAG */
  
 class Okrag : public Figura{
	public:
   		Okrag(double promien=1){r=promien;}
	    double pole(){return M_PI*r*r;}
 
	protected:
    	double r;
 
};


int main()
{
    Figura *f;
   	f = new Kwadrat(6);
	   cout<<"Pole figury = " << f->pole() <<endl;
	   delete f;
	   f = new Okrag(3);
	   cout<<"Pole figury = " << f->pole() <<endl;
	   delete f;   
   
  
    return 0;
}
