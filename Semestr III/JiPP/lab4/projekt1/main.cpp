#include <iostream>
#include <cmath>
#include <string>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

using namespace std;

class Point
	{

	public:
		Point(double x,double y);
		void display();
		double distance(Point& );
		protected: 
		double x,y;
	};

Point::Point(double x=0, double y=0){
	this->x=x;
	this->y=y;
	
}
void Point::display(){
	cout<<"("<<x<<","<<y<<")\n";
	
}

/*double Point::distance(Point&p){
	double a=p.x-x, b=p.y-y;
	return sqrt(a*a+b*b);	
}*/

class NamedPoint: public Point{
	public:
		NamedPoint(string name, double x, double y);
		void display(){cout<<name<<"("<<x<<", "<<y<<")\n";}
		protected:
			string name;
};
NamedPoint::NamedPoint(string name, double x=0, double y=7){
	this->name=name;
	this->x=x;
	this->y=y;
}



int main(int argc, char** argv) {


Point pl(2,6);
NamedPoint p2("A", 6,9);
pl.display();
p2.display();




	return 0;
}
