#include <iostream>
#include <cmath>
#include <string>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */


using namespace std;


class Point
{
public:
Point(double x,double y);
virtual void display();
double distance(Point& );
protected: 
double x,y;
};


Point::Point(double x=0, double y=0){
this->x=x;
this->y=y;
}
void Point::display(){
cout<<"("<<x<<","<<y<<")\n";
}



class NamedPoint: public Point{
public:
NamedPoint(string name, double x, double y);
void display(){cout<<name<<"("<<x<<", "<<y<<")\n";}
protected:
string name;
};



NamedPoint::NamedPoint(string name, double x=0, double y=7):Point(x,y){
this->name=name;
this->x=x;
this->y=y;
}



int main(int argc, char** argv) {Point pl(2,6);


Point* pp1;

Point* pp2;

pp1= new Point(2,6);
pp2= new NamedPoint("A",9);
pp1->display();
pp2->display();


return 0;
}
