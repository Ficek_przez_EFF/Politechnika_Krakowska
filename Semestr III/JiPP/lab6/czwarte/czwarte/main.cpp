// PI�TE.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <stdio.h>

class Complex
{
public:
    Complex(double r, double i);
    double getRe() { return real;}
    double getIm() { return imaginary; }
    Complex operator+(Complex c2);
    Complex operator-(Complex c2);
    Complex operator*(Complex c2);

private:
    double real, imaginary;
};

Complex::Complex(double r, double i) : real(r), imaginary(i)
{
}

Complex Complex::operator+(Complex c2)
{
    return Complex(real + c2.real, imaginary + c2.imaginary);
}

Complex Complex::operator-(Complex c2)
{
    return Complex(real-c2.real, imaginary-c2.imaginary);
}

Complex Complex::operator*(Complex c2)
{
    return Complex(real*c2.real - imaginary*c2.imaginary, real*c2.imaginary + imaginary*c2.real);
}


int main()
{


    Complex l1(25, 25);
    Complex l2(4, 3);
    Complex l6 = l1 - l2;
    Complex l3 = l1 + l2;
    Complex l4 = l3 - l1 - l2;
    Complex l5 = l1*l2;
    std::cout << "Liczba pierwsza: " << l1.getRe() << "," << l1.getIm() << std::endl;
    std::cout << "Liczba druga : " << l2.getRe() << "," << l2.getIm() << "\n" << std::endl;
    std::cout << "Dodawanie: " << l3.getRe() << "," << l3.getIm() << std::endl;
    std::cout << "Odejmowanie : " << l6.getRe() << "," << l6.getIm() << std::endl;
    std::cout << "Dzielenie : " << l4.getRe() << "," << l4.getIm() << std::endl;
    std::cout << "Mnozenie : " << l5.getRe() << "," << l5.getIm() << std::endl;

    getchar();
    return 0;
}
