#include <iostream>
#include <cmath>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

using namespace std;

class Punkt
	{

	public:
		Punkt(double x,double y);
		void show();
		double distance(Punkt& );
		protected: 
		double x,y;
	};

Punkt::Punkt(double x, double y){
	this->x=x;
	this->y=y;
	
}
void Punkt::show(){
	cout<<"("<<x<<","<<y<<")\n";
	
}

double Punkt::distance(Punkt&p){
	double a=p.x-x, b=p.y-y;
	return sqrt(a*a+b*b);	
}



class Odcinek{
	public:
		Odcinek(Punkt& A, Punkt& B);
		double lenght();
		
		Punkt A, B;
};

	Odcinek::Odcinek(Punkt& A, Punkt& B):A(A),B(B){}


double Odcinek::lenght(){
	return A.distance(B);
	
}


class Trojkat{
	public:
		Trojkat(Punkt& A, Punkt& B, Punkt& C);
		double area();
		Punkt  A, B, C;	
	
};
	Trojkat::Trojkat(Punkt& A, Punkt& B, Punkt& C):A(A),B(B),C(C){
	this->A=A;
	this->B=B;
	this->C=C;
	
	}
	double Trojkat::area(){
		double a=A.distance(B);
		double b=B.distance(C);
		double c=C.distance(A);
		double p = (a+b+c)/2;
		return sqrt(p*(p-a)*(p-b)*(p-c));
	}
	


int main(int argc, char** argv) {




Punkt A(0,0);
Punkt B(2,0);
Punkt C(0,4);
A.show();
B.show();
C.show();

cout<<"AB = "<<A.distance(B)<<endl;
cout<<"BC = "<<B.distance(C)<<endl;
cout<<"CA = "<<C.distance(A)<<endl;



Odcinek odc(A,B);
cout<<"Lenght= "<<odc.lenght()<<endl;


Trojkat tro(A,B,C);
cout<<tro.area();



	return 0;
}
