#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;

template<typename Typ>
class Point{
private:
	Typ x;
	Typ y;
public:
	Point(){
		std::string wyjatek = "Konstruktor domyslny Point!";
		throw wyjatek;
	};
	Point(Typ x, Typ y) : x(x), y(y){};
	void Wyswietl(){
		std::cout << "Point=(" << x << "," << y << ");" << std::endl;
	};
	Typ Ret_x(){return x;}
	Typ Ret_y(){return y;}
};


template<typename Typ>
class Segment{
private:
	Point<Typ> pierwszy{0, 0};
//	Point drugi;
	Point<Typ> drugi{0, 0};
public:
	Segment(){
		std::string wyjatek = "Konstruktor domyslny Segment!";
		throw wyjatek;
	};
	Segment(Point<Typ> &pierwszy, Point<Typ> &drugi){
		std::cout << "Konstruktor Segment!" << std::endl;
		this->pierwszy = Point(pierwszy.Ret_x(), pierwszy.Ret_y());
		this->drugi = Point(drugi.Ret_x(), drugi.Ret_y());
	};
	Typ Oblicz(){
		Typ x1 = pierwszy.Ret_x();
		Typ y1 = pierwszy.Ret_y();
		Typ x2 = drugi.Ret_x();
		Typ y2 = drugi.Ret_y();

		return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	};
};

class Rectangle{
private:
	double x, y, a, b;
	// x,y to punkt - lewy dolny
	// a to długość boku od x,y równoległego do osi X
	// b to długość boku od x,y równoległego do osi Y
	//
	//  ---------------------------(x+a,y+b)
	//  |                         |
	// b|                         |
	//  |                         |
	// (x,y)-----------------------
	//                a
	// Pole to x nalezy do {x,x+a}
	// 		   y nalzey do {y,y+b}

public:
	Rectangle(){
		std::string wyjatek = "Konstruktor domyslny Rectangle";
//		throw wyjatek;
	}
	Rectangle(double x, double y, double a, double b) : x(x), y(y), a(a), b(b){}
	friend ostream &operator<<(ostream &os, Rectangle &obiekt){
		return os << "x=" << obiekt.x << ", y=" << obiekt.y << ", a=" << obiekt.a << ", b=" << obiekt.b << ", x2="
				  << obiekt.x + obiekt.a << ", y2=" << obiekt.y + obiekt.b
				  << "\n P(" << obiekt.x << "," << obiekt.y
				  << "), P2(" << obiekt.x + obiekt.a << "," << obiekt.y + obiekt.b << ")";
	}
	double Rx(){return x;}
	double Ry(){return y;}
	double Ra(){return a;}
	double Rb(){return b;}
	Rectangle operator*(Rectangle obiekt){
		double reta, retb, retx, rety;
		//this->rectanle srodkowy
		//obiekt naokolo
		if ((obiekt.Rx() + obiekt.Ra() < x) || (obiekt.Ry() + obiekt.Rb() < y) || (x + a < obiekt.Rx()) ||
			(y + b < obiekt.Ry())){
			return Rectangle(0, 0, 0, 0);
		}
		else{
			double x2 = obiekt.Rx();
			double y2 = obiekt.Ry();
			double a2 = obiekt.Ra();
			double b2 = obiekt.Rb();
			double x1 = x;
			double y1 = y;
			double a1 = a;
			double b1 = b;

//			dodanie a
			double starta, konieca;

			if (x1 > x2 && x2 + a2 < x1 + a1){
				starta = x1;
				konieca = x2 + a2;
				reta = konieca - starta;
			}
			if (x1 > x2 && x2 + a2 > x1 + a1){
				starta = x1;
				konieca = x1 + a1;
				reta = konieca - starta;
			}
			if (x1 < x2 && x2 + a2 > x1 + a1){
				starta = x2;
				konieca = x1 + a1;
				reta = konieca - starta;
			}
			if (x1 < x2 && x2 + a2 < x1 + a1){
				starta = x2;
				konieca = x2 + a2;
				reta = konieca - starta;
			}
			if (x1 == x2 && x2 > 0 && x1 > 0){
				if (a1 > a2) reta = a2;
				else reta = a1;
			}

			double startb, koniecb;
//			dodanie b
			if (y1 > y2 && y2 + b2 < y1 + b1){
				startb = y1;
				koniecb = y2 + b2;
				retb = koniecb - startb;
			}
			if (y1 > y2 && y2 + b2 > y1 + b1){
				startb = y1;
				koniecb = y1 + b1;
				retb = koniecb - startb;
			}
			if (y1 < y2 && y2 + b2 > y1 + b1){
				startb = y2;
				koniecb = y1 + b1;
				retb = koniecb - startb;
			}
			if (y1 < y2 && y2 + b2 < y1 + b1){
				startb = y2;
				koniecb = y2 + b2;
				retb = koniecb - startb;
			}
			if (y1 == y2 && b2 > 0 && b1 > 0){
				if (b1 > b2) retb = b2;
				else retb = b1;
			}

//		dodanie x
			if (obiekt.Rx() > x)
				retx = obiekt.Rx();
			else
				retx = x;

//		dodanie y
			if (obiekt.Ry() > y)
				rety = obiekt.Ry();
			else
				rety = y;
			Rectangle retval(retx, rety, reta, retb);
			return retval;
		}
	}
};
int main(){

	/*
	 * Operatorów których ne wolno przeładowywać:
	 * "." - kropki -> odniesienie do składowej klasy
	 * ".*" - kropki gwiazdki -> wybieranie składnika wskaźnikiem
	 * "?:" - pytajnika gwiazdki -> zwracanie wartości wskazanym warunkiem
	 * "::" - operatora zakresu
	 * sizeof - operatora rozmiaru
	 * static,dynamic,const,reinterpret_case - operatorow rzutowania
	 *
	 * Operatory które mogą zostać zdefiniowane wyłącznie jako metody
	 * =
	 * []
	 * ->
	 *
	 * Nie możemy przeładowywać operatorów wbudowanych int, char, float
	*/

	int a = 0;
	double b = 9.99;
	b = 9.99;
	cout << "a=" << a << ", b=" << b << endl;
	cout << "a=b :";
	a = b;
	cout << "a=" << a << ", b=" << b << endl;
	a = 0;

	cout << "a=int(b): ";
	a = int(b);
	cout << "a=" << a << ", b=" << b << endl;
	a = 0;

	cout << "a=(int)b: ";
	a = (int) b;
	cout << "a=" << a << ", b=" << b << endl;
	a = 0;

	cout << "a=dynamic_cast<int>(b): ";
//	error: cannot dynamic_cast ‘b’ (of type ‘double’) to type ‘int’ (target is not pointer or reference)
//	a = dynamic_cast<int>(b);
	cout << "a=" << a << ", b=" << b << endl;
	a = 0;

	cout << "a=static_cast<int>(b): ";
	a = static_cast<int>(b);
	cout << "a=" << a << ", b=" << b << endl;
	a = 0;

	cout << "a=const_cast<int>(b): ";
//	error: invalid use of const_cast with type ‘int’, which is not a pointer, reference, nor a pointer-to-data-member type
//	a = const_cast<int>(b);
	cout << "a=" << a << ", b=" << b << endl;
	a = 0;

	cout << "a=reinterpret_cast<int>(b): ";
//	error: invalid cast from type ‘double’ to type ‘int’
//	a = reinterpret_cast<int>(b);
	cout << "a=" << a << ", b=" << b << endl;

	/*
	 * Więc biorąc pod uwagę rzutowanie to żadne nie zadziała poprawnie, straciliśmy .99 przy każdym
	 * Dodatkowo w ogóle nie zadziała dynamic,const,reinterpret_cast ze względu na zły typ danych
	 * czyt. wyżej
	 */

	try{
		Point<int> INT(0, 0);
		Point<double> DOUBLE(2.1, 0);


		Point<int> INT2(5, 0);
		Point<double> DOUBLE2(4.1, 0);

		cout << "INT:";
		INT.Wyswietl();
		cout << "INT2:";
		INT2.Wyswietl();
		cout << "DOUBLE:";
		DOUBLE.Wyswietl();
		cout << "DOUBLE2:";
		DOUBLE2.Wyswietl();

		Segment<int> Segment1(INT, INT2);
		Segment<double> Segment2(DOUBLE, DOUBLE2);

		cout << "Segment1.Oblicz()= " << Segment1.Oblicz() << endl;
		cout << "Segment2.Oblicz()= " << Segment2.Oblicz() << endl;

//		prostokat glowny
		Rectangle prostokat(5, 5, 5, 5);

//		wynik: P(5,5), P2(10,10)
//		Rectangle prostokat2(4, 4, 7, 7);

//		wynik: P(6,6), P2(9,9)
//		Rectangle prostokat2(6, 6, 3, 3);

//		wynik: P(5,5), P2(6,6)
//		Rectangle prostokat2(4, 4, 2, 2);

//		wynik: P(5,9), P2(6,10)
//		Rectangle prostokat2(4, 9, 2, 2);

//		wynik: P(9,5), P2(10,6)
//		Rectangle prostokat2(9, 4, 2, 2);

//		wynik: P(9,9), P2(10,10)
//		Rectangle prostokat2(9, 9, 2, 2);

		Rectangle prostokat2(5, 5, 3, 3);

//		cout << prostokat << endl;
//		cout << prostokat2 << endl;
		Rectangle prostokat3 = prostokat * prostokat2;
		cout << "prostokat3 :" << prostokat3 << endl;

		Rectangle prostokat4 = prostokat2 * prostokat;
		cout << "prostokat4: " << prostokat4 << endl;


//		Tutaj test domyslnych konstruktorow
//		cout << "\t\tPoint<int> d_INT;" << endl;
//		Point<int> d_INT;
//		cout << "\t\tPoint<double> d_DOUBLE;:" << endl;
//		Point<double> d_DOUBLE;
//
//		cout << "d_INT && d_DOUBLE.Wyswietl():" << endl;
//		d_INT.Wyswietl();
//		d_DOUBLE.Wyswietl();
	}
	catch (std::string &w){
		std::cout << "Wyjatek: " << w << std::endl;
	}


	return 0;
}

